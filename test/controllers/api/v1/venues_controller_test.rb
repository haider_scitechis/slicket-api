require 'test_helper'

class Api::V1::VenuesControllerTest < ActionController::TestCase
	test "index" do
		get :index
		assert_response :true
		assert_not_nil assigns(:venues)
	end
end
