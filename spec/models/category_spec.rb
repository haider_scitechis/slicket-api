require 'rails_helper'

RSpec.describe Category, :type => :model do
  category = FactoryGirl.create(:category)
  it 'Checks the class name' do
    expect(category.class.name).to eq("Category")
  end

  it { should belong_to(:parent)}
  it { should have_many(:event_categories)}
  it { should have_many(:subcategories)}
end
