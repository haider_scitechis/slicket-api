require 'rails_helper'

RSpec.describe NegotiationLog, :type => :model do


  # create object
  negotiation_log = FactoryGirl.create(:negotiation_log)

  # set polymorphics 
  bid   = FactoryGirl.create(:bid)
  negotiation_log.negotiable      = bid
  negotiation_log.negotiable_type = "Bid"


  # validate object
  it 'Checks the class name' do
    expect(negotiation_log.class.name).to eq("NegotiationLog")
  end

  # validate associations
  it { should belong_to(:bid) }
  it { should belong_to(:offer) }
  it { should belong_to(:negotiable) }

  # validate presence
  it { should validate_presence_of :price }
  it { should validate_presence_of :quantity }


end