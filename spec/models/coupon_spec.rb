require 'rails_helper'

RSpec.describe Coupon, :type => :model do
  coupon =  FactoryGirl.create(:coupon)

  it 'Checks the class name' do
    expect(coupon.class.name).to eq("Coupon")
  end

  it { should validate_presence_of :coupon_number }

end