require 'rails_helper'

RSpec.describe Event, :type => :model do

  # create factory object
  event =  FactoryGirl.create(:event)

  # validate object
  it 'Checks the class name' do
    expect(event.class.name).to eq("Event")
  end

  # validate associations
  it { should belong_to(:venue) }
  it { should have_many(:offers) }
  it { should have_many(:venue_groups) }
  it { should have_many(:bids) }
  it { should have_many(:event_sections) }

  # validate presence
  it { should validate_presence_of :date }
  it { should validate_presence_of :name }

end