require 'rails_helper'

RSpec.describe Bid, :type => :model do

  bid           =  FactoryGirl.create(:bid)
  venue_group   =  FactoryGirl.create(:venue_group)

  bid.groupable         = venue_group
  bid.groupable_type    = "VenueGroup" 


  # validate object
  it 'Checks the class name' do
    expect(bid.class.name).to eq("Bid")
  end

  # validate associations
  it { should belong_to(:user) }
  it { should belong_to(:event) }
  it { should belong_to(:groupable) }

  # validate presence
  it { should validate_presence_of :price }
  it { should validate_presence_of :quantity }
  it { should validate_presence_of :expire_at }
  it { should validate_presence_of :cancelothers }
  it { should validate_presence_of :is_negotiation }

end