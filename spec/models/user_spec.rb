require 'rails_helper'

RSpec.describe User, :type => :model do

  # create object
  user =  FactoryGirl.create(:user_addresses)

  # validate object
  it 'Checks the class name' do
    expect(user.class.name).to eq("User")
  end

  # validate associations

  it { should have_many(:bids) }
  it { should have_many(:security_answers) }
  it { should have_many(:addresses) }
  it { should have_many(:custom_venue_groups) }
  it { should have_many(:sentNotifications) }
  it { should have_many(:recievedNotifications) }

  #validate presence and checks

  it 'checks the  presence & validity of password' do
    expect(user.password.length).to be >= 8
  end

  it 'checks email presence' do
    expect(user.email).to include('@')
  end

  it 'checks the  presence & validity of password' do
    expect(user.email.length).to be <= 256
  end

  it 'checks the  presence & validity of password' do
    expect(user.username.length).to be <= 128
  end

  it 'checks the  presence & validity of password' do
    expect(user.title.length).to be <= 32
  end

  it 'checks the  presence & validity of password' do
    expect(user.first_name.length).to be <= 128
  end


end