require 'rails_helper'

RSpec.describe UploadedTicket, :type => :model do
  uploaded_ticket =FactoryGirl.create(:uploaded_ticket)
  it 'Checks the class name' do
    expect(uploaded_ticket.class.name).to eq("UploadedTicket")
  end
  it{should belong_to(:offer_ticket)}
end
