require 'rails_helper'

RSpec.describe VenueSection, :type => :model do

  venue_section  =  FactoryGirl.create(:venue_section)

  # validate object
  it 'Checks the class name' do
  expect(venue_section.class.name).to eq("VenueSection")
  end

  # validate associations
  it { should have_many(:event_sections) }
  it { should have_many(:disclosures) }


  # validate presence

end