require 'rails_helper'

RSpec.describe Sale, :type => :model do
  sale = FactoryGirl.create(:sale)
  # validate object
  it 'Checks the class name' do
    expect(sale.class.name).to eq("Sale")
  end

  # validate associations
  it { should belong_to(:bid) }
  it { should belong_to(:offer) }
  it { should belong_to(:ticket_status) }
  it { should belong_to(:coupon)}

  # validate presence
  it { should validate_presence_of :bid }
  it { should validate_presence_of :quantity }
  it { should validate_presence_of :offer }
  it { should validate_presence_of :price }
end
