require 'rails_helper'

RSpec.describe VenueGroup, :type => :model do
	venue_group =  FactoryGirl.create(:venue_group)

  it 'Checks the class name' do
    expect(venue_group.class.name).to eq("VenueGroup")
  end
	it { should belong_to(:event) }
  it { should have_many(:venue_rows) }
  it { should have_many(:offers) }
end
