require 'rails_helper'

RSpec.describe VenueRow, :type => :model do

  #create object
  venue_row = FactoryGirl.create(:venue_row)

  #validate object
  it 'Checks the class name' do
    expect(venue_row.class.name).to eq("VenueRow")
  end
  
  #validate associations
  it { should belong_to (:event_section) }
  it { should belong_to (:venue_group) }

  #validate presence
  it { should validate_presence_of :row_number }

end