require 'rails_helper'

RSpec.describe Offer, :type => :model do

  # create object
  offer = FactoryGirl.create(:offer)

    # validate object
    it 'Checks the class name' do
    expect(offer.class.name).to eq("Offer")
    end

    # validate associations
    it { should have_many(:offer_tickets) }
    it { should belong_to(:ticket_type) }
    it { should belong_to(:venue_group) }
    it { should belong_to(:event) }
    it { should belong_to(:user) }

    # validate presence
    it { should validate_presence_of :venue_group_id }
    it { should validate_presence_of :event_id }
    it { should validate_presence_of :user_id }
    it { should validate_presence_of :ticket_type_id }
    it { should validate_presence_of :price }
    it { should validate_presence_of :cancelothers }

end