require 'rails_helper'

RSpec.describe EventSection, :type => :model do

  event_seciton = FactoryGirl.create(:event_section)

  it 'Checks the class name' do
    expect(event_seciton.class.name).to eq("EventSection")
  end

  it { should belong_to(:event) }
  it { should belong_to(:venue_section) }

end
