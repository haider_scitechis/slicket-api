require 'rails_helper'

RSpec.describe Disclosure, :type => :model do
  disclosure           =  FactoryGirl.create(:disclosure)

  # validate object
  it 'Checks the class name' do
    expect(disclosure.class.name).to eq("Disclosure")
  end

  # validate associations
  it { should belong_to(:disclosureable) }

  # validate presence
  it { should validate_presence_of :name }
  it { should validate_presence_of :desc } 
  
end
