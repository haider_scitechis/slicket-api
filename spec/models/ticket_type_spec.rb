require 'rails_helper'

RSpec.describe TicketType, :type => :model do

  ticket_type = FactoryGirl.create(:ticket_type)
  it 'Checks the class name' do
  expect(ticket_type.class.name).to eq("TicketType")
  end
  it { should have_many(:offers) }
end
