require 'rails_helper'

RSpec.describe Venue, :type => :model do

  venue = FactoryGirl.create(:venue)

  # validate object
  it 'Checks the class name' do
    expect(venue.class.name).to eq("Venue")
  end

  # validate associations
  it { should have_many(:venue_sections) }
  it { should have_many(:events) }

  # validate presence
  it { should validate_presence_of :venue }
  it { should validate_presence_of :street_address }
  it { should validate_presence_of :team }
    
end