require 'rails_helper'

RSpec.describe Address, :type => :model do
  address = FactoryGirl.create(:address)
  it "should be valid" do
    expect(address).to respond_to(:name, :address1, :address2, :city, :state, :zip, :phone)
  end

  it 'checks if address belongs to a user' do
    expect(address.user).to be
  end

end
