require 'rails_helper'

RSpec.describe OfferTicket, :type => :model do
  offer_ticket=FactoryGirl.create(:offer_ticket)
  it 'Checks the class name' do
  expect(offer_ticket.class.name).to eq("OfferTicket")
  end
  it { should have_one(:uploaded_ticket) }
  it { should belong_to(:offer) }
end
