require 'rails_helper'

RSpec.describe Api::V1::VenueGroupsController, :type => :controller do
  let(:venue_group) { VenueGroup.create! }
  describe "GET #index" do
    it "responds successfully with an HTTP 200 status code" do
      event = FactoryGirl.create(:event)
      
      user         = FactoryGirl.create(:user)
      user.reset_authentication_token
      user.save
      
      get :index, :user_email => user.email, :user_token => user.authentication_token, event_id: event.id, :format => :json

      expect(response).to be_success
      expect(response).to have_http_status(200)
      expect(response).to render_template("index")

    end
  end

  describe "#create" do
    it "responds successfully with an HTTP 201 status code" do
      event =FactoryGirl.create(:event)

      user         = FactoryGirl.create(:user)
      user.reset_authentication_token
      user.save
      
      post :create, :user_email => user.email, :user_token => user.authentication_token, event_id: event.id,:format => :json

      expect(response).to be_success
      expect(response).to have_http_status(201)

    end
  end

 describe "#show" do
    it "responds successfully with an HTTP 200 status code" do
      venue_group = FactoryGirl.create(:venue_group)
      
      user         = FactoryGirl.create(:user)
      user.reset_authentication_token
      user.save
      
      get :show, :user_email => user.email, :user_token => user.authentication_token, event_id: venue_group.event_id, id: venue_group.id, :format => :json

      expect(response).to be_success
      expect(response).to have_http_status(200)
      expect(response).to render_template("show")

    end
  end


 describe "#update" do
    it "responds successfully with an HTTP 204 status code" do
      venue_group = FactoryGirl.create(:venue_group)
      
      user         = FactoryGirl.create(:user)
      user.reset_authentication_token
      user.save
      
      put :update, :user_email => user.email, :user_token => user.authentication_token, event_id: venue_group.event_id, id: venue_group.id, :format => :json

      expect(response).to be_success
      expect(response).to have_http_status(204)

    end
  end


 describe "#delete" do
  venue_group = FactoryGirl.create(:venue_group)
    it "responds successfully with an HTTP 204 status code" do
      
      user         = FactoryGirl.create(:user)
      user.reset_authentication_token
      user.save
      
      delete :destroy, :user_email => user.email, :user_token => user.authentication_token, event_id: venue_group.event_id, id: venue_group.id, :format => :json

      expect(response).to be_success
      expect(response).to have_http_status(204)

    end
  end

  describe "#list_tickets" do
  venue_group = FactoryGirl.create(:venue_group)
    it "responds successfully with an HTTP 200 status code" do
      
      user         = FactoryGirl.create(:user)
      user.reset_authentication_token
      user.save
      
      get :list_tickets, :user_email => user.email, :user_token => user.authentication_token, event_id: venue_group.event_id, id: venue_group.id, :format => :json

      expect(response).to be_success
      expect(response).to have_http_status(200)

    end
  end

end
