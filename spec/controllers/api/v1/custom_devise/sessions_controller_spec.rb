require 'rails_helper'



RSpec.describe Api::V1::CustomDevise::SessionsController, :type => :controller do
  include Devise::TestHelpers

  before do
    @user =  FactoryGirl.create(:user)
    @params           =  {}
    @params[:user]    =  {}
    @params[:user][:email]    = @user.email
    @params[:user][:password] = "hellohello"
  end

  describe "create session successfully" do
    render_views

    before(:each) do
      @request.env['devise.mapping'] = Devise.mappings[:user]
      post :create, @params,  :format => :json
    end

    it "should have first_name, last_name, email, auth_token" do
      json = JSON.parse response.body
      json['first_name'].should_not be_nil
      json['last_name'].should_not be_nil
      json['email'].should_not be_nil
      json['auth_token'].should_not be_nil
    end

    it "responds successfully with an HTTP 200 status code" do
      expect(response).to be_success
      expect(response).to have_http_status(200)
    end

  end

  describe "raise exception in session with wrong password" do
    render_views

    before(:each) do
      @request.env['devise.mapping'] = Devise.mappings[:user]

      @user =  FactoryGirl.create(:user)
      @params           =  {}
      @params[:user]    =  {}
      @params[:user][:email]    = @user.email
      @params[:user][:password] = "hellohell"
      post :create, @params,  :format => :json
    end

    it "should have error" do
      json = JSON.parse response.body
      json['errors'].should_not be_nil
    end

    it "should responds successfully with status code" do
      expect(response).to have_http_status(401)
    end

  end

  describe "Delete #destroy" do
    it "responds successfully with an HTTP 204 status code" do
      @request.env['devise.mapping'] = Devise.mappings[:user]

      user =  FactoryGirl.create(:user_addresses)
      delete :destroy

      expect(response).to be_success
      expect(response).to have_http_status(204)
    end
  end
end
