require 'rails_helper'

RSpec.describe Api::V1::CustomDevise::PasswordsController, :type => :controller do
               
  include Devise::TestHelpers
  describe "send reset password token with email successfully" do
    render_views

    before(:each) do
        @request.env['devise.mapping'] = Devise.mappings[:user]
        @user =  FactoryGirl.create(:user)
        post :create, {:email => "#{@user.email}"},  :format => :json
    end

    it "responds successfully with an HTTP 200 status code" do
        expect(response).to be_success
        expect(response).to have_http_status(200)
    end

    it "should respond with Email sent text" do
      json = JSON.parse response.body
      json['message'].should_not be_nil
      json['message'].should include("Email sent")
    end

  end


  describe "raise exception in send reset password token" do
    it "responds successfully with an HTTP 503 status code" do
        @request.env['devise.mapping'] = Devise.mappings[:user]
        post :create, {:email => "wrong_email@gmail.com"},  :format => :json
        expect(response).to have_http_status(503)

    end
  end


  describe "raise exception in reset password with wrong reset passwor dtoken" do
    it "responds successfully with an HTTP 422 status code" do
        @request.env['devise.mapping'] = Devise.mappings[:user]

        patch :update, {:password => "abcd1234", :password_confirmation => "abcd1234", :reset_password_token => "cyCCdCF3h3mNPBwKzELK"},  :format => :json
        expect(response).to have_http_status(422)

    end
  end

'''
# please reset_password_token with valid token

  describe "reset password with reset password token successfully" do
    include Devise::TestHelpers
    it "responds successfully with an HTTP 202 status code" do
        @request.env["devise.mapping"] = Devise.mappings[:user]

        patch :update, {:password => "abcd1234", :password_confirmation => "abcd1234", :reset_password_token => "cyCCdCF3h3mNPBwKzELK"},  :format => :json

        expect(response).to be_success
        expect(response).to have_http_status(202)

    end
  end
'''


end