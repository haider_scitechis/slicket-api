require 'rails_helper'



RSpec.describe Api::V1::CustomDevise::RegistrationsController, :type => :controller do
  include Devise::TestHelpers


  describe "create user successfully" do
    render_views

    before(:each) do
      @request.env['devise.mapping'] = Devise.mappings[:user]
      post :create, {:email=>"hello#{Random.rand(10000)}@gmail.com", :password => "hellohello", :username => "lomonop#{Random.rand(10000)}", :first_name => "adsa", :last_name => "asasd", :title => "sdsd"},  :format => :json
    end

    it "responds successfully with an HTTP 201 status code" do
  		expect(response).to be_success
      expect(response).to have_http_status(201)
    end

    it "should have first_name, last_name and auth_token" do
      json = JSON.parse response.body
      json['first_name'].should_not be_nil
      json['last_name'].should_not be_nil
      json['auth_token'].should_not be_nil
    end

  end

  describe "raise an error while creating user " do
    it "responds unsuccessfully with an HTTP 422 status code" do
      @request.env['devise.mapping'] = Devise.mappings[:user]
      post :create, {:email=>"", :password => "hellohello", :username => "lomonop#{Random.rand(10000)}", :first_name => "adsa", :last_name => "asasd", :title => "sdsd"},  :format => :json
      expect(response).to have_http_status(422)
    end
  end
end
