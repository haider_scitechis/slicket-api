require 'rails_helper'

RSpec.describe Api::V1::EventsController, :type => :controller do

  let(:event) { Event.create! }

   before do

      @user = FactoryGirl.create(:user)
      @user.reset_authentication_token
      @user.save

      @event = FactoryGirl.create(:event)

   end

  describe "GET #index" do
    render_views

    before(:each) do
      get :index, :user_email => @user.email, :user_token => @user.authentication_token, :format => :json
    end

    it "should responds successfully with an HTTP 200 status code" do
      expect(response).to be_success
      expect(response).to have_http_status(200)
    end

    it "should responds successfully with an HTTP 200 status code" do
      expect(response).to render_template("index")
    end

    it "should have a event object" do

      json    = JSON.parse(response.body)
      events  = json['events']

      events.each do |obj|

        event   = obj["event"]
        event["id"].should_not be_nil
        event["name"].should_not be_nil
        event["date"].should_not be_nil

        venue   = obj['venue']
        event["id"].should_not be_nil

      end
    end

  end

  describe "#create" do

    before(:each) do

      event_category  = FactoryGirl.create(:event_category)

      @params            = {}
      @params[:name]     = Faker::Name.name
      @params[:date]     = DateTime.now.to_date
      @params[:event_category_id] = event_category.id

      post :create, :user_email => @user.email, :user_token => @user.authentication_token, :event =>  @params, :format => :json
    end

    it "should increase bid count by 1" do
      lambda do
      post :create, :user_email => @user.email, :user_token => @user.authentication_token, :event =>  @params, :format => :json
      end.should change(Event, :count).by(1)
    end

    it "responds successfully with an HTTP 201 status code" do

      expect(response).to be_success
      expect(response).to have_http_status(201)
    end

  end

 describe "#show" do
  render_views
    
    before(:each) do      
      get :show, :user_email => @user.email, :user_token => @user.authentication_token, id: @event.id, :format => :json
    end

    it "should have a event object" do

      json    = JSON.parse(response.body)
      event   = json["event"]

      event["id"].should_not be_nil
      event["name"].should_not be_nil
      event["date"].should_not be_nil
      event["event_category_id"].should_not be_nil
      event["venue_id"].should_not be_nil

    end

    it "responds successfully with an HTTP 200 status code" do
      expect(response).to be_success
      expect(response).to have_http_status(200)
    end

    it "should be render by template show" do
      expect(response).to render_template("show")
    end

  end

  describe "#update" do
    it "responds successfully with an HTTP 204 status code" do

      venue= FactoryGirl.create(:event)      
      event = FactoryGirl.create(:event)


      params            = {}
      params[:name]     = Faker::Name.name
      params[:date]     = DateTime.now.to_date
      params[:venue_id] = venue.id
      
      put :update, :user_email => @user.email, :user_token => @user.authentication_token, :id => @event.id, :event => params, :format => :json


      expect(response).to be_success
      expect(response).to have_http_status(204)

    end
  end

  describe "#delete" do

    it "should responds successfully with an HTTP 204 status code and should change count by -1" do
      lambda do
        delete :destroy, :user_email => @user.email, :user_token => @user.authentication_token, id: @event.id, :format => :json
      end.should change(Event, :count).by(-1)

      expect(response).to be_success
      expect(response).to have_http_status(204)
    end
  end



  describe "#list_tickets" do

    render_views
      
    before(:each) do
      @event = FactoryGirl.create(:event)
      get :list_tickets, :user_email => @user.email, :user_token => @user.authentication_token, id: @event.id, :format => :json
    end

    it "responds successfully with an HTTP 200 status code" do
      expect(response).to be_success
      expect(response).to have_http_status(200)
    end

    it "should be render by template" do    
      expect(response).to render_template("list_tickets")
    end

    it "should have array of tickets" do

      json      = JSON.parse(response.body)
      tickets   = json["offer_tickets"]

      tickets.each do |ticket|

        offer_ticket    = ticket['offer_ticket']
        uploaded_ticket = ticket['uploaded_ticket']

        offer_ticket.should_not be_nil
        uploaded_ticket.should_not be_nil

      end
    end

  end

end