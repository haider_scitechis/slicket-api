require 'rails_helper'

RSpec.describe Api::V1::OfferTicketsController, :type => :controller do


  before do
    @user   = FactoryGirl.create(:user)
    @user.reset_authentication_token
    @user.save

  end

  describe "GET #index" do
    render_views

    before(:each) do
      @offer_ticket = FactoryGirl.create(:offer_ticket)
      get :index, :user_email => @user.email, :user_token => @user.authentication_token , offer_id: @offer_ticket.offer.id, event_id: @offer_ticket.offer.event_id, :format => :json
    end

    it "should have json array of offer objects" do
      json = JSON.parse response.body

      offer_tickets = json['offer_tickets']

      offer_tickets.each do |json_object|
        offer_ticket = json_object['offer_ticket']
        offer_ticket['offer_id'].should_not be_nil
        offer_ticket['venue_row_id'].should_not be_nil
      end
    end

    it "responds successfully with an HTTP 200 status code" do      
      expect(response).to be_success
      expect(response).to have_http_status(200)
    end

    it "should be render by template" do
      expect(response).to render_template("index")
    end

  end

  describe "#create" do
    it "responds successfully with an HTTP 201 status code" do
      offer       = FactoryGirl.create(:offer)
      venue_row   = FactoryGirl.create(:venue_row)
      user        = FactoryGirl.create(:user)
      user.reset_authentication_token
      user.save
      
      post :create, :user_email => user.email, :user_token => user.authentication_token , offer_id: offer.id, event_id: offer.event_id, venue_row_id: venue_row.id,  :format => :json


      expect(response).to be_success
      expect(response).to have_http_status(201)

    end
  end

  describe "#show" do
    render_views

    before(:each) do
      @offer_ticket = FactoryGirl.create(:offer_ticket)      
      get :show, :user_email => @user.email, :user_token => @user.authentication_token ,  event_id: @offer_ticket.offer.event_id, offer_id: @offer_ticket.offer_id, id: @offer_ticket.id, :format => :json
    end

    it "should have offer_ticket json" do
      json = JSON.parse(response.body)

      offer_ticket = json['offer_ticket']
      offer_ticket['offer_id'].should_not be_nil
      offer_ticket['venue_row_id'].should_not be_nil
      end

    it "responds successfully with an HTTP 200 status code" do
      expect(response).to be_success
      expect(response).to have_http_status(200)
    end

    it "should be render by template" do
      expect(response).to render_template("show")
    end

  end


 describe "#update" do
    it "responds successfully with an HTTP 204 status code" do
      offer_ticket = FactoryGirl.create(:offer_ticket)
      user         = FactoryGirl.create(:user)
      user.reset_authentication_token
      user.save
      
      put :update, :user_email => user.email, :user_token => user.authentication_token , id: offer_ticket.id, offer_id: offer_ticket.offer_id, event_id: offer_ticket.offer.event_id, venue_row_id: offer_ticket.venue_row_id,  :format => :json

      expect(response).to be_success
      expect(response).to have_http_status(204)

    end
  end


  describe "#delete" do

    before(:each) do
      @offer_ticket    = FactoryGirl.create(:offer_ticket)
      @offer_ticket1   = FactoryGirl.create(:offer_ticket, :offer_id => @offer_ticket.offer_id)
      
      delete :destroy, :user_email => @user.email, :user_token => @user.authentication_token ,  event_id: @offer_ticket.offer.event_id, offer_id: @offer_ticket.offer_id, id: @offer_ticket.id, :format => :json
    end

    it "responds successfully with an HTTP 204 status code" do
      expect(response).to be_success
      expect(response).to have_http_status(204)
    end

    it "should change count by -1" do 
      lambda do
        @offer_ticket    = FactoryGirl.create(:offer_ticket)
        @offer_ticket1   = FactoryGirl.create(:offer_ticket, :offer_id => @offer_ticket.offer_id)

        delete :destroy, :user_email => @user.email, :user_token => @user.authentication_token ,  event_id: @offer_ticket.offer.event_id, offer_id: @offer_ticket.offer_id, id: @offer_ticket.id, :format => :json
      end.should change(Bid, :count).by(0)
    end

  end

  describe "#check_ticket_status" do

    it "responds successfully with an HTTP 200 status code" do
      @offer_ticket = FactoryGirl.create(:offer_ticket)      
      get :check_ticket_status, :user_email => @user.email, :user_token => @user.authentication_token , :barcode => @offer_ticket.barcode , :format => :json

      expect(response).to be_success
      expect(response).to have_http_status(200)

    end
  end
end
