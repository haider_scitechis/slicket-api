require 'rails_helper'

RSpec.describe Api::V1::EmailAlertConfigOptionsController, :type => :controller do


  describe "GET #index" do
    it "responds successfully with an HTTP 200 status code" do

      user  = FactoryGirl.create(:user)
      user.reset_authentication_token
      user.save

      email_alert_config_options = FactoryGirl.create(:email_alert_config_option)

      get :index, :user_email => user.email, :user_token => user.authentication_token, :format => :json

      expect(response).to be_success
      expect(response).to have_http_status(200)

    end
  end


  describe "POST #create" do
    it "responds successfully with an HTTP 201 status code" do

      user  = FactoryGirl.create(:user)
      user.reset_authentication_token
      user.save

      email_alert_config = FactoryGirl.create(:email_alert_config)

      post :create, :user_email => user.email, :user_token => user.authentication_token, :email_alert_config_id => email_alert_config.id,  :format => :json

      expect(response).to be_success
      expect(response).to have_http_status(201)

    end
  end


  describe "DELETE #destroy" do
    it "responds successfully with an HTTP 204 status code" do

      user  = FactoryGirl.create(:user)
      user.reset_authentication_token
      user.save

      email_alert_config_option = FactoryGirl.create(:email_alert_config_option)
      email_alert_config_option.user = user
      email_alert_config_option.save      

      delete :destroy, :user_email => user.email, :user_token => user.authentication_token, :id => email_alert_config_option.id,  :format => :json

      expect(response).to be_success
      expect(response).to have_http_status(204)

    end
  end



end