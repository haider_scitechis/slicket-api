require 'rails_helper'

RSpec.describe Api::V1::UploadedTicketsController, :type => :controller do
   let(:uploaded_ticket) { UploadedTicket.create! }

  before do
    @user         = FactoryGirl.create(:user)
    @user.reset_authentication_token
    @user.save

    @uploaded_ticket = FactoryGirl.create(:uploaded_ticket)
  end

  describe "#show" do
    render_views

    before(:each) do
      get :show, :user_email => @user.email, :user_token => @user.authentication_token,  event_id: @uploaded_ticket.offer_ticket.offer.event_id, offer_id: @uploaded_ticket.offer_ticket.offer_id, offer_ticket_id: @uploaded_ticket.offer_ticket.id, id: @uploaded_ticket.id,:format => :json
    end

    it "responds uploaded_ticket json" do
      json = JSON.parse response.body

      uploaded_ticket = json['uploaded_ticket']
      uploaded_ticket['offer_ticket_id'].should_not be_nil

    end

    it "responds successfully with an HTTP 200 status code" do
      uploaded_ticket = FactoryGirl.create(:uploaded_ticket) 
      expect(response).to be_success
      expect(response).to have_http_status(200)
    end

    it "should be render by template" do
      expect(response).to render_template("show")
    end

  end

  describe "#update" do
    it "responds successfully with an HTTP 200 status code" do
      uploaded_ticket = FactoryGirl.create(:uploaded_ticket)
      
      user         = FactoryGirl.create(:user)
      user.reset_authentication_token
      user.save
      
      put :update, :user_email => user.email, :user_token => user.authentication_token,  event_id: uploaded_ticket.offer_ticket.offer.event_id, offer_id: uploaded_ticket.offer_ticket.offer_id, offer_ticket_id: uploaded_ticket.offer_ticket.id, id: uploaded_ticket.id,:format => :json

      expect(response).to be_success
      expect(response).to have_http_status(204)

    end
  end

  describe "#print" do
    render_views

    before(:each) do
      get :print, :user_email => @user.email, :user_token => @user.authentication_token,  event_id: @uploaded_ticket.offer_ticket.offer.event_id, offer_id: @uploaded_ticket.offer_ticket.offer_id, offer_ticket_id: @uploaded_ticket.offer_ticket.id, id: @uploaded_ticket.id,:format => :json
    end

    it "responds successfully with an HTTP 200 status code" do
      expect(response).to be_success
      expect(response).to have_http_status(200)
    end

    it "should render by template" do 
      expect(response).to render_template("print")
    end

    it "responds uploaded_ticket json" do
      json = JSON.parse response.body

      uploaded_ticket = json['uploaded_ticket']
      uploaded_ticket['offer_ticket_id'].should_not be_nil

    end

  end

  describe "#email" do
    it "responds successfully with an HTTP 200 status code" do
      uploaded_ticket = FactoryGirl.create(:uploaded_ticket)
      user = FactoryGirl.create(:user)
      
      user         = FactoryGirl.create(:user)
      user.reset_authentication_token
      user.save
      
      get :email, :user_email => user.email, :user_token => user.authentication_token,user_email: user.email, event_id: uploaded_ticket.offer_ticket.offer.event_id, offer_id: uploaded_ticket.offer_ticket.offer_id, offer_ticket_id: uploaded_ticket.offer_ticket.id, id: uploaded_ticket.id,:format => :json

      expect(response).to be_success
      expect(response).to have_http_status(200)

    end
  end

end
