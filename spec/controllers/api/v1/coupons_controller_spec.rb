require 'rails_helper'

RSpec.describe Api::V1::CouponsController, :type => :controller do
let(:coupon) { Coupon.create! }

  before do
    @user         = FactoryGirl.create(:user)
    @user.reset_authentication_token
    @user.save
  end

  describe "GET #index" do
    render_views

    before(:each) do
      get :index, :user_email => @user.email, :user_token => @user.authentication_token ,:format => :json
    end

    it "responds successfully with an HTTP 200 status code" do      
      expect(response).to be_success
      expect(response).to have_http_status(200)
    end

    it "should render by template " do
      expect(response).to render_template("index")
    end

    it "should have an array of coupons" do
      json      = JSON.parse response.body
      coupons   = json["coupons"]

      coupons.each do |coupon|

        coupon = coupon['coupon']
        coupon["id"].should_not be_nil        
        coupon["coupon_number"].should_not be_nil

      end
    end

  end

  describe "#create" do
    render_views

    before(:each) do
      post :create, :user_email => @user.email, :user_token => @user.authentication_token , coupon_number: Faker::Code.isbn ,:format => :json
    end

    it "responds successfully with an HTTP 201 status code" do    
      expect(response).to be_success
      expect(response).to have_http_status(201)
    end

    it "should responds with coupon json" do
        json      = JSON.parse response.body
        coupon = json['coupon']
        coupon["id"].should_not be_nil        
        coupon["coupon_number"].should_not be_nil
    end

    it "should change count by 1" do
      lambda do
        post :create, :user_email => @user.email, :user_token => @user.authentication_token , coupon_number: Faker::Code.isbn ,:format => :json
      end.should change(Coupon, :count).by(1)
    end


  end

  describe "#show" do
    render_views

    before(:each) do
      @coupon = FactoryGirl.create(:coupon)
      get :show, :user_email => @user.email, :user_token => @user.authentication_token, id: @coupon.id, :format => :json
    end

    it "responds successfully with an HTTP 200 status code" do    
      expect(response).to be_success
      expect(response).to have_http_status(200)
    end

    it "should be render by template show" do
      expect(response).to render_template("show")
    end

    it "should have a coupon json" do
      json      = JSON.parse response.body
      coupon = json['coupon']
      coupon["id"].should_not be_nil        
      coupon["coupon_number"].should_not be_nil
    end

  end


 describe "#update" do
    it "responds successfully with an HTTP 204 status code" do

      @coupon = FactoryGirl.create(:coupon)
      put :update, :user_email => @user.email, :user_token => @user.authentication_token, id: @coupon.id, :format => :json

      expect(response).to be_success
      expect(response).to have_http_status(204)

    end
  end


  describe "#delete" do
    render_views

    before(:each) do
      @coupon = FactoryGirl.create(:coupon)
      delete :destroy, :user_email => @user.email, :user_token => @user.authentication_token, id: @coupon.id, :format => :json
    end

    it "responds successfully with an HTTP 204 status code" do
      expect(response).to be_success
      expect(response).to have_http_status(204)
    end

    it "should change count by -1" do
      lambda do
        @coupon = FactoryGirl.create(:coupon)
        delete :destroy, :user_email => @user.email, :user_token => @user.authentication_token, id: @coupon.id, :format => :json
      end.should change(Coupon, :count).by(-1)
    end

  end

end