require 'rails_helper'

RSpec.describe Api::V1::VenueSectionsController, :type => :controller do
  let(:venue_section) { VenueSection.create! }
  describe "GET #index" do
    it "responds successfully with an HTTP 200 status code" do
      venue = FactoryGirl.create(:venue)
      
      user         = FactoryGirl.create(:user)
      user.reset_authentication_token
      user.save
      
      get :index, :user_email => user.email, :user_token => user.authentication_token, venue_id: venue.id, :format => :json

      expect(response).to be_success
      expect(response).to have_http_status(200)
      expect(response).to render_template("index")

    end
  end

  describe "#create" do
    it "responds successfully with an HTTP 201 status code" do


      disclosures1  = FactoryGirl.create(:disclosure)
      disclosures2  = FactoryGirl.create(:disclosure)
      venue         = FactoryGirl.create(:venue)

      params = {}
      params[:disclosures_attributes] = [{:name => disclosures1.name, :desc => disclosures1.desc}, {:name => disclosures2.name, :desc => disclosures2.desc}]


      user         = FactoryGirl.create(:user)
      user.reset_authentication_token
      user.save
      
      post :create, :venue_id => venue.id, :disclosures_attributes => params[:disclosures_attributes], :user_email => user.email, :user_token => user.authentication_token,:format => :json


      expect(response).to be_success
      expect(response).to have_http_status(201)

    end
  end

 describe "#show" do
    it "responds successfully with an HTTP 200 status code" do
      venue_section = FactoryGirl.create(:venue_section)
      
      user         = FactoryGirl.create(:user)
      user.reset_authentication_token
      user.save
      
      get :show, :user_email => user.email, :user_token => user.authentication_token, venue_id: venue_section.venue_id, id: venue_section.id, :format => :json

      expect(response).to be_success
      expect(response).to have_http_status(200)
      expect(response).to render_template("show")

    end
  end


 describe "#update" do
    it "responds successfully with an HTTP 204 status code" do
      venue_section = FactoryGirl.create(:venue_section)
      
      user         = FactoryGirl.create(:user)
      user.reset_authentication_token
      user.save
      
      put :update, :user_email => user.email, :user_token => user.authentication_token, venue_id: venue_section.venue_id, id: venue_section.id, :format => :json

      expect(response).to be_success
      expect(response).to have_http_status(204)

    end
  end


 describe "#delete" do
    it "responds successfully with an HTTP 204 status code" do
      venue_section = FactoryGirl.create(:venue_section)
      
      user         = FactoryGirl.create(:user)
      user.reset_authentication_token
      user.save
      
      delete :destroy, :user_email => user.email, :user_token => user.authentication_token, venue_id: venue_section.venue_id, id: venue_section.id, :format => :json

      expect(response).to be_success
      expect(response).to have_http_status(204)

    end
  end


  describe "#list_by_row" do
    it "responds successfully with an HTTP 204 status code" do
      venue_section = FactoryGirl.create(:venue_section)
      
      user         = FactoryGirl.create(:user)
      user.reset_authentication_token
      user.save
      
      get :list_by_row, :user_email => user.email, :user_token => user.authentication_token, venue_id: venue_section.venue_id, row_num: '1', :format => :json

      expect(response).to be_success
      expect(response).to have_http_status(200)

    end
  end


end
