require 'rails_helper'

RSpec.describe Api::V1::VenueRowsController, :type => :controller do
  let(:venue_row) { VenueRow.create! }
  describe "GET #index" do
    it "responds successfully with an HTTP 200 status code" do
      event_section = FactoryGirl.create(:event_section)
      
      user         = FactoryGirl.create(:user)
      user.reset_authentication_token
      user.save
      
      get :index, :user_email => user.email, :user_token => user.authentication_token, event_id: event_section.event_id, event_section_id: event_section.id, :format => :json

      expect(response).to be_success
      expect(response).to have_http_status(200)
      expect(response).to render_template("index")

    end
  end

  describe "#create" do
    it "responds successfully with an HTTP 201 status code" do
      event_section = FactoryGirl.create(:event_section)
      
      user         = FactoryGirl.create(:user)
      user.reset_authentication_token
      user.save
      
      post :create, :user_email => user.email, :user_token => user.authentication_token, event_id: event_section.event_id, event_section_id: event_section.id, row_number: rand(1..100), :format => :json

      expect(response).to be_success
      expect(response).to have_http_status(201)

    end
  end

 describe "#show" do
    it "responds successfully with an HTTP 200 status code" do
      venue_row = FactoryGirl.create(:venue_row)
      
      user         = FactoryGirl.create(:user)
      user.reset_authentication_token
      user.save
      
      get :show, :user_email => user.email, :user_token => user.authentication_token, event_id: venue_row.event_section.event_id, event_section_id: venue_row.event_section.id, id: venue_row.id,:format => :json

      expect(response).to be_success
      expect(response).to have_http_status(200)
      expect(response).to render_template("show")

    end
  end


 describe "#update" do
    it "responds successfully with an HTTP 204 status code" do
      venue_row = FactoryGirl.create(:venue_row)
      
      user         = FactoryGirl.create(:user)
      user.reset_authentication_token
      user.save
      
      put :update, :user_email => user.email, :user_token => user.authentication_token, event_id: venue_row.event_section.event_id, event_section_id: venue_row.event_section.id,id: venue_row.id,:format => :json

      expect(response).to be_success
      expect(response).to have_http_status(204)

    end
  end

 describe "#delete" do
    it "responds successfully with an HTTP 204 status code" do
      venue_row = FactoryGirl.create(:venue_row)
      
      user         = FactoryGirl.create(:user)
      user.reset_authentication_token
      user.save
      
      delete :destroy, :user_email => user.email, :user_token => user.authentication_token, event_id: venue_row.event_section.event_id, event_section_id: venue_row.event_section.id, id: venue_row.id,:format => :json

      expect(response).to be_success
      expect(response).to have_http_status(204)

    end
  end

  describe "#list_by_rating" do
  venue_row = FactoryGirl.create(:venue_row)
    it "responds successfully with an HTTP 200 status code" do
      
      user         = FactoryGirl.create(:user)
      user.reset_authentication_token
      user.save
      
      get :list_by_rating, :user_email => user.email, :user_token => user.authentication_token, event_id: venue_row.event_section.event_id, event_section_id: venue_row.event_section.id , id: venue_row.id, rating: '4', :format => :json

      expect(response).to be_success
      expect(response).to have_http_status(200)

    end
  end

  describe "#list_tickets" do
  venue_row = FactoryGirl.create(:venue_row)
    it "responds successfully with an HTTP 200 status code" do
      
      user         = FactoryGirl.create(:user)
      user.reset_authentication_token
      user.save
      
      get :list_tickets, :user_email => user.email, :user_token => user.authentication_token, event_id: venue_row.event_section.event_id, event_section_id: venue_row.event_section.id , id: venue_row.id, :format => :json

      expect(response).to be_success
      expect(response).to have_http_status(200)

    end
  end

end
