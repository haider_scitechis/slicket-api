require 'rails_helper'

RSpec.describe Api::V1::VenuesController, :type => :controller do


  before do
    @user         = FactoryGirl.create(:user)
    @user.reset_authentication_token
    @user.save
  end

  describe "GET #index" do
    render_views

    before(:each) do
      @venue = FactoryGirl.create(:venue)      
      get :index, :user_email => @user.email, :user_token => @user.authentication_token, :format => :json
    end

    it "should have json array of venue" do
      json    = JSON.parse response.body
      venues  = json['venues']

      venues.each do |json_object|
        venue = json_object['venue']

        venue['venue'].should_not be_nil
        venue['street_address'].should_not be_nil
        venue['team'].should_not be_nil

        venue_section = json_object['venue_sections']
        venue_section.should_not be_nil
      end

    end

    it "responds successfully with an HTTP 200 status code" do
      expect(response).to be_success
      expect(response).to have_http_status(200)
    end

    it "should be render by template" do
      expect(response).to render_template("index")
    end

  end

  describe "#create" do
    render_views

    before(:each) do
      post :create, :user_email => @user.email, :user_token => @user.authentication_token, venue: 'MU', street_address: 'ds', team:'abc', school:'adasd', :format => :json
    end

    it "responds successfully with an HTTP 201 status code" do      
      expect(response).to be_success
      expect(response).to have_http_status(201)
    end

    it "should increase bid count by 1" do
      lambda do
      post :create, :user_email => @user.email, :user_token => @user.authentication_token, venue: 'MU', street_address: 'ds', team:'abc', school:'adasd', :format => :json
      end.should change(Venue, :count).by(1)
    end

  end

 describe "#show" do
  render_views

    before(:each) do
      @venue = FactoryGirl.create(:venue)
      get :show, :user_email => @user.email, :user_token => @user.authentication_token, id: @venue.id, :format => :json
    end

    it "responds successfully with an HTTP 200 status code" do
      expect(response).to be_success
      expect(response).to have_http_status(200)
    end

    it "should be render by template" do
      expect(response).to render_template("show")
    end

    it "should have venue json" do

      json = JSON.parse response.body

      venue = json['venue']
      venue['venue'].should_not be_nil
      venue['street_address'].should_not be_nil
      venue['team'].should_not be_nil
    end

  end


  describe "#update" do
    it "responds successfully with an HTTP 204 status code" do
      venue = FactoryGirl.create(:venue)

      user         = FactoryGirl.create(:user)
      user.reset_authentication_token
      user.save
      
      put :update, :user_email => user.email, :user_token => user.authentication_token, id: venue.id,  venue: 'MU', street_address: 'ds', team:'abc', school:'adasd', :format => :json

      expect(response).to be_success
      expect(response).to have_http_status(204)

    end
  end


  describe "#delete" do
    it "responds successfully with an HTTP 204 status code" do
      venue = FactoryGirl.create(:venue)
      
      user         = FactoryGirl.create(:user)
      user.reset_authentication_token
      user.save
      
      delete :destroy, :user_email => user.email, :user_token => user.authentication_token, id: venue.id, :format => :json

      expect(response).to be_success
      expect(response).to have_http_status(204)

    end
  end

end
