require 'rails_helper'

RSpec.describe Api::V1::BidsController,:type => :controller do

   before do
      @event  = FactoryGirl.create(:event)
      @user   = FactoryGirl.create(:user)
      @user.reset_authentication_token
      @user.save
   end

  describe "#create" do
    render_views

    before(:each) do
      @bid          = FactoryGirl.create(:bid)    
      @venue_group  = FactoryGirl.create(:venue_group)

      @params = {}
      @params[:venue_group_id] = @venue_group.id
      @params[:event_id]       = @event.id
      @params[:price]          = 100
      @params[:quantity]       = 2
      @params[:cancelothers]   = true
      @params[:is_negotiation] = true
      @params[:expire_at]      = Faker::Date.forward(20)

      post :create, :event_id => @event.id, :bid => @params, :user_email => @user.email, :user_token => @user.authentication_token, :format => :json
    end

    it "should increase bid count by 1" do
      lambda do
      post :create, :event_id => @event.id, :bid => @params, :user_email => @user.email, :user_token => @user.authentication_token, :format => :json
      end.should change(Bid, :count).by(1)
    end

    it "sould respond successfully with an HTTP 201 status code" do      
      response.should be_success
      expect(response).to have_http_status(201)
    end

    it "should have validate attributes as not null" do
      body  = JSON.parse(response.body)
      bid   = body["result"]

      bid["id"].should_not be_nil
      bid["price"].should_not be_nil
      bid["quantity"].should_not be_nil
      bid["expire_at"].should_not be_nil
      bid["cancelothers"].should_not be_nil
      bid["is_negotiation"].should_not be_nil
      bid["user_id"].should_not be_nil
      bid["event_id"].should_not be_nil
    end

    it "should have correct event_id and user_id" do
      body  = JSON.parse(response.body)
      bid   = body["result"]
      bid["user_id"].should   == @user.id
      bid["event_id"].should  == @event.id
    end

  end


  describe "GET #index" do
    render_views

    before(:each) do
      @bid    = FactoryGirl.create(:bid) 
      get :index, :user_email => @user.email, :user_token => @user.authentication_token, event_id: @bid.event.id , :format => :json

    end

    it "should respond successfully with an HTTP 200 status code" do
      expect(response).to be_success
      expect(response).to have_http_status(200)
    end

    it "should have validate attributes as not null" do
      json  = JSON.parse(response.body)
      bids  = json["bids"]

      bids.each do |bid|

        bid = bid['bid']

        bid["id"].should_not be_nil
        bid["price"].should_not be_nil
        bid["quantity"].should_not be_nil
        bid["expire_at"].should_not be_nil
        bid["cancelothers"].should_not be_nil
        bid["is_negotiation"].should_not be_nil
        bid["user_id"].should_not be_nil
        bid["event_id"].should_not be_nil

      end
    end

    it "should be render by template" do
      expect(response).to render_template("index")
    end

  end

  describe "#show" do
    render_views

    before(:each) do
      @bid           =  FactoryGirl.create(:bid)
      @venue_group   =  FactoryGirl.create(:venue_group)

      @bid.groupable         = @venue_group
      @bid.groupable_type    = "VenueGroup"

      get :show, :user_email => @user.email, :user_token => @user.authentication_token, :id => @bid.id, :event_id => @bid.event.id,  :format => :json
    end

    it "should respond successfully with an HTTP 200 status code" do
      expect(response).to be_success
      expect(response).to have_http_status(200)
    end


    it "should be render by template" do
      expect(response).to render_template("show")
    end

    it "should have validate attributes as not null" do
      json  = JSON.parse(response.body)

      bid   = json["bid"]
      bid["id"].should_not be_nil
      bid["price"].should_not be_nil
      bid["quantity"].should_not be_nil
      bid["expire_at"].should_not be_nil
      bid["cancelothers"].should_not be_nil
      bid["is_negotiation"].should_not be_nil
      bid["user_id"].should_not be_nil
      bid["event_id"].should_not be_nil
    end

    it "should have valid user, event and venue_group in response" do
      json  = JSON.parse(response.body)

      json["user"].should_not be_nil
      json["event"].should_not be_nil
      json["groupable"].should_not be_nil
    end

  end


  describe "#update" do
    render_views

    it "responds successfully with an HTTP 204 status code" do
      bid           = FactoryGirl.create(:bid)
      params        = {}

      params[:price]          = 200
      params[:quantity]       = 5
      params[:cancelothers]   = false
      params[:is_negotiation] = true
      params[:expire_at]      = Faker::Date.forward(15)

      user         = FactoryGirl.create(:user)
      user.reset_authentication_token
      user.save
      
      put :update, :event_id => bid.event_id, :id => bid.id, :bid => params, :user_email => user.email, :user_token => user.authentication_token, :format => :json
      
      expect(response).to be_success
      expect(response).to have_http_status(204)
    end
  end


  describe "#delete" do
    render_views

    before(:each) do
      @bid       = FactoryGirl.create(:bid)      
      delete :destroy, :event_id => @bid.event_id, :id => @bid.id, :user_email => @user.email, :user_token => @user.authentication_token,  :format => :json
    end

    it "responds successfully with an HTTP 204 status code" do      
      expect(response).to be_success
      expect(response).to have_http_status(204)
    end

    it "should change count by -1" do 
      lambda do
        @bid       = FactoryGirl.create(:bid)  
        delete :destroy, :event_id => @bid.event_id, :id => @bid.id, :user_email => @user.email, :user_token => @user.authentication_token,  :format => :json
      end.should change(Bid, :count).by(-1)
    end

  end

  describe "GET #all_user_bids" do
    render_views

      before(:each) do
        @bid    = FactoryGirl.create(:bid) 
        get :all_user_bids, :user_email => @user.email, :user_token => @user.authentication_token, event_id: @bid.event.id , :format => :json
      end

    it "should respond successfully with an HTTP 200 status code" do
      expect(response).to be_success
      expect(response).to have_http_status(200)
    end

    it "should have validate attributes as not null" do
      json  = JSON.parse(response.body)
      bids  = json["bids"]

      bids.each do |bid|

        bid = bid['bid']

        bid["id"].should_not be_nil
        bid["price"].should_not be_nil
        bid["quantity"].should_not be_nil
        bid["expire_at"].should_not be_nil
        bid["cancelothers"].should_not be_nil
        bid["is_negotiation"].should_not be_nil
        bid["user_id"].should_not be_nil
        bid["event_id"].should_not be_nil

      end
    end

    it "should be render by template" do
      expect(response).to render_template("all_user_bids")
    end

  end


  describe "#get_notifications" do
    render_views

    before(:each) do
      @bid    = FactoryGirl.create(:bid) 
      get :get_notifications, event_id: @bid.event_id, bid_id: @bid.id, :user_email => @user.email, :user_token => @user.authentication_token, :format => :json

    end

    it "responds successfully with an HTTP 200 status code" do
      expect(response).to be_success      
      expect(response).to have_http_status(200)

    end

    it "should have offers array" do

      json    = JSON.parse(response.body)
      offers  = json["offers"]

      offers.each do |offer|

        offer = offer['offer']        
        expect(offer.class.name).to eq("Offer")
        offer["id"].should_not be_nil

      end

    end

    it "should be render by template" do
      expect(response).to render_template("get_notifications")
    end

  end


  describe "#buy_now" do
    render_views

    it "responds successfully with an HTTP 200 status code" do
      @venue_row   = FactoryGirl.create(:venue_row)
      @offer       = FactoryGirl.create(:offer, :venue_group => @venue_row.venue_group)
      @offer.offer_tickets << FactoryGirl.create(:offer_ticket, :venue_row => @venue_row)
      @bid         = FactoryGirl.create(:bid, :event_id => @offer.event_id, :groupable => @offer.venue_group)

      post :buy_now, :event_id => @bid.event_id, :offer_id => @offer.id, :bid_id => @bid.id, :user_email => @user.email, :user_token => @user.authentication_token, :format => :json

      expect(response).to be_success
      expect(response).to have_http_status(200)
    end
  end

  describe "#confirm_deal" do
    render_views
    it "responds successfully with an HTTP 200 status code" do
      @offer       = FactoryGirl.create(:offer)
      @bid         = FactoryGirl.create(:bid)

      get :confirm_deal, :user_email => @user.email, :user_token => @user.authentication_token, :event_id => @bid.event_id, :offer_id => @offer.id, :bid_id => @bid.id, :format => :json

      expect(response).to be_success
      expect(response).to have_http_status(201)
      expect(response).to render_template("confirm_deal")
    end
  end

end