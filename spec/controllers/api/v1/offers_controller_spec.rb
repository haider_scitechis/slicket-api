require 'rails_helper'

RSpec.describe Api::V1::OffersController, :type => :controller do
  let(:offer) { Offer.create! }
  render_views

   before do

      @user = FactoryGirl.create(:user)
      @user.reset_authentication_token
      @user.save

      @event       = FactoryGirl.create(:event)
   end

  describe "GET #index" do
    render_views
    before(:each) do
      get :index, :user_email => @user.email, :user_token => @user.authentication_token, event_id: @event.id,  :format => :json
    end

    it "should be an array" do

      json    = JSON.parse response.body
      offers  = json['offers']

      offers.each do |offer|

        offer   = offer['offer']
        offer["id"].should_not be_nil
        offer["venue_group_id"].should_not be_nil
        offer["user_id"].should_not be_nil
        offer["event_id"].should_not be_nil
        offer["ticket_type_id"].should_not be_nil
        offer["price"].should_not be_nil
        offer["cancelothers"].should_not be_nil
        offer["status"].should_not be_nil

      end
    end

    it "responds successfully with an HTTP 200 status code" do
      expect(response).to be_success
      expect(response).to have_http_status(200)
    end

    it "should be render by template" do
      expect(response).to render_template("index")
    end

  end

  describe "#create" do
    render_views

    before(:each) do

      @event       = FactoryGirl.create(:event)
      @venue_group = FactoryGirl.create(:venue_group)
      @ticket_type = FactoryGirl.create(:ticket_type)
      @commission  = FactoryGirl.create(:commission_shipping_config, :name => "commission", :amount => '10')
      @shipping    = FactoryGirl.create(:commission_shipping_config, :name => "shipping", :amount => '20')
      @eticket     = FactoryGirl.create(:commission_shipping_config, :name => "e-ticket", :amount => '0')

      @disclosures = {}
      @disclosures[:name] = "abc"
      @disclosures[:desc] = "skldfghskihf"

      post :create, :user_email => @user.email, :user_token => @user.authentication_token, disclosures:  @disclosures, venue_group_id: @venue_group.id, event_id: @event.id, ticket_type_id: @ticket_type.id, price:'100', proxy:'95', multiple: '1', cancelothers: '1' , seat_numbers: '1,2,3,4,54,5', :format => :json

    end

    it "should increase count by 1" do 
      lambda do
        post :create, :user_email => @user.email, :user_token => @user.authentication_token, disclosures:  @disclosures, venue_group_id: @venue_group.id, event_id: @event.id, ticket_type_id: @ticket_type.id, price:'100', proxy:'95', multiple: '1', cancelothers: '1' , seat_numbers: '1,2,3,4,54,5', :format => :json
      end.should change(Offer, :count).by(1)
    end

    it "should have validate attributes" do

      json = JSON.parse response.body

      offer  = json['offer']

      offer["id"].should_not be_nil
      offer["venue_group_id"].should_not be_nil
      offer["user_id"].should_not be_nil
      offer["event_id"].should_not be_nil
      offer["ticket_type_id"].should_not be_nil
      offer["price"].should_not be_nil
      offer["cancelothers"].should_not be_nil
#      offer["status"].should_not be_nil

    end

    it "responds successfully with an HTTP 201 status code" do
      expect(response).to be_success
      expect(response).to have_http_status(201)
    end


  end

 describe "#show" do

  before(:each) do
    @offer       = FactoryGirl.create(:offer)      
    get :show, :user_email => @user.email, :user_token => @user.authentication_token,  event_id: @offer.event_id, id: @offer.id, :format => :json
  end

  it "should have validate attributes" do 
      json = JSON.parse response.body

      offer  = json['offer']

      offer["id"].should_not be_nil
      offer["venue_group_id"].should_not be_nil
      offer["user_id"].should_not be_nil
      offer["event_id"].should_not be_nil
      offer["ticket_type_id"].should_not be_nil
      offer["price"].should_not be_nil
      offer["cancelothers"].should_not be_nil
      offer["status"].should_not be_nil
  end

  it "responds successfully with an HTTP 200 status code" do
    expect(response).to be_success
    expect(response).to have_http_status(200)
  end

  it "should be render by template" do
    expect(response).to render_template("show")
  end


  end


 describe "#update" do
    it "responds successfully with an HTTP 204 status code" do
      offer       = FactoryGirl.create(:offer)
      
      user         = FactoryGirl.create(:user)
      user.reset_authentication_token
      user.save
      
      put :update, :user_email => user.email, :user_token => user.authentication_token, id: offer.id, venue_group_id: offer.venue_group_id, event_id: offer.event_id, ticket_type_id: offer.ticket_type.id, price:'100', proxy:'95', multiple: '1', cancelothers: '1' , :format => :json

      expect(response).to be_success
      expect(response).to have_http_status(204)

    end
  end


 describe "#delete" do

    before(:each) do 
      @offer       = FactoryGirl.create(:offer, :event => @event)    
      delete :destroy, :user_email => @user.email, :user_token => @user.authentication_token,  event_id: @offer.event_id, id: @offer.id, :format => :json
    end

    it "responds successfully with an HTTP 204 status code" do
      expect(response).to be_success
      expect(response).to have_http_status(204)
    end

    it "should change Offer count by -1" do
      lambda do
        delete :destroy, :user_email => @user.email, :user_token => @user.authentication_token,  event_id: @offer.event_id, id: @offer.id, :format => :json
      end.should change(Offer, :count).by(-1)
    end

  end

  describe "GET #all_user_offers" do

    before(:each) do
      get :all_user_offers, :user_email => @user.email, :user_token => @user.authentication_token, event_id: @event.id , user_email: @user.email, :format => :json
    end

    it "should be an array of offers" do

      json = JSON.parse response.body

      offers  = json['offers']
      offers.each do |offer|

        offer   = offer['offer']
        offer["id"].should_not be_nil
        offer["venue_group_id"].should_not be_nil
        offer["user_id"].should_not be_nil
        offer["event_id"].should_not be_nil
        offer["ticket_type_id"].should_not be_nil
        offer["price"].should_not be_nil
        offer["cancelothers"].should_not be_nil
        offer["status"].should_not be_nil
      end

    end

    it "responds successfully with an HTTP 200 status code" do      
      expect(response).to be_success
      expect(response).to have_http_status(200)
    end

    it "should be render by template" do
      expect(response).to render_template("all_user_offers")
    end

  end
  
  describe "#list_offers" do

    before(:each) do
      @offer       = FactoryGirl.create(:offer)
      get :list_offers, :user_email => @user.email, :user_token => @user.authentication_token,  event_id: @offer.event_id, id: @offer.id, quantity: '10', :format => :json
    end

    it "should be an array of offers" do

      json = JSON.parse response.body

      offers  = json['offers']
      offers.each do |offer|

        offer   = offer['offer']
        offer["id"].should_not be_nil
        offer["venue_group_id"].should_not be_nil
        offer["user_id"].should_not be_nil
        offer["event_id"].should_not be_nil
        offer["ticket_type_id"].should_not be_nil
        offer["price"].should_not be_nil
        offer["cancelothers"].should_not be_nil
        offer["status"].should_not be_nil
      end

    end

    it "responds successfully with an HTTP 200 status code" do
      expect(response).to be_success
      expect(response).to have_http_status(200)
    end

  end


  describe "#get_notifications" do
    it "responds successfully with an HTTP 200 status code" do
      offer       = FactoryGirl.create(:offer)
      
      user         = FactoryGirl.create(:user)
      user.reset_authentication_token
      user.save
      
      get :get_notifications, :user_email => user.email, :user_token => user.authentication_token,  event_id: offer.event_id, offer_id: offer.id, :format => :json

      expect(response).to be_success
      expect(response).to have_http_status(200)
      expect(response).to render_template("get_notifications")


    end
  end

  describe "#sell_now" do
    it "responds successfully with an HTTP 200 status code" do
      venue_row = FactoryGirl.create(:venue_row)
      offer       = FactoryGirl.create(:offer, :venue_group => venue_row.venue_group)
      offer.offer_tickets << FactoryGirl.create(:offer_ticket, :venue_row => venue_row)
      bid         = FactoryGirl.create(:bid, :event_id => offer.event_id, :groupable => offer.venue_group)
      
      user         = FactoryGirl.create(:user)
      user.reset_authentication_token
      user.save
      
#      post :sell_now, :user_email => user.email, :user_token => user.authentication_token,  event_id: offer.event_id, offer_id: offer.id, bid_id: bid.id, :format => :json

#      expect(response).to be_success
#      expect(response).to have_http_status(200)


    end
  end

  describe "#confirm_deal" do
    it "responds successfully with an HTTP 200 status code" do
      offer       = FactoryGirl.create(:offer)
      bid         = FactoryGirl.create(:bid)
      
      user         = FactoryGirl.create(:user)
      user.reset_authentication_token
      user.save
      
      get :confirm_deal, :user_email => user.email, :user_token => user.authentication_token,  event_id: offer.event_id, offer_id: offer.id, bid_id: bid.id, :format => :json

      expect(response).to be_success
      expect(response).to have_http_status(201)
      expect(response).to render_template("confirm_deal")

    end
  end

end
