require 'rails_helper'

RSpec.describe Api::V1::NegotiationLogsController, :type => :controller do


  describe "GET #index" do
    it "responds successfully with an HTTP 200 status code" do
      negotiation_log = FactoryGirl.create(:negotiation_log)

      user         = FactoryGirl.create(:user)
      user.reset_authentication_token
      user.save
      
      get :index, :user_email => user.email, :user_token => user.authentication_token, :offer_id => negotiation_log.offer_id, :bid_id => negotiation_log.bid_id, :format => :json

      expect(response).to be_success
      expect(response).to have_http_status(200)

    end
  end

  describe "#create" do
    it "responds successfully with an HTTP 201 status code" do

      offer   = FactoryGirl.create(:offer)
      bid     = FactoryGirl.create(:bid)

      params  = {}
      params[:price]    = 1500
      params[:quantity] = 3

      user         = FactoryGirl.create(:user)
      user.reset_authentication_token
      user.save
      
      post :create, :negotiation_log =>  params, :user_email => user.email, :user_token => user.authentication_token, :offer_id => offer.id, :bid_id => bid.id, :flag => "bid", :negotiation_log => params, :format => :json

      expect(response).to be_success
      expect(response).to have_http_status(201)

    end
  end


 describe "#show" do

    it "responds successfully with an HTTP 200 status code" do
      negotiation_log = FactoryGirl.create(:negotiation_log)

      user         = FactoryGirl.create(:user)
      user.reset_authentication_token
      user.save
      
      get :show, :user_email => user.email, :user_token => user.authentication_token, :offer_id => negotiation_log.offer_id, :bid_id => negotiation_log.bid_id, :id => negotiation_log.id, :format => :json

      expect(response).to be_success
      expect(response).to have_http_status(200)

    end
  end


  describe "#delete" do
    it "responds successfully with an HTTP 204 status code" do
      negotiation_log = FactoryGirl.create(:negotiation_log)

      user         = FactoryGirl.create(:user)
      user.reset_authentication_token
      user.save
      
      delete :destroy, :user_email => user.email, :user_token => user.authentication_token, :offer_id => negotiation_log.offer_id, :bid_id => negotiation_log.bid_id,:id => negotiation_log.id, :format => :json

      expect(response).to be_success
      expect(response).to have_http_status(204)

    end
  end

end