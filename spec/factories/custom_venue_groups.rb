# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :custom_venue_group do |f|
  	f.name Faker::Name.name
    f.association(:user)
    f.association(:event)

  end
end