FactoryGirl.define  do
  factory :venue_section do

    raphael_data      "football"
    color             "RED"
    section_text      "ABC XYZ"
    section_number    1
    row_start         Random.rand(1..10)
    row_end           Random.rand(10..20)

    association(:venue)
#    association(:disclosure)

  end
end
