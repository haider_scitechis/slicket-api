# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :email_alert_config_option do

    association(:user)
    association(:email_alert_config)
  end
end
