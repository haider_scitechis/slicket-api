# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
    factory :bid do |f|
        f.price Random.rand(3000)
        f.quantity Random.rand(10)
        f.expire_at Faker::Date.forward(20)
        f.cancelothers Random.rand(0..1)
        f.status true
        f.is_negotiation true
        f.association(:user)
        f.association(:event)
        f.association :groupable, factory: :venue_group
    end
end