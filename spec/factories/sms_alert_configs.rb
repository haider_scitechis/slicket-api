# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :sms_alert_config do

    option "Do you want to receive sms ?"
    type "SmsAlertConfig"

  end
end
