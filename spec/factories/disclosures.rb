# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :disclosure do |f|

    f.name { Faker::Name.name  }
    f.desc { Faker::Lorem.paragraph }

  end
end
