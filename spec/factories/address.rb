FactoryGirl.define do
  factory :address do
    sequence(:name) { |n| "John#{n * Random.rand}'s home" }
    address1  { "#{name} Address 1" }
    address2 { "#{name} Address 2" }
    city "Lahore"
    state "someStatename"
    zip "10010"
    sequence(:phone) { |n| "03211111475{n}" }

    association :user, factory: :user, strategy: :create
  end
end