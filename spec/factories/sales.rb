# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :sale do
    price     1.5
    quantity  1
    association :bid
    association :offer
    association :ticket_status
    association :coupon
  end
end
