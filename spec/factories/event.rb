FactoryGirl.define do
  factory :event do |f|

    f.name { Faker::Name.name  }
    f.date { Faker::Date.backward(Random.rand(20)).to_date }
    f.description { Faker::Lorem.paragraph }
    f.primary { true }
    f.primary_pdf { Faker::Avatar.image }
    f.setgeek_id { Random.rand(20) }
    f.event_type { Faker::Hacker.abbreviation  }

    f.association(:venue)
    f.association(:event_category)

  end
end
