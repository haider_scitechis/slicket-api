# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :offer_disclosure do
    offer nil
    disclosure nil
  end
end
