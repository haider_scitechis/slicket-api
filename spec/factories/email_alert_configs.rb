# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :email_alert_config do
    option "Do you want to receive emails ?"
    type "EmailAlertConfig"
  end
end
