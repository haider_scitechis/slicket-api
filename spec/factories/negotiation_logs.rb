# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :negotiation_log do |f|

    f.association(:bid)
    f.association(:offer)
    f.quantity Random.rand(10)
    f.price Random.rand(10)

  end
end