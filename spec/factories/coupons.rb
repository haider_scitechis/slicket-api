# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :coupon do

    coupon_number "MyString"
    amount 1.5
    is_percentage false

  end
end
