# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do

  factory :offer do
    price                   1
    proxy                   1
    multiple                1
    cancelothers            1
    status                  "MyString"
    in_hand_date            "2014-10-23"
    expected_ship_date      "2014-10-23"
    is_negotiation          1
    expires_at              "2014-10-23 12:48:50"
    confirm_transtaction    1
    piggybacked             1
    notes                   "MyText"
    association(:ticket_type)
    association(:user)
    association(:event)
    association(:venue_group)

  end
end