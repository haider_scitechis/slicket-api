FactoryGirl.define do
  factory :venue_row do

    sequence(:name) {|n| "Front#{n}" }
    seats Random.rand(0..100)
    row_number Random.rand(0..100)

    association(:venue_group)
    association(:event_section)

  end
end