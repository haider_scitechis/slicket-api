# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :uploaded_ticket do
    association(:offer_ticket)
  end
end
