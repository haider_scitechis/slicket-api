FactoryGirl.define  do
    factory :venue  do |f|

        f.venue         Faker::Address.city
        f.team          "Manchester United"
        f.city          Faker::Address.city
        f.state         Faker::Address.state
        f.zip           Faker::Address.zip 
        f.phone         Faker::PhoneNumber.cell_phone
        f.description   Faker::Lorem.paragraph
        f.team_name     "Manchester United"
        f.school        "ABC"
        f.mascot        "abc"
        f.seatgeek_id       Random.rand(20)
        f.latitude          Faker::Address.latitude
        f.longitude         Faker::Address.longitude
        f.street_address    Faker::Address.street_address

        factory :venue_sections do
          after(:create) do |venue|
            create(:venue_section, venue: venue)
          end
        end

    end
end