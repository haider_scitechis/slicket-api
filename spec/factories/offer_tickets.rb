# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :offer_ticket do
    venue_row_id        1
    sale_id             1
    status              true
    seat_number         1
    barcode             "MyText"
    attendee_name       "MyString"
    association(:offer)
  end
end
