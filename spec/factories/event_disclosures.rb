# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :event_disclosure do
    event_section nil
    disclosure nil
  end
end
