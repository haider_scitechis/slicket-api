FactoryGirl.define do
  factory :user do

    sequence(:first_name) {|n| "John#{n}" }
    last_name  "Doe"
    sequence(:email) { |n| "john#{ n * Random.rand }@example.com!" }
    password "hellohello"
    sequence(:username) { |n| "John#{n}" }
    title "someTitle"

    factory :user_addresses do
      after(:create) do |user|
        create(:address, user: user)
      end
    end

  end
end