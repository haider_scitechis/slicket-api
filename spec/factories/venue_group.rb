FactoryGirl.define  do
  factory :venue_group do
    name Faker::Name.name
    rating 5
    association(:event)
  end
end