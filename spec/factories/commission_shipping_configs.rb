# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :commission_shipping_config do
    name "shipping"
    amount 1.5
  end
end
