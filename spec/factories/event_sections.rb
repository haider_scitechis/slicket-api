# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :event_section do

    association(:venue_section)
    association(:event)

  end
end