# => sudo apt-get install pdftk # Ubuntu

User.create :email => 'muhammad.haris.4063@gmail.com', password: 'hellohello', title: 'Saad M.', username: 'SM', :first_name => Faker::Name.name, :last_name => Faker::Name.name, :user_type => "admin"



Venue.create :street_address => Faker::Address.street_address, :city => Faker::Address.city, :state => Faker::Address.state, :zip => Faker::Address.zip, :phone => Faker::PhoneNumber.phone_number, :description => Faker::Lorem.paragraph, :latitude => Faker::Address.latitude, :longitude => Faker::Address.longitude, :venue => "ABC", :team => "ABC", :phone => Faker::PhoneNumber.extension 



Array(1..5).each do |no|

  Category.create :category => "ABC#{no}"

  Array(1..3).each do |sub_category|
    Category.last.subcategories.create :category => "subcategory#{sub_category}"
    EventCategory.create :name => "event#{sub_category}", :category_id => Category.last.id
  end

end

row_sequence = 0

Array(1..50).each do |section_no| 
  Venue.last.venue_sections.create( :raphael_data => "ABC", :color => Faker::Commerce.color, :section_number => section_no, :row_start => row_sequence, :row_end => row_sequence + 100, :photo => "/home/haider/" )
  
  row_sequence += 100
end

Array(1..50).each do
  Event.create :name => Faker::Name.name, :date=> Faker::Date.forward(10), :description => Faker::Lorem.paragraph, :primary => true, :primary_pdf => Faker::Avatar.image, :setgeek_id => Random.rand(20), :event_type => Faker::Hacker.abbreviation, :venue => Venue.last, :lat => Faker::Address.latitude, :lang => Faker::Address.longitude
end



url     = "Sample_Tickets_File_1.csv"
data    = SmarterCSV.process(url)

data.each do |item|
  Event.create :name => item[:event], :date=> Date.parse(item[:eventdate]), :description => Faker::Lorem.paragraph, :primary => true, :primary_pdf => Faker::Avatar.image, :setgeek_id => Random.rand(20), :event_type => Faker::Hacker.abbreviation, :venue => Venue.last, :lat => Faker::Address.latitude, :lang => Faker::Address.longitude
end

Array(1..5).each do |no|
  VenueGroup.create(:event => Event.last, :rating => no)
end

Array(1..49).each do |no|
  if rand(10)%2 == 0
    EventSection.create(:event => Event.last, :venue_section => VenueSection.all[no])
    Array(1..30).each do |row_number|
      VenueRow.create(:row_number => row_number,  :event_section => EventSection.last, :seats => 100, :name => "#{Faker::Name.name} povillion end", :venue_group => VenueGroup.all[rand(0..4)])
    end
  end
end



TicketType.create(:name => "pdf")
TicketType.create(:name => "fedex")

Array(1..10).each do |offer_no|

  Offer.create(:venue_group => VenueGroup.find(rand(1..5)), :event => Event.last, :user => User.last, :ticket_type => TicketType.last, :price =>  rand(300 .. 700), :multiple => rand(0..1), :cancelothers => 1)
  Array(1..10).each do |offer_ticket_no|
    OfferTicket.create(:offer => Offer.last, :seat_number => rand(100), :venue_row => Offer.last.venue_group.venue_rows.all[rand(0..Offer.last.venue_group.venue_rows.count-1)], :barcode => Faker::Code.isbn)
  end
end

Array(1..10).each do |no|
  Bid.create(:price => rand(300..700), :quantity => rand(1..10), :expire_at => Faker::Date.forward(10), :cancelothers => 1, :is_negotiation => 1, :user => User.find(rand(1..User.count)), :event => Event.find(rand(1..Event.count)), :groupable => VenueGroup.find(rand(1..VenueGroup.count)), :groupable_type => "VenueGroup")
end

@user = User.first
@user.commission_shipping_configs.create(:name => "commission", :amount => 10.0, :action_type => "seller")
@user.commission_shipping_configs.create(:name => "shipping", :amount => 20.0, :action_type => "seller")
@user.commission_shipping_configs.create(:name => "e-ticket", :amount => 0.0, :action_type => "seller")
@user.commission_shipping_configs.create(:name => "commission", :amount => 15.0, :action_type => "buyer")
@user.commission_shipping_configs.create(:name => "shipping", :amount => 20.0, :action_type => "buyer")
@user.commission_shipping_configs.create(:name => "e-ticket", :amount => 0.0, :action_type => "buyer")


Offer.all.each do |offer|
  Disclosure.create(:name => "for_of#{offer.id}", :disclosureable_id => Offer.find(rand(1..Offer.count)), :disclosureable_type => "Offer")
end
EventSection.all.each do |event_section|
  Disclosure.create(:name => "for_es#{event_section.id}", :disclosureable_id => EventSection.find(rand(1..EventSection.count)), :disclosureable_type => "EventSection")
end


VenueGroup.all.each do |aVenue|
  if aVenue.offers  
    aVenue.offers.each do |aOffer|    
      if aVenue.bids      
        aVenue.bids.each do |aBid|        
          if aBid.groupable.id == aOffer.venue_group.id          
            noti = Notification.create(:sendable => aOffer.user, :notifiable => aOffer)
            notiRec = noti.recipients.build(:recievable => aBid.user)
            notiRec.content = "Offer is Created by this #{aOffer.user.first_name} #{aOffer.user.last_name} user in VenueGroup #{aVenue.name} where you place a bid"
            noti.save
          end  
        end  
      end  
    end  
  end
  if aVenue.bids
    aVenue.bids.each do |aBid|
      if aVenue.offers
        aVenue.offers.each do |aOffer|
          if aOffer.venue_group.id == aBid.groupable.id
            noti = Notification.create(:sendable => aBid.user, :notifiable => aBid)
            notiRec = noti.recipients.build(:recievable => aOffer.user)
            notiRec.content = "Bid is Created by this #{aBid.user.first_name} #{aBid.user.last_name} user in VenueGroup #{aVenue.name} where you place a offer"
            noti.save
          end
        end
      end
    end
  end  
end

user = User.first

EmailAlertConfig.create :option => "on bid match"
EmailAlertConfig.create :option => "on offer match"
EmailAlertConfig.create :option => "buy now"
EmailAlertConfig.create :option => "sell now"

EmailAlertConfig.all.each do |email_alert_config|
  EmailAlertConfigOption.create :user_id => user.id, :email_alert_config_id => email_alert_config.id
end

SmsAlertConfig.create :option => "on bid match"
SmsAlertConfig.create :option => "on offer match"
SmsAlertConfig.create :option => "buy now"
SmsAlertConfig.create :option => "sell now"

SmsAlertConfig.all.each do |sms_alert_config|
  SmsAlertConfigOption.create :user_id => user.id, :sms_alert_config_id => sms_alert_config.id
end