class ConvertSubscribeUserToBoolean < ActiveRecord::Migration
  def change
  	remove_column :users, :subscribe, :integer
  	add_column :users, :subscribe, :boolean
  end
end
