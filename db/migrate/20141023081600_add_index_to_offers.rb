class AddIndexToOffers < ActiveRecord::Migration
  def change
    add_index :offers, :user_id
    add_index :offers, :venue_group_id
    add_index :offers, :event_id
    add_index :offers, :ticket_type_id
  end
end
