class CreateOfferTickets < ActiveRecord::Migration
  def change
    create_table :offer_tickets do |t|
      t.integer :offer_id
      t.integer :venue_row_id
      t.integer :sale_id
      t.string :status
      t.integer :seat_number
      t.text :barcode
      t.string :attendee_name

      t.timestamps
    end
    add_index :offer_tickets, :offer_id
    add_index :offer_tickets, :venue_row_id
    add_index :offer_tickets, :sale_id
  end
end
