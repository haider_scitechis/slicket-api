class CreateCustomVenueRows < ActiveRecord::Migration
  def change
    create_table :custom_venue_rows do |t|
      t.integer :custom_venue_group_id
      t.integer :venue_row_id

      t.timestamps
    end
  end
end
