class CreateOfferDisclosures < ActiveRecord::Migration
  def change
    create_table :offer_disclosures do |t|
      t.belongs_to :offer, index: true
      t.belongs_to :disclosure, index: true

      t.timestamps
    end
  end
end
