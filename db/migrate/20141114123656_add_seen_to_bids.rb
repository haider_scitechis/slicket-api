class AddSeenToBids < ActiveRecord::Migration
  def change
    add_column :bids, :notification_seen, :boolean, default: false
  end
end
