class CreateSmsAlertConfigOptions < ActiveRecord::Migration
  def change
    create_table :sms_alert_config_options do |t|
      t.integer :user_id
      t.integer :sms_alert_config_id

      t.timestamps
    end
  end
end
