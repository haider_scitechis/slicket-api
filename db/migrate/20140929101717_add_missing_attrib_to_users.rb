class AddMissingAttribToUsers < ActiveRecord::Migration
  def change
    add_column :users, :first_name, :string
    add_column :users, :middle_name, :string
    add_column :users, :last_name, :string
    add_column :users, :phone, :string
    add_column :users, :phone_daytime, :string
    add_column :users, :phone_evening, :string
    add_column :users, :phone_cell, :string
  end
end
