class CreateCustomVenueGroups < ActiveRecord::Migration
  def change
    create_table :custom_venue_groups do |t|
      t.string :name

      t.timestamps
    end
  end
end
