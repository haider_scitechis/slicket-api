class CreateUploadedTickets < ActiveRecord::Migration
  def change
    create_table :uploaded_tickets do |t|
      t.integer :offer_ticket_id

      t.timestamps
    end
    add_index :uploaded_tickets, :offer_ticket_id
  end
end
