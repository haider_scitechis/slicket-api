class AddAttachmentToUploadedTicket < ActiveRecord::Migration
  def change
    add_attachment :uploaded_tickets, :ticket
  end
end
