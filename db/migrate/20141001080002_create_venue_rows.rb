class CreateVenueRows < ActiveRecord::Migration
  def change
    create_table :venue_rows do |t|
      t.integer :event_section_id
      t.integer :venue_group_id
      t.string :name
      t.integer :seats

      t.timestamps
    end
    add_index :venue_rows, :event_section_id
    add_index :venue_rows, :venue_group_id
  end
end
