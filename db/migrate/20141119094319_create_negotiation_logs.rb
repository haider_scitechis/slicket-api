class CreateNegotiationLogs < ActiveRecord::Migration
  def change
    create_table :negotiation_logs do |t|
    	t.references :negotiable, polymorphic: true
    	t.integer :bid_id
    	t.integer :offer_id
    	t.integer :quantity
    	t.float :price
      t.timestamps
    end
  end
end
