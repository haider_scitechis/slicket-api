class CreateAlertConfigs < ActiveRecord::Migration
  def change
    create_table :alert_configs do |t|
      t.string :option
      t.string :type
      t.timestamps
    end
  end
end
