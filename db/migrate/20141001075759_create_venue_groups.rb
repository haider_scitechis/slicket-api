class CreateVenueGroups < ActiveRecord::Migration
  def change
    create_table :venue_groups do |t|
      t.string :name
      t.integer :rating
      t.integer :event_id

      t.timestamps
    end
    add_index :venue_groups, :event_id
  end
end
