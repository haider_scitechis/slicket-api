class CreateTicketStatuses < ActiveRecord::Migration
  def change
    create_table :ticket_statuses do |t|
      t.integer :name

      t.timestamps
    end
  end
end
