class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|

        t.string    :name
        t.date  :date
        t.time  :time
        t.text      :description
        t.boolean   :primary
        t.string    :primary_pdf
        t.string    :setgeek_id
        t. integer  :venue_id
        t.timestamps

    end
  end
end