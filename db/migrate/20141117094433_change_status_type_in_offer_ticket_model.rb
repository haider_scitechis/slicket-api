class ChangeStatusTypeInOfferTicketModel < ActiveRecord::Migration
	
	def change
		change_column :offer_tickets, :status, :boolean, :default => true	
	end
end
