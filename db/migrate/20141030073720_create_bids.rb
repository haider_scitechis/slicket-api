class CreateBids < ActiveRecord::Migration
  def change
    create_table :bids do |t|

#   t.belongs_to   :venue_group
    t.belongs_to 	:event
    t.belongs_to 	:user
    t.integer       :groupable_id
    t.string        :groupable_type
    t.integer 		:price
    t.integer		:quantity
    t.integer 		:proxy
    t.datetime		:expire_at
    t.integer 		:cancelothers
    t.string		:status
    t.datetime		:in_hand_date
    t.boolean		:is_negotiation
    t.datetime		:expacted_ship_date
    t.timestamps
    end
  end
end
