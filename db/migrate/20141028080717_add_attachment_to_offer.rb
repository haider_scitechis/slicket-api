class AddAttachmentToOffer < ActiveRecord::Migration
  def change
    add_attachment :offers, :tickets
  end
end
