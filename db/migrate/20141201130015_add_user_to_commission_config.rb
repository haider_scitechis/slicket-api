class AddUserToCommissionConfig < ActiveRecord::Migration
  def change
    add_column :commission_shipping_configs, :user_id, :integer
    add_column :commission_shipping_configs, :action_type, :string
  end
end
