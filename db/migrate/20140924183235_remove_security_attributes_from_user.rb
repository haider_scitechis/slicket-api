class RemoveSecurityAttributesFromUser < ActiveRecord::Migration
  def change
  	remove_column :users, :security_question_1_id
  	remove_column :users, :security_answer_1
  end
end
