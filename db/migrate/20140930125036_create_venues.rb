class CreateVenues < ActiveRecord::Migration
  def change
    create_table :venues do |t|
      t.string :category
      t.string :subcategory
      t.string :venue
      t.string :street_address
      t.string :team
      t.string :city
      t.string :state
      t.string :zip
      t.string :phone
      t.text :description
      t.string :school
      t.string :team_name
      t.string :mascot
      t.integer :seatgeek_id

      t.timestamps
    end
  end
end
