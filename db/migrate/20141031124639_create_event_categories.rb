class CreateEventCategories < ActiveRecord::Migration
  def change
    create_table :event_categories do |t|
      t.string :name
      t.belongs_to :category

      t.timestamps
    end
  end
end
