class AddAttributesToUser < ActiveRecord::Migration
	def change
		change_table(:users) do |t|
			t.integer :subscribe
			t.string :security_question_1_id
			t.string :security_answer_1
			t.integer :sale_commission
			t.integer :negotiation_commission
		end
	end
end
