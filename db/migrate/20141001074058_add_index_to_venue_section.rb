class AddIndexToVenueSection < ActiveRecord::Migration
  def change
  	add_index :venue_sections, :venue_id
  end
end
