class AddAnswersToSecurityAnswers < ActiveRecord::Migration
  def change
  	add_column :security_answers, :user_id, :integer
  	add_column :security_answers, :question_id, :integer
  	add_column :security_answers, :answer, :string
  end
end
