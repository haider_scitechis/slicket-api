class CreateEventSections < ActiveRecord::Migration
  def change
    create_table :event_sections do |t|

     	t.belongs_to :event
     	t.belongs_to :venue_section
    	t.timestamps
    end
  end
end
