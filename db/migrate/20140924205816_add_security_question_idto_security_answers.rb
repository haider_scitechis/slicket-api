class AddSecurityQuestionIdtoSecurityAnswers < ActiveRecord::Migration
  def change
  	remove_column :security_answers , :question_id, :integer
  	add_column :security_answers, :security_question_id, :integer
  end
end
