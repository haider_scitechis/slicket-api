class CreateSales < ActiveRecord::Migration
  def change
    create_table :sales do |t|
      t.belongs_to :bid, index: true
      t.belongs_to :offer, index: true
      t.belongs_to :ticket_status, index: true
      t.belongs_to :coupon, index: true
      t.float :price
      t.integer :quantity

      t.timestamps
    end
  end
end
