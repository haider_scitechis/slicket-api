class CreateEventDisclosures < ActiveRecord::Migration
  def change
    create_table :event_disclosures do |t|
      t.belongs_to :event_section, index: true
      t.belongs_to :disclosure, index: true

      t.timestamps
    end
  end
end
