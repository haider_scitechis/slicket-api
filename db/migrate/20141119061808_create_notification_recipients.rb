class CreateNotificationRecipients < ActiveRecord::Migration
  def change
    create_table :notification_recipients do |t|
    	t.references :recievable, polymorphic: true
		t.boolean :did_read, default: false
		t.string :content
		t.belongs_to :notification
      t.timestamps
    end
  end
end
