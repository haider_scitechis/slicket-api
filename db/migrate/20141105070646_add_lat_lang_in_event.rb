class AddLatLangInEvent < ActiveRecord::Migration
  def change

  	add_column :events, :lat, :double
  	add_column :events, :lang, :double

  end
end
