class CreateOffers < ActiveRecord::Migration
  def change
    create_table :offers do |t|
      t.integer :venue_group_id
      t.integer :event_id
      t.integer :user_id
      t.integer :ticket_type_id
      t.float :price
      t.float :proxy
      t.integer :multiple
      t.integer :cancelothers
      t.string :status
      t.date :in_hand_date
      t.date :expected_ship_date
      t.integer :is_negotiation
      t.datetime :expires_at
      t.integer :confirm_transtaction
      t.integer :piggybacked
      t.text :notes

      t.timestamps
    end
  end
end
