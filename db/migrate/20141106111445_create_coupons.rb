class CreateCoupons < ActiveRecord::Migration
  def change
    create_table :coupons do |t|
      t.string :coupon_number
      t.float :amount
      t.boolean :is_percentage

      t.timestamps
    end
    add_index :coupons, :coupon_number
  end
end
