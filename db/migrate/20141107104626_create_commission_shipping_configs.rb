class CreateCommissionShippingConfigs < ActiveRecord::Migration
  def change
    create_table :commission_shipping_configs do |t|
      t.string :name
      t.float :amount

      t.timestamps
    end
  end
end
