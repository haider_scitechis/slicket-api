class CreateAuthorizations < ActiveRecord::Migration
  def change
    create_table :authorizations do |t|

      t.integer :user_id
      t.string  :provider
      t.string  :token
      t.string  :uid
      t.string  :secret
      t.string  :profile_page
      t.timestamps

    end
  end
end
