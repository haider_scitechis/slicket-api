class CreateEmailAlertConfigOptions < ActiveRecord::Migration
  def change
    create_table :email_alert_config_options do |t|
      t.integer :user_id
      t.integer :email_alert_config_id

      t.timestamps
    end
  end
end
