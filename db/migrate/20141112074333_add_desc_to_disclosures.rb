class AddDescToDisclosures < ActiveRecord::Migration
  def change
    add_column :disclosures, :desc, :text
    add_column :disclosures, :disclosureable_id, :integer
    add_column :disclosures, :disclosureable_type, :string
  end
end
