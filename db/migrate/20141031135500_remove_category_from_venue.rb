class RemoveCategoryFromVenue < ActiveRecord::Migration
  def change
    remove_column :venues, :category, :string
    remove_column :venues, :subcategory, :string
  end
end
