# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20141201130015) do

  create_table "addresses", force: true do |t|
    t.integer  "user_id"
    t.string   "name"
    t.string   "address1"
    t.string   "address2"
    t.string   "city"
    t.string   "state"
    t.string   "zip"
    t.string   "phone"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "addresses", ["user_id"], name: "index_addresses_on_user_id", using: :btree

  create_table "alert_configs", force: true do |t|
    t.string   "option"
    t.string   "type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "authorizations", force: true do |t|
    t.integer  "user_id"
    t.string   "provider"
    t.string   "token"
    t.string   "uid"
    t.string   "secret"
    t.string   "profile_page"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "bids", force: true do |t|
    t.integer  "event_id"
    t.integer  "user_id"
    t.integer  "groupable_id"
    t.string   "groupable_type"
    t.integer  "price"
    t.integer  "quantity"
    t.integer  "proxy"
    t.datetime "expire_at"
    t.integer  "cancelothers"
    t.string   "status"
    t.datetime "in_hand_date"
    t.boolean  "is_negotiation"
    t.datetime "expacted_ship_date"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "notification_seen",  default: false
  end

  create_table "categories", force: true do |t|
    t.string   "category"
    t.integer  "parent_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "commission_shipping_configs", force: true do |t|
    t.string   "name"
    t.float    "amount"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "user_id"
    t.string   "action_type"
  end

  create_table "coupons", force: true do |t|
    t.string   "coupon_number"
    t.float    "amount"
    t.boolean  "is_percentage"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "coupons", ["coupon_number"], name: "index_coupons_on_coupon_number", using: :btree

  create_table "custom_venue_groups", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "event_id"
    t.integer  "user_id"
  end

  create_table "custom_venue_rows", force: true do |t|
    t.integer  "custom_venue_group_id"
    t.integer  "venue_row_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "disclosures", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "desc"
    t.integer  "disclosureable_id"
    t.string   "disclosureable_type"
  end

  create_table "email_alert_config_options", force: true do |t|
    t.integer  "user_id"
    t.integer  "email_alert_config_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "event_categories", force: true do |t|
    t.string   "name"
    t.integer  "category_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "event_disclosures", force: true do |t|
    t.integer  "event_section_id"
    t.integer  "disclosure_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "event_disclosures", ["disclosure_id"], name: "index_event_disclosures_on_disclosure_id", using: :btree
  add_index "event_disclosures", ["event_section_id"], name: "index_event_disclosures_on_event_section_id", using: :btree

  create_table "event_sections", force: true do |t|
    t.integer  "event_id"
    t.integer  "venue_section_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "events", force: true do |t|
    t.string   "name"
    t.date     "date"
    t.time     "time"
    t.text     "description"
    t.boolean  "primary"
    t.string   "primary_pdf"
    t.string   "setgeek_id"
    t.integer  "venue_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "event_type"
    t.integer  "event_category_id"
    t.float    "lat"
    t.float    "lang"
  end

  add_index "events", ["venue_id"], name: "index_events_on_venue_id", using: :btree

  create_table "negotiation_logs", force: true do |t|
    t.integer  "negotiable_id"
    t.string   "negotiable_type"
    t.integer  "bid_id"
    t.integer  "offer_id"
    t.integer  "quantity"
    t.float    "price"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "notification_recipients", force: true do |t|
    t.integer  "recievable_id"
    t.string   "recievable_type"
    t.boolean  "did_read",        default: false
    t.string   "content"
    t.integer  "notification_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "notifications", force: true do |t|
    t.integer  "notifiable_id"
    t.string   "notifiable_type"
    t.integer  "sendable_id"
    t.string   "sendable_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "offer_disclosures", force: true do |t|
    t.integer  "offer_id"
    t.integer  "disclosure_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "offer_disclosures", ["disclosure_id"], name: "index_offer_disclosures_on_disclosure_id", using: :btree
  add_index "offer_disclosures", ["offer_id"], name: "index_offer_disclosures_on_offer_id", using: :btree

  create_table "offer_tickets", force: true do |t|
    t.integer  "offer_id"
    t.integer  "venue_row_id"
    t.integer  "sale_id"
    t.boolean  "status",        default: true
    t.integer  "seat_number"
    t.text     "barcode"
    t.string   "attendee_name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "offer_tickets", ["offer_id"], name: "index_offer_tickets_on_offer_id", using: :btree
  add_index "offer_tickets", ["sale_id"], name: "index_offer_tickets_on_sale_id", using: :btree
  add_index "offer_tickets", ["venue_row_id"], name: "index_offer_tickets_on_venue_row_id", using: :btree

  create_table "offers", force: true do |t|
    t.integer  "venue_group_id"
    t.integer  "event_id"
    t.integer  "user_id"
    t.integer  "ticket_type_id"
    t.float    "price"
    t.float    "proxy"
    t.integer  "multiple"
    t.integer  "cancelothers"
    t.string   "status"
    t.date     "in_hand_date"
    t.date     "expected_ship_date"
    t.integer  "is_negotiation"
    t.datetime "expires_at"
    t.integer  "confirm_transtaction"
    t.integer  "piggybacked"
    t.text     "notes"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "tickets_file_name"
    t.string   "tickets_content_type"
    t.integer  "tickets_file_size"
    t.datetime "tickets_updated_at"
  end

  add_index "offers", ["event_id"], name: "index_offers_on_event_id", using: :btree
  add_index "offers", ["ticket_type_id"], name: "index_offers_on_ticket_type_id", using: :btree
  add_index "offers", ["user_id"], name: "index_offers_on_user_id", using: :btree
  add_index "offers", ["venue_group_id"], name: "index_offers_on_venue_group_id", using: :btree

  create_table "payments", force: true do |t|
    t.string   "status"
    t.float    "amount"
    t.string   "transaction_number"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "sales", force: true do |t|
    t.integer  "bid_id"
    t.integer  "offer_id"
    t.integer  "ticket_status_id"
    t.integer  "coupon_id"
    t.float    "price"
    t.integer  "quantity"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "sales", ["bid_id"], name: "index_sales_on_bid_id", using: :btree
  add_index "sales", ["coupon_id"], name: "index_sales_on_coupon_id", using: :btree
  add_index "sales", ["offer_id"], name: "index_sales_on_offer_id", using: :btree
  add_index "sales", ["ticket_status_id"], name: "index_sales_on_ticket_status_id", using: :btree

  create_table "security_answers", force: true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "user_id"
    t.string   "answer"
    t.integer  "security_question_id"
  end

  create_table "security_questions", force: true do |t|
    t.string   "question"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "sms_alert_config_options", force: true do |t|
    t.integer  "user_id"
    t.integer  "sms_alert_config_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "ticket_statuses", force: true do |t|
    t.integer  "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "ticket_types", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "uploaded_tickets", force: true do |t|
    t.integer  "offer_ticket_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "ticket_file_name"
    t.string   "ticket_content_type"
    t.integer  "ticket_file_size"
    t.datetime "ticket_updated_at"
  end

  add_index "uploaded_tickets", ["offer_ticket_id"], name: "index_uploaded_tickets_on_offer_ticket_id", using: :btree

  create_table "users", force: true do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: ""
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "authentication_token"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.integer  "sale_commission"
    t.integer  "negotiation_commission"
    t.string   "title"
    t.boolean  "subscribe"
    t.string   "username"
    t.string   "first_name"
    t.string   "middle_name"
    t.string   "last_name"
    t.string   "phone"
    t.string   "phone_daytime"
    t.string   "phone_evening"
    t.string   "phone_cell"
    t.string   "invitation_token"
    t.datetime "invitation_created_at"
    t.datetime "invitation_sent_at"
    t.datetime "invitation_accepted_at"
    t.integer  "invitation_limit"
    t.integer  "invited_by_id"
    t.string   "invited_by_type"
    t.string   "user_type"
  end

  add_index "users", ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true, using: :btree
  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["invitation_token"], name: "index_users_on_invitation_token", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  add_index "users", ["username"], name: "index_users_on_username", using: :btree

  create_table "venue_groups", force: true do |t|
    t.string   "name"
    t.integer  "rating"
    t.integer  "event_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "venue_groups", ["event_id"], name: "index_venue_groups_on_event_id", using: :btree

  create_table "venue_rows", force: true do |t|
    t.integer  "event_section_id"
    t.integer  "venue_group_id"
    t.string   "name"
    t.integer  "seats"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "row_number"
  end

  add_index "venue_rows", ["event_section_id"], name: "index_venue_rows_on_event_section_id", using: :btree
  add_index "venue_rows", ["venue_group_id"], name: "index_venue_rows_on_venue_group_id", using: :btree

  create_table "venue_sections", force: true do |t|
    t.integer  "venue_id"
    t.string   "raphael_data"
    t.string   "color"
    t.string   "section_text"
    t.string   "section_number"
    t.string   "row_start"
    t.string   "row_end"
    t.string   "photo"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "venue_sections", ["venue_id"], name: "index_venue_sections_on_venue_id", using: :btree

  create_table "venues", force: true do |t|
    t.string   "venue"
    t.string   "street_address"
    t.string   "team"
    t.string   "city"
    t.string   "state"
    t.string   "zip"
    t.string   "phone"
    t.text     "description"
    t.string   "school"
    t.string   "team_name"
    t.string   "mascot"
    t.integer  "seatgeek_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "latitude"
    t.string   "longitude"
  end

end
