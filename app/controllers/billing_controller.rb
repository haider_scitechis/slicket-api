# app/controllers/billing_controller.rb 
class BillingController < ApplicationController
  helper :authorize_net

  before_filter :get_order, :only => [:checkout, :authorize, :thank_you]

  def checkout
    # ASSUMPTION order is valid means amount is entered

    @transaction = @order.setup_transaction(
      {:link_text => 'Continue',
        :link_url => confirm_billing_url(@order)})
  end

  ## CALL BACK
  def authorize
    resp = AuthorizeNet::SIM::Response.new(params)
    if resp.approved?
      @order.status = Payment::SUCCESS
      @order.transaction_num = resp.transaction_id
    else
      @order.status = Payment::FAILED
    end
    @order.save(:validate => false)
    redirect_to thank_you_billing_url(@order)
  end

  private
  def auth
    Payment.conf
  end

  def get_order
    @order = Payment.find_by_id(params[:id])
    @order && @order.valid? || invalid_url
  end
end