
class Api::V1::UploadedTicketsController < ApiController
  rescue_from ActiveRecord::RecordNotFound, :with => :record_not_found
  skip_before_filter :authenticate_user_from_token!, :only => [:show]
## => View Uploaded Tickets (View, Edit, email, print)



  ##
      # = Show Uploaded Tickets
      #
      # Show Uploaded Tickets ( by event id, offer id, offer ticket id and Uploaded id)
      #
      # Need a Uploaded Ticket ID. 
      # Need a user email and authentication token for getting the offer ticket. 
      # Need an Event, Offer, Offer Ticket and Uploaded Ticket ID for getting Uploaded Ticket. 
      # Send user email and authentication token as url params. 
      #
      # GET /api/events/:id/offers/:id/offer_tickets/:id
      #
      # url_params
          #   user_email -- someone@example.com
          #   user_token -- xg282iD2oNTq8hgiyhhF
      #
      # = Examples
      #
      #   resp = conn.get("/api/events/:id/offers/:id/offer_tickets/:id/uploaded_tickets/:id")
      #
      #   resp.status
      #   => 200
      #
      #   resp.body
      #   => {"uploaded_ticket":{"id":1,"offer_ticket_id":101,"created_at":"2014-11-12T08:14:15.000Z","updated_at":"2014-11-12T08:14:15.000Z","ticket_file_name":"sample_1.pdf","ticket_content_type":"application/pdf","ticket_file_size":3406,"ticket_updated_at":"2014-11-12T08:14:15.000Z"}}
      #
      #   resp = conn.get("/api/events/:id/offers/:id/offer_tickets/:id/uploaded_tickets/:id")
      #
      #   resp.status
      #   => 404
      #
      #   resp.body
      #   => {"message":"Record Not Found"}
      #

  def show
    @uploaded_ticket = Event.find(params[:event_id]).offers.find(params[:offer_id]).offer_tickets.find(params[:offer_ticket_id]).uploaded_ticket
    render status: 200
  end

   ##
      # = Update Uploaded Tickets
      #
      # Update Uploaded Tickets ( by event id, offer id, offer ticket id and Uploaded id)
      #
      # Need a Uploaded Ticket ID. 
      # Need a user email and authentication token for getting the offer ticket. 
      # Need an Event, Offer, Offer Ticket and Uploaded Ticket ID for Updating Uploaded Ticket. 
      # Send user email and authentication token as url params. 
      #
      # GET /api/events/:id/offers/:id/offer_tickets/:id
      #
      # url_params
          #   user_email -- someone@example.com
          #   user_token -- xg282iD2oNTq8hgiyhhF
      #
      # = Examples
      #
      #   resp = conn.put("/api/events/:id/offers/:id/offer_tickets/:id/uploaded_tickets/:id")
      #
      #   resp.status
      #   => 200
      #
      #   resp.body
      #   => {"uploaded_ticket":{"id":1,"offer_ticket_id":101,"created_at":"2014-11-12T08:14:15.000Z","updated_at":"2014-11-12T08:14:15.000Z","ticket_file_name":"sample_1.pdf","ticket_content_type":"application/pdf","ticket_file_size":3406,"ticket_updated_at":"2014-11-12T08:14:15.000Z"}}
      #
      #   resp = conn.put("/api/events/:id/offers/:id/offer_tickets/:id/uploaded_tickets/:id")
      #
      #   resp.status
      #   => 404
      #
      #   resp.body
      #   => {"message":"Record Not Found"}

  def update

    @uploaded_ticket = Event.find(params[:event_id]).offers.find(params[:offer_id]).offer_tickets.find(params[:offer_ticket_id]).uploaded_ticket
    if @uploaded_ticket.update_attribute(:ticket , params[:ticket])
      render status: 204, json: {}
    else
      render status: 422, json: {message: @uploaded_ticket.errors}
    end
  end

  ##
      # = Print Uploaded Tickets
      #
      # Print Uploaded Tickets
      #
      # Need a Uploaded Ticket ID. 
      # Need a user email and authentication token for getting the offer ticket. 
      # Need an Event, Offer, Offer Ticket and Uploaded Ticket ID for getting Uploaded Ticket. 
      # Send user email and authentication token as url params. 
      #
      # GET /api/events/:id/offers/:id/offer_tickets/:id
      #
      # url_params
          #   user_email -- someone@example.com
          #   user_token -- xg282iD2oNTq8hgiyhhF
      #
      # = Examples
      #
      #   resp = conn.get("/api/events/:id/offers/:id/offer_tickets/:id/uploaded_tickets/:id/print")
      #
      #   resp.status
      #   => 200
      #
      #   resp.body
      #   => {"uploaded_ticket":{"id":1,"offer_ticket_id":101,"created_at":"2014-11-12T08:14:15.000Z","updated_at":"2014-11-12T08:14:15.000Z","ticket_file_name":"sample_1.pdf","ticket_content_type":"application/pdf","ticket_file_size":3406,"ticket_updated_at":"2014-11-12T08:14:15.000Z"}}
      #
      #   resp = conn.get("/api/events/:id/offers/:id/offer_tickets/:id/uploaded_tickets/:id/print")
      #
      #   resp.status
      #   => 404
      #
      #   resp.body
      #   => {"message":"Record Not Found"}

  def print
    @uploaded_ticket = Event.find(params[:event_id]).offers.find(params[:offer_id]).offer_tickets.find(params[:offer_ticket_id]).uploaded_ticket
    render status: 200
  end

  ##
      # = Email Uploaded Tickets to User
      #
      # Email Uploaded Tickets to Current User
      #
      # Need a Uploaded Ticket ID. 
      # Need a user email and authentication token for getting the offer ticket. 
      # Need an Event, Offer, Offer Ticket and Uploaded Ticket ID for getting Uploaded Ticket. 
      # Send user email and authentication token as url params. 
      #
      # GET /api/events/:id/offers/:id/offer_tickets/:id
      #
      # url_params
          #   user_email -- someone@example.com
          #   user_token -- xg282iD2oNTq8hgiyhhF
      #
      # = Examples
      #
      #   resp = conn.get("/api/events/:id/offers/:id/offer_tickets/:id/uploaded_tickets/:id/email")
      #
      #   resp.status
      #   => 200
      #
      #   resp.body
      #   => {"message":"email sent"}
      #
      #   resp = conn.get("/api/events/:id/offers/:id/offer_tickets/:id/uploaded_tickets/:id/email")
      #
      #   resp.status
      #   => 404
      #
      #   resp.body
      #   => {"message":"User Not Found"}

  def email
    @uploaded_ticket = Event.find(params[:event_id]).offers.find(params[:offer_id]).offer_tickets.find(params[:offer_ticket_id]).uploaded_ticket
    if @user.nil?
      render status: 404 , json: {message: "User Not Found"}
    else
      if UserMailer.send_eticket(@user, @uploaded_ticket).deliver
        render status: 200, json: {message: "email sent"}
      else
        render status: 422, json: {}
      end
    end
  end

end
