class Api::V1::OfferTicketsController < ApiController
	rescue_from ActiveRecord::RecordNotFound, :with => :record_not_found
  skip_before_filter :authenticate_user_from_token!, :only => [:show, :index]
  ## => Commission/Shipping Configs are implemented in create, update and destroy of Offer Tickets

	##
			# = All Offer_Tickets
			#
			# Return a List Of All Offers
			#
			# Need a user email and authentication token for get all offer tickets. 
			# Need a Event and Offer ID for Getting All Offer Ticket. 
			# Send user email and authentication token as url params. 
			#
			# Get /api/events/:id/offers/:id/offer_tickets
			#
			# url_params
        	#   user_email -- someone@example.com
        	#   user_token -- xg282iD2oNTq8hgiyhhF
			#
			# = Examples
			#
			#   resp = conn.get("/api/events/:id/offers/:id/offer_tickets")
			#
			#   resp.status
			#   => 200
			#
			#   resp.body
			#   => {"offer_tickets":[{"offer_ticket":{"id":37,"offer_id":1,"venue_row_id":1,"sale_id":1,"status":null,"seat_number":2,"barcode":"ef34erferf43t43t","attendee_name":"haris","created_at":"2014-10-27T09:04:41.000Z","updated_at":"2014-10-27T09:04:41.000Z"},"uploaded_ticket":{"id":5,"offer_ticket_id":37,"created_at":"2014-10-27T09:04:41.000Z","updated_at":"2014-10-27T09:04:41.000Z","ticket_file_name":"Salman Khalid.pdf","ticket_content_type":"application/pdf","ticket_file_size":597524,"ticket_updated_at":"2014-10-27T09:04:41.000Z"}}]}

	def index
		@offer_tickets = Event.find(params[:event_id]).offers.find(params[:offer_id]).offer_tickets
		render status: 200
	end

	##
			# = Create Offer_Tickets
			#
			# POST /api/events/:id/offers/:id/offer_tickets
			#
			# Need a Offer Ticket ID. 
			# Need a user email and authentication token for creating the new offer ticket. 
			# Need a Event and Offer ID for Creating Offer Ticket. 
			# Send user email and authentication token as url params. 
			#
			# url_params
        	#   user_email -- someone@example.com
        	#   user_token -- xg282iD2oNTq8hgiyhhF
			#
			# params
			#   venue_row_id  -- id -- 1
			#   sale_id      -- id -- 1
			#   status   -- text -- abc
			#   seat_number   -- number -- 1
			#   barcode   -- code -- kjsdhf9384yfjshfs
			#   attendee_name    -- Name    -- haris
      #   ticket      -- file -- PDF ticket file
			#
			# = Examples
			#
			#   resp = conn.post("/api/events/:id/offers/:id/offer_tickets")
			#
			#   resp.status
			#   => 200
			#
			#   resp.body
			#   => {"offer_ticket":{"offer_id":1,"venue_row_id":1,"sale_id":1,"status":null,"seat_number":2,"barcode":"ef34erferf43t43t","attendee_name":"haris","uploaded_ticket":{"filename":"Salman Khalid.pdf","content_type":"application/pdf","file_size":"584 KB","path":"/home/haris/Developer/RoR/slicket-api/public/system/uploaded_tickets/tickets/000/000/005/original/Salman Khalid.pdf"}}}

	def create
    @offer_ticket = Event.find(params[:event_id]).offers.find(params[:offer_id]).offer_tickets.build(get_params_create)
    if @offer_ticket.save
      if(@offer_ticket.offer.ticket_type.name.to_s == "pdf")
        if params[:ticket]
          avatar = params[:ticket]
          attachment = {
            :ticket_file_name => avatar.original_filename,
            :ticket_content_type => avatar.content_type,
            :ticket_file_size => avatar.size,
            :ticket_updated_at => DateTime.now
          }

          @uploaded_ticket = UploadedTicket.create(attachment)
          @uploaded_ticket.offer_ticket_id = @offer_ticket.id
          if @uploaded_ticket.save
            render status: 201
          else
            render status: 422, json: {errors: @uploaded_ticket.errors}
          end
        else
          render status: 201
        end
      else
        @offer = @offer_ticket.offer
        if (@offer.offer_tickets.count-1) > 0
          if @user.commission_shipping_configs.where(:action_type => "seller", :name => "shipping").nil? then amount = 20 else amount = @user.commission_shipping_configs.where(:action_type => "seller", :name => "shipping").first.amount end
          shipping_prev = amount/(@offer.offer_tickets.count-1)
          shipping = amount/@offer.offer_tickets.count
          diff = shipping_prev - shipping
          @offer.price = @offer.price-diff
          if @offer.proxy
            @offer.proxy = @offer.proxy-diff
          end
        end
        @offer.save
        render status: 201
      end
    else
      render status: 422, json:{error: @offer_ticket.errors}
    end
  end


		##
			# = Show Offer_Tickets
			#
			# Show Offer_Tickets ( by event id and by offer id)
			#
			# Need a Offer Ticket ID. 
			# Need a user email and authentication token for getting the offer ticket. 
			# Need a Event and Offer ID Updating Offer Ticket. 
			# Send user email and authentication token as url params. 
			#
			# GET /api/events/:id/offers/:id/offer_tickets/:id
			#
			# url_params
        	#   user_email -- someone@example.com
        	#   user_token -- xg282iD2oNTq8hgiyhhF
			#
			# = Examples
			#
			#   resp = conn.get("/api/events/:id/offers/:id/offer_tickets/:id")
			#
			#   resp.status
			#   => 200
			#
			#   resp.body
			#   => {"offer_ticket":{"offer_id":1,"venue_row_id":1,"sale_id":1,"status":null,"seat_number":2,"barcode":"ef34erferf43t43t","attendee_name":"haris","uploaded_ticket":{"filename":"Salman Khalid.pdf","content_type":"application/pdf","file_size":"584 KB","path":"/home/haris/Developer/RoR/slicket-api/public/system/uploaded_tickets/tickets/000/000/005/original/Salman Khalid.pdf"}}}
			#
			#   resp = conn.put("/api/events/:id/offers/:id/offer_tickets/:id")
			#
			#   resp.status
			#   => 404
			#
			#   resp.body
			#   => {"message":"Offer id is invalid"}
			#

	def show
		@offer_ticket = Event.find(params[:event_id]).offers.find(params[:offer_id]).offer_tickets.find(params[:id])
		render status: 200
	end

	##
			# = Update Offer_Tickets
			#
			# Need a Offer Ticket ID. 
			# Need a user email and authentication token for getting the offer ticket. 
			# Need a Event and Offer ID for getting Offer Ticket. 
			# Send user email and authentication token as url params. 
			#
			# PUT /api/events/:id/offers/:id/offer_tickets/:id
			#
			# url_params
        	#   user_email -- someone@example.com
        	#   user_token -- xg282iD2oNTq8hgiyhhF
			#
			# params
			#   venue_row_id  -- id -- 1
			#   sale_id      -- id -- 1
			#   status   -- text -- abc
			#   seat_number   -- number -- 1
			#   barcode   -- code -- kjsdhf9384yfjshfs
			#   attendee_name    -- Name    -- haris
      #   ticket      -- file -- PDF ticket file
			#
			# = Examples
			#
			#   resp = conn.put("/api/events/:id/offers/:id/offer_tickets/:id")
			#
			#   resp.status
			#   => 204
			#
			#   resp.body
			#   	=> {}
			#
			#   resp = conn.put("/api/events/:id/offers/:id/offer_tickets/:id")
			#
			#   resp.status
			#   => 404
			#
			#   resp.body
			#   => {"message": "No Record Found"}

	def update
		@offer_ticket = Event.find(params[:event_id]).offers.find(params[:offer_id]).offer_tickets.find(params[:id])
		if @offer_ticket.update(get_params_update)
      if(@offer_ticket.offer.ticket_type.name.to_s == "pdf")
        if params[:ticket]
          avatar = params[:ticket]
          attachment = {
            :ticket_file_name => avatar.original_filename,
            :ticket_content_type => avatar.content_type,
            :ticket_file_size => avatar.size,
            :ticket_updated_at => DateTime.now
          }
          if @offer_ticket.uploaded_ticket
            if @offer_ticket.uploaded_ticket.update(attachment)
              render status: 204, json: {}
            else
              render status: 422, json: {errors: @uploaded_ticket.errors}
            end
          else
            @uploaded_ticket = UploadedTicket.create(attachment)
            @uploaded_ticket.offer_ticket_id = @offer_ticket.id

            if @uploaded_ticket.save
              render status: 204, json: {}
            else
              render status: 422, json: {errors: @uploaded_ticket.errors}
            end
          end
        else
          render status: 204, json: {}
        end
      else
        render status: 204, json: {}
      end
		else
			render status: 422, json:{error: @offer_ticket.errors}
		end
	end

	##
			# = Delete Offer_Tickets
			#
			# Need a Offer Ticket ID. 
			# Need a user email and authentication token for deleting the offer ticket. 
			# Need a Event and Offer ID for Deleting Offer Ticket. 
			# Send user email and authentication token as url params. 
			#
			# DELETE /api/events/:id/offers/:id/offer_tickets/:id
			#
			# url_params
        	#   user_email -- someone@example.com
        	#   user_token -- xg282iD2oNTq8hgiyhhF
			#
			# = Examples
			#
			#   resp = conn.DELETE("/api/events/:id/offers/:id/offer_tickets/:id")
			#
			#   resp.status
			#   => 204
			#
			#   resp.body
			#   => {}
			#
			#   resp = conn.DELETE("/api/events/:id/offers/:id/offer_tickets/:id")
			#
			#   resp.status
			#   => 404
			#
			#   resp.body
			#   => {"message": "No Record Found"}

	def destroy
		@offer_ticket = Event.find(params[:event_id]).offers.find(params[:offer_id]).offer_tickets.find(params[:id])
		if @offer_ticket.destroy()
      @offer = @offer_ticket.offer
      if @user.commission_shipping_configs.where(:action_type => "seller", :name => "shipping").first.nil? then amount = 20 else amount = @user.commission_shipping_configs.where(:action_type => "seller", :name => "shipping").first.amount end
      shipping_prev = amount/(@offer.offer_tickets.count+1)
      shipping = amount/@offer.offer_tickets.count
      diff = shipping - shipping_prev
      @offer.price = @offer.price + diff
      if @offer.proxy
        @offer.proxy = @offer.proxy + diff
      end
      @offer.save
			render status: 204, json: {}
		else
			render status: 422, json:{error: @offer_ticket.errors}
		end
	end

  ##
      # = Show Status of an Offer_Ticket
      #
      # Show Status of an Offer Ticket ( by barcode)
      #
      # Need a Offer Ticket barcode. 
      # Need a user email and authentication token for getting the offer ticket. 
      #
      # GET /api/offer_tickets/check_ticket_status
      #
      # url_params
          #   user_email -- someone@example.com
          #   user_token -- xg282iD2oNTq8hgiyhhF
          #   barcode   -- barcode of offer ticket
      #
      # = Examples
      #
      #   resp = conn.get("/api/offer_tickets/check_ticket_status")
      #
      #   resp.status
      #   => 200
      #
      #   resp.body
      #   => {"ticket_status":true}
      #
      #   resp = conn.put("/api/offer_tickets/check_ticket_status")
      #
      #   resp.status
      #   => 404
      #
      #   resp.body
      #   => {"message":"Not Found"}
      #


	def check_ticket_status
		offer_ticket = OfferTicket.find_by_barcode (params[:barcode])
		if offer_ticket
			render status: 200, json: {ticket_status: offer_ticket.status} 
		else
			render status: 404, json: {error_message: "Not Found"}
		end
	end


	private
	def get_params_create
		params.permit(:offer_id, :venue_row_id, :sale_id, 
									:status, :seat_number, :barcode, 
									:attendee_name)
	end

	def get_params_update
		params.permit(:offer_id, :venue_row_id, :sale_id, 
									:status, :seat_number, :barcode, 
									:attendee_name)
	end
end
