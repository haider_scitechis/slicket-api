class Api::V1::VenueRowsController < ApiController
  	rescue_from ActiveRecord::RecordNotFound, :with => :record_not_found
    skip_before_filter :authenticate_user_from_token!, :only => [:show, :index]
  	##
		# = List Of ALL Venue Rows
		#
		# Return a List Of All Venus Rows
		#
		# Need a Venue ID For Getting Venue Rows. 
		# Need a user email and authentication token for getting the list of venue rows. 
        # Send user email and authentication token as url params. 
		#
		# Get /api/venues/id/event_sections/id/venue_rows
		#
		# url_params
		#   user_email -- someone@example.com
		#   user_token -- xg282iD2oNTq8hgiyhhF
		#
		# = Examples
		#
		#   resp = conn.post("/api/venues/id/event_sections/id/venue_rows")
		#
		#   resp.status
		#   => 200
		#
		#   resp.body
		#   => {"venue_rows":[{"venue_row":{"id":2,"event_section_id":1,"venue_group_id":1,"name":"front","seats":45,"created_at":"2014-10-20T12:30:22.000Z","updated_at":"2014-10-20T12:30:22.000Z"}}]}
		#
		#   resp = conn.get("/api/venues/id/event_sections/id/venue_rows")
		#
		#   resp.status
		#   => 201
		#
		#   resp.body
		#   => {}

	def index
		@venue_rows=Event.find(params[:event_id]).event_sections.find(params[:event_section_id]).venue_rows
		render status: 200
	end

	##
		# = Create Venue Row
		#
		# Creating a New Venue Row
		#
		# Need a Venue ID and Venue Section ID For Creating Venue Rows. 
		# Need a user email and authentication token for creating the Venue Rows and related it to single venue and venue section. 
        # Send user email and authentication token as url params. 
		#
		# POST /api/venues/id/event_sections/id/venue_rows
		#
		# url_params
		#   user_email -- someone@example.com
		#   user_token -- xg282iD2oNTq8hgiyhhF
		#
		# params:
		#   name -- Row Name
		#   seats -- Number of seats
		#   venue_group_id -- Rating Group id
		#
		# = Examples
		#
		#   resp = conn.post("/api/venues/id/event_sections/id/venue_rows")
		#
		#   resp.status
		#   => 201
		#
		#   resp.body
		#   => {"message": "venue_section has been created successfully"}
		#


  	def create
		@venue_row=Event.find(params[:event_id]).event_sections.find(params[:event_section_id]).venue_rows.build(get_params_create)
		if @venue_row.save
		  render status: 201, json: {message: "venue_row has been created successfully"}
		else
		  render status: 422, json:{errors: @venue_row.errors}
		end
  	end

  	##
		# = Show Venue Row
		#
		# Return a Single Venue Row
		#
		# Need a Venue Row ID
		# Need a Venue ID and Venue Section ID For Getting Venue Rows. 
		# Need a user email and authentication token for getting the all list of venue rows related to single venue and venue section. 
        # Send user email and authentication token as url params. 
		#
		# GET /api/venues/id/event_sections/id/venue_rows/id
		#
		# url_params
		#   user_email -- someone@example.com
		#   user_token -- xg282iD2oNTq8hgiyhhF
		#
		# = Examples
		#
		#   resp = conn.get("/api/venues/id/event_sections/id/venue_rows/id")
		#
		#   resp.status
		#   => 200
		#
		#   resp.body
		#   => {"venue_row":{"id":2,"event_section_id":1,"venue_group_id":1,"name":"front","seats":45,"created_at":"2014-10-20T12:30:22.000Z","updated_at":"2014-10-20T12:30:22.000Z"}}
		#
		#   resp = conn.get("/api/venues/id/event_sections/id/venue_rows/id")
		#
		#   resp.status
		#   => 404
		#
		#   resp.body
		#   => { "message": "No Record Found"}
		#

  	def show
		@venue_row = Event.find(params[:event_id]).event_sections.find(params[:event_section_id]).venue_rows.find(params[:id])
		render status: 200
  	end

  	##
		# = Update Venue Row
		#
		# Updating the existing Venue Row
		#
		# Need a Venue Row ID
		# Need a Venue ID and Venue Section ID For Updaing Venue Row. 
		# Need a user email and authentication token for updating the exisiting venue row related to a single venue and venue section. 
        # Send user email and authentication token as url params. 
		#
		# Patch /api/venues/id/event_sections/id/venue_rows/id
		#
		# url_params
		#   user_email -- someone@example.com
		#   user_token -- xg282iD2oNTq8hgiyhhF
		#
		# params:
		#   name -- Row Name
		#   seats -- Number of seats
		#   venue_group_id -- Rating Group id
		#
		# = Examples
		#
		#   resp = conn.patch("/api/venues/id/event_sections/id/venue_rows/id")
		#
		#   resp.status
		#   => 204
		#
		#   resp.body
		#   => {}
		#
		#   resp = conn.patch("/api/venues/id/event_sections/id/venue_rows/id")
		#
		#   resp.status
		#   => 404
		#
		#   resp.body
		#   => {"message": "No Record Found"}
		#

  	def update
		@venue_row=Event.find(params[:event_id]).event_sections.find(params[:event_section_id]).venue_rows.find(params[:id])
		if @venue_row.update(get_params_update)
			render status: 204, json: {}
		else
		  	render status: 422, json:{errors: @venue_row.errors}
		end
  	end

  	##
		# = Delete Venue Row
		#
		# Deleting the existing Venue Row
		#
		# Need a Venue Row ID. 
		# Need a Venue ID and Venue Section ID For Deleting Venue Row. 
		# Need a user email and authentication token for deleting the venue row related the single venue and venue section. 
        # Send user email and authentication token as url params. 
		#
		# Delete /api/venues/id/event_sections/id/venue_rows/id
		#
		# url_params
		#   user_email -- someone@example.com
		#   user_token -- xg282iD2oNTq8hgiyhhF
		#
		# = Examples
		#
		#   resp = conn.delete("/api/venues/id/event_sections/id/venue_rows/id")
		#
		#   resp.status
		#   => 204
		#
		#   resp.body
		#   => {}
		#
		#   resp = conn.delete("/api/venues/id/event_sections/id/venue_rows/id")
		#
		#   resp.status
		#   => 404
		#
		#   resp.body
		#   => {"message": "No Record Found"}
		#

  	def destroy
		@venue_row=Event.find(params[:event_id]).event_sections.find(params[:event_section_id]).venue_rows.find(params[:id])
		if @venue_row.destroy()
			render status: 204, json: {}
		else
		  	render status: 422, json:{errors: @venue_row.errors}
		end
  	end

    ## => List Rows By Star Rating

  	##
		# = List Of Venue Row by Rating
		#
		# Return a List Of Venus Rows Related to One Venue Section Based on Given Rating
		#
		# Need a Venue ID and Venue Section ID For Getting Venue Row. 
		# Need a user email and authentication token for getting the venue row related the single venue and venue section. 
        # Send user email and authentication token as url params. 
		#
		# Get /api/venues/id/event_sections/id/venue_rows/list_by_rating
		#
		# url_params
		#   user_email -- someone@example.com
		#   user_token -- xg282iD2oNTq8hgiyhhF
		#
		# params:
		#   rating -- Rating
		#
		# = Examples
		#
		#   resp = conn.post("/api/venues/id/event_sections/id/venue_rows/list_by_rating")
		#
		#   resp.status
		#   => 200
		#
		#   resp.body
		#   => {"venue_rows":[{"venue_row":{"id":3,"event_section_id":4,"venue_group_id":1,"name":"if","seats":50,"created_at":"2014-10-22T13:29:47.000Z","updated_at":"2014-10-22T13:29:47.000Z"}}]}
		#
		#   resp = conn.get("/api/venues/id/event_sections/id/venue_rows/list_by_rating")
		#
		#   resp.status
		#   => 200
		#
		#   resp.body
		#   => {}

	def list_by_rating

		@venue_rows = Event.find(params[:event_id]).event_sections.find(params[:event_section_id]).venue_rows.joins(:venue_group).where(venue_groups: {:rating => params[:rating]})
		render status: 200
	end

  ## => List tickets by venue rows

  ##
        # = List of Tickets of a Venue Row
        #
        # Return a List of All Tickets That Belong to the Given Venue Row Present in the System
        #
        # Need a Event ID For Getting Event.
        # Need a Event Section ID For Getting Event Section.
        # Need a Venue Row ID For Getting Venue Row. 
        # Need a user email and authentication token for getting the list of venue rows. 
        # Send user email and authentication token as url params. 
        #
        # Get /api/events/id/event_sections/id/venue_rows/id/list_tickets
        #
        # url_params
        #   user_email -- someone@example.com
        #   user_token -- xg282iD2oNTq8hgiyhhF
        #
        #
        # params:
        #   name -- Group Name
        #   rating -- Number 
        #
        # = Examples
        #
        #   resp = conn.get("/api/events/id/event_sections/id/venue_rows/id/list_tickets")
        #
        #   resp.status
        #   => 200
        #
        #   resp.body
        #   => {"offer_tickets":[{"offer_ticket":{"id":19,"offer_id":2,"venue_row_id":8,"sale_id":null,"status":null,"seat_number":81,"barcode":null,"attendee_name":null,"created_at":"2014-11-05T08:20:27.000Z","updated_at":"2014-11-05T08:20:27.000Z"},"uploaded_ticket":null}]}
        #
        #

  def list_tickets
    @tickets=Event.find(params[:event_id]).event_sections.find(params[:event_section_id]).venue_rows.find(params[:id]).offer_tickets
    render status: 200
  end

	private

	def get_params_create
		params.permit(:event_section_id, :venue_group_id, :name, :seats, :row_number)
	end

	def get_params_update
		params.permit(:event_section_id, :venue_group_id, :name, :seats, :row_number)
	end
	
	
end
