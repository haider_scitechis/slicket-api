class Api::V1::SmsAlertConfigOptionsController < ApiController
  rescue_from ActiveRecord::RecordNotFound, :with => :record_not_found

  before_filter :set_user, :only => [:index, :create, :destroy]
  before_filter :set_sms_alert_config, :only => [:create]
  before_filter :set_sms_alert_config_option, :only => [:destroy]


  ##
        # => List all SMS alert config options
        #
        # Return a List of All sms alert config questions
        #
        # => Required Parameters
        # user_email and user_token
        #
        # => Example 1
        #
        # Get /api/sms_alert_config_options
        #
        #   resp = conn.get("/api/sms_alert_config_options")
        #
        #
        # => parameters
        #   user_email = saad@slicket.com
        #   user_token = 8q2chs3eu5HPuwstMxsp
        #
        #   resp.status
        #   => 200
        #
        #   resp.body
        #   => {"result":[{"id":2,"option":"Do you want to receive sms for bids","created_at":"2014-11-26T09:46:40.000Z","updated_at":"2014-11-26T09:46:40.000Z"},{"id":4,"option":null,"created_at":"2014-11-26T10:26:14.000Z","updated_at":"2014-11-26T10:26:14.000Z"}]}
        #
        # => Example 2
        #
        # Get /api/sms_alert_config_options
        #
        #   resp = conn.get("/api/sms_alert_config_options")
        #
        # => parameters
        #   user_email = saad@slicket.com
        #   user_token = 8q2chs3eu5HPuwstMxs
        #
        #   resp.status
        #   => 401
        #
        #   resp.body
        #   => {"errors":["Invalid login credentials"],"debug_error_from":"DEBUG_INFO: authenticate_user_from_token!"}

  def index
    render status: 200, json: { result: @user.sms_alert_configs }
  end

  def create

    @user.sms_alert_config_options.create :user_id => @user.id, :sms_alert_config_id =>  @sms_alert_config.id
    render status: 201, json: {}
  end

  def destroy

    if @alert_config_option_id.destroy()
      render status: 204, json: {}
    else
      render status: 422, json:{errors: @alert_config_option_id.errors}
    end

  end

  private
    def set_sms_alert_config
      @sms_alert_config = SmsAlertConfig.find params[:sms_alert_config_id]
    end

    def set_sms_alert_config_option
      @alert_config_option_id = @user.sms_alert_config_options.find params[:id]
    end

    def set_user
      @user  = User.find_by_email(params[:user_email])
    end
end