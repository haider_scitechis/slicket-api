class Api::V1::AccountsController < ApiController

  skip_before_filter :authenticate_user_from_token!
  skip_before_filter :authenticate_user!

  prepend_before_filter      :check_sign_up_flag, :only => [:create]


  def new
  end

  def create
    user = User.from_omniauth(env['omniauth.auth'], current_user)
    if user.persisted?
      sign_in user
      user.reset_authentication_token!
      user.save!

      render json: {
      first_name: user.first_name,
      last_name: user.last_name,
      email: user.email,
      auth_token: user.authentication_token,
      }, status: :ok
    else
      render json: {
      result: "unable to register, please try again"
      }, status: :ok

    end
  end


  def instagram
    data        = {}
    data["code"] = params["code"]
    data["client_id"]     = "341186a8f963477aa462a5e94b45b3db"
    data["client_secret"] = "0b89104a3e9c4a32a1797bdf8af5b60c"
    data["grant_type"]    = "authorization_code"
    data["redirect_uri"]  = "http://localhost:3000/auth/instagram_callback"

    uri       = URI.parse("https://api.instagram.com/oauth/access_token")
    response  = Net::HTTP.post_form(uri, data)

    if response.code == "200"
      hash  = JSON.parse response.body
      email = "#{hash["user"]["username"]}@example.com"
      if not User.exists?(:email => email)
        user = User.new
        user.username   = hash["user"]["username"]
        user.email      = "#{hash["user"]["username"]}@example.com"
        user.first_name = hash["user"]["full_name"]
        user.last_name  = hash["user"]["full_name"]
        user.title      = "-"
        user.save()
      end

      user = User.where(:email => email).first
      sign_in user
      user.reset_authentication_token!
      user.save!

      render json: {
        first_name: user.first_name,
        last_name: user.last_name,
        email: user.email,
        auth_token: user.authentication_token,
        }, status: :ok


    else
      render json: {
        result: "Please try again !",
        }, status: :ok

    end
  end


  def dropbox_authorize

    consumer = Dropbox::API::OAuth.consumer(:authorize)
    request_token = consumer.get_request_token
    session[:request_token] = request_token.token
    session[:request_token_secret] = request_token.secret
    redirect_to request_token.authorize_url(:oauth_callback => 'http://localhost:3000/dropbox/authorized_callback')

  end

  def authorized_callback

    hash = { oauth_token: session[:token], oauth_token_secret: session[:token_secret]}
    request_token  = OAuth::RequestToken.from_hash(consumer, hash)
    oauth_verifier = params[:oauth_verifier]
    result = request_token.get_access_token(:oauth_verifier => oauth_verifier)

    client = Dropbox::API::Client.new :token => result.token, :secret => result.secret
 

    consumer = Dropbox::API::OAuth.consumer(:authorize)
    request_token = OAuth::RequestToken.new(consumer, session[:request_token], session[:request_token_secret])
    access_token = request_token.get_access_token(:oauth_verifier => params[:oauth_token])
    # MusicRage doesn't have 'Users', just Orders accessed with secure URLs
    # that's why we save key-secret pair on Order, not User basis
    @order.dropbox_access_key = access_token.token
    @order.dropbox_access_secret = access_token.secret
    @order.save
    redirect_to order_download_path(@order.access_token)

  end

  private

    def check_sign_up_flag

      if User::ALLOW_SIGN_UP
        render json:{message: "Sign up is not allowed"}, status: 401
      end
      return true
    end

end