class Api::V1::UsersController < ApiController
  before_filter :check_if_admin, :except => [:show]

  ##
        # = List Of Users
        #
        # Return a list of all users present in the system
        #
        # Need a user email and authentication token for get the list of users. 
        # Send user email and authentication token as url params. 
        #
        # GET /api/v1/users
        #
        # params:
        #   user_email -- User Email Address
        #   user_token -- User Authantication Token
        #
        # = Examples
        #
        #   resp = conn.get("/api/v1/users", "user_email" => "someone@example.com", "user_token" => "dcbb7b36acd4438d07abafb8e28605a4")
        #
        #   resp.status
        #   => 200
        #
        #   resp.body
        #   => [{"project": "id": 1, "email": "saad@example.com", "created_at": "2014-10-16T08:14:40.000Z", "updated_at": "2014-10-16T09:05:21.000Z", "sale_commission": null, "negotiation_commission": null, "title": "Saad M.", "subscribe": null, "username": "SM", "first_name": "Saad", "middle_name": null, "last_name": "Masood", "phone": null, "phone_daytime": null, "phone_evening": null, "phone_cell": null}]
        #
        #   resp = conn.get("/api/v1/users", "user_token" => "dcbb7b36acd4438d07abafb8e28605a4")
        #
        #   resp.status
        #   => 401
        #
        #   resp.body
        #   => {"errors": "Invalid login credentials"}
        #
  	def index
    	@users =  User.all
    	render json: @users, status: :ok
  	end

    def create 
      @user = User.new create_user_params
      if @user.save
        render json: @users, status: :ok
      else
        render json: {error: @user.errors}, status: 422
      end
    end

    ##
        # = Show User
        #
        # Returns a user present in the system
        #
        # Need a user email and authentication token for get the user. 
        # Send user email and authentication token as url params. 
        #
        # GET /api/v1/users
        #
        # params:
        #   user_email -- User Email Address
        #   user_token -- User Authantication Token
        #
        # = Examples
        #
        #   resp = conn.get("/api/v1/users", "user_email" => "someone@example.com", "user_token" => "dcbb7b36acd4438d07abafb8e28605a4")
        #
        #   resp.status
        #   => 200
        #
        #   resp.body
        #   => [{"project": "id": 1, "email": "saad@example.com", "created_at": "2014-10-16T08:14:40.000Z", "updated_at": "2014-10-16T09:05:21.000Z", "sale_commission": null, "negotiation_commission": null, "title": "Saad M.", "subscribe": null, "username": "SM", "first_name": "Saad", "middle_name": null, "last_name": "Masood", "phone": null, "phone_daytime": null, "phone_evening": null, "phone_cell": null}]
        #
        #   resp = conn.get("/api/v1/users", "user_token" => "dcbb7b36acd4438d07abafb8e28605a4")
        #
        #   resp.status
        #   => 401
        #
        #   resp.body
        #   => {"errors": "Invalid login credentials"}
        #

    def show
      @user=User.find_by_email(params[:user_email])
      render status: 200
    end

    def destroy
      @user=User.find_by_email(params[:user_email])
      @user.destroy
      render status: 204
    end

end
