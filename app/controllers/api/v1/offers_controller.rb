class Api::V1::OffersController < ApiController
  rescue_from ActiveRecord::RecordNotFound, :with => :record_not_found
  skip_before_filter :authenticate_user_from_token!, :only => [:show, :index]
  before_filter :set_twilio, :only => [:create]
  ## => Offer cancellation & crud

  ## => Get all other offers
  ##
      # = All Offers
      #
      # Return a List Of All Offers
      #
      # Need a Event ID For Getting Offers 
      # Need a user email and authentication token for get the list of offers. 
      # Send user email and authentication token as url params. 
      #
      # url_params
          #   user_email -- someone@example.com
          #   user_token -- xg282iD2oNTq8hgiyhhF
          #
      # Get /api/events/:id/offers
      #
      # = Examples
      #
      #   resp = conn.get("/api/events/:id/offers")
      #
      #   resp.status
      #   => 200
      #
      #   resp.body
      #   => {"offers":[{"offer":{"id":5,"name":"ABC","datetime":"2013-02-03T00:00:00.000Z","description":"this is certainly not description","primary":true,"primary_pdf":null,"setgeek_id":"5","venue_id":1,"created_at":"2014-10-20T12:24:46.000Z","updated_at":"2014-10-20T12:31:55.000Z","event_type":null},{"id":6,"name":"ABC","datetime":"2013-02-03T00:00:00.000Z","description":"This is description","primary":true,"primary_pdf":null,"setgeek_id":"2","venue_id":1,"created_at":"2014-10-20T12:57:09.000Z","updated_at":"2014-10-20T12:57:09.000Z","event_type":null}}]}

  def index
    @offers = Event.find(params[:event_id]).offers
    render status: 200
  end

  ## => Commission/Shipping Configs are implemented in create of Offer
  ## => Create offer


  ##
      # = Create Offer
      #
      # Creating a New Offer
      #
      # Need a Offer ID. 
      # Need a Event ID For Creating Offer. 
      # Need a user email and authentication token for creating a new offer and related it to the event. 
      # Send user email and authentication token as url params. 
      #
      # POST /api/events/:id/offers/
      #
      # url_params
          #   user_email -- someone@example.com
          #   user_token -- xg282iD2oNTq8hgiyhhF
      #
      # params
      #   venue_group_id  -- id
      #   ticket_type_id      -- id
      #   price   -- 100
      #   proxy   -- 95
      #   multiple   -- 1
      #   cancelothers    Required    -- 1
      #   in_hand_date    Required    -- 2014-02-03
      #   expected_ship_date    Required    -- 2014-02-03
      #   is_negotiation    Required    -- 1
      #   user_id    Required    -- User ID
      #   seat_numbers    Required    -- Seat Numbers
      #   venue_row_id    -- Venue Row Id for Tickets
      #   tickets    Required   -- PDF file for tickets
      #
      # = Examples
      #
      #   resp = conn.post("/api/events/:id/offers/")
      #
      #   resp.status
      #   => 200
      #
      #   resp.body
      #   => {"offer":{"id":30,"venue_group_id":1,"event_id":1,"user_id":1,"ticket_type_id":1,"price":200.0,"proxy":195.0,"multiple":1,"cancelothers":1,"status":null,"in_hand_date":"2014-10-26","expected_ship_date":"2014-10-26","is_negotiation":1,"expires_at":null,"confirm_transtaction":null,"piggybacked":null,"notes":null,"venue_group":{"id":1,"name":"4star","rating":4,"venue_id":1,"created_at":"2014-10-23T11:47:43.000Z","updated_at":"2014-10-23T11:47:43.000Z"},"ticket_type":{"id":1,"name":"pdf","created_at":"2014-10-27T07:27:03.000Z","updated_at":"2014-10-27T07:27:03.000Z"},"offer_tickets":[{"offer_ticket":{"id":84,"offer_id":30,"venue_row_id":1,"sale_id":1,"status":"available","seat_number":65,"barcode":null,"attendee_name":null,"created_at":"2014-10-28T12:25:25.000Z","updated_at":"2014-10-28T12:25:25.000Z"},"uploaded_ticket":{"id":40,"offer_ticket_id":84,"created_at":"2014-10-28T12:25:26.000Z","updated_at":"2014-10-28T12:25:26.000Z","ticket_file_name":"pdf-sample_1.pdf","ticket_content_type":"application/pdf","ticket_file_size":9503,"ticket_updated_at":"2014-10-28T12:25:26.000Z"}}]}}


def create
  @offer      = Event.find(params[:event_id]).offers.build(get_params_create)
  @offer.disclosures.build(params.require(:disclosures).permit(:name, :desc))
  if @user.commission_shipping_configs.where(:action_type => "seller", :name => "commission").first.nil? then amount = 20 else amount = @user.commission_shipping_configs.where(:action_type => "seller", :name => "commission").first.amount end
  commission      = ( amount / @offer.price ) * 100
  @offer.price    = @offer.price + commission
  @offer.proxy    = @offer.proxy + commission


  if @offer.ticket_type.name.to_s == "pdf"


    if params[:tickets]
      avatar      = params[:tickets]
    

      attachment  = {
        :ticket_file_name => avatar.original_filename,
        :ticket_content_type => avatar.content_type,
        :ticket_file_size => avatar.size,
        :ticket_updated_at => DateTime.now
      }
    

      @offer.tickets = avatar
    

      if @offer.save
        path    = @offer.tickets.path
        file    =path.split(".pdf")[0]
        length  = Docsplit.extract_length(@offer.tickets.path)
        Docsplit.extract_pages(path, 
                              :pages => 1..length,
                              :output => path.split(avatar.original_filename)[0])
    

        if params[:seat_numbers].split(",").length == length && params[:venue_row_id]
    

          (0..length-1).each do |i|
            @offer_ticket             = @offer.offer_tickets.build(get_ticket_params_create)
            @offer_ticket.status      = "available"
            @offer_ticket.seat_number = params[:seat_numbers].split(",")[i]
            @offer_ticket.save
            @uploaded_ticket          = UploadedTicket.new()
            @uploaded_ticket.offer_ticket_id = @offer_ticket.id
            ticket                    = File.open("#{file}_#{i+1}.pdf")
            @uploaded_ticket.ticket   = ticket
            @uploaded_ticket.save
          end
          post_to_twitter @offer
          send_notifications @offer
          post_to_facebook @offer
          render status: 201
        

        else
          render status:422, json: {errors: "Seat Numbers not equal to uploaded tickets OR Venue Row ID missing"}    
        end
      

      else
        render status: 422, json: {errors: @offer.errors}
      end
    

    else
      render status:422, json: {errors: "Tickets File Not Found"}
    end

  else
    

    if params[:seat_numbers] && params[:venue_row_id]
      length=params[:seat_numbers].split(",").length
      

      if @user.commission_shipping_configs.where(:action_type => "seller", :name => "shipping").first.nil? then amount = 20 else amount = @user.commission_shipping_configs.where(:action_type => "seller", :name => "shipping").first.amount end
      shipping      = amount / length
      @offer.price  = @offer.price + shipping
      @offer.proxy  = @offer.proxy + shipping


      if @offer.save
        

        if params[:seat_numbers]
          
          (0..length).each do |i|
            @offer_ticket = @offer.offer_tickets.build(get_ticket_params_create)
            @offer_ticket.status = "available"
            @offer_ticket.seat_number = params[:seat_numbers].split(",")[i]
            @offer_ticket.save
            #binding.pry
          end
        
        end
        post_to_twitter @offer
        send_notifications @offer
        post_to_facebook @offer
        render status: 201
      
      else
        render status: 422, json: {errors: @offer.errors}
      end
    

    else
      render status: 422, json: {errors: "Seat numbers missing OR venue_row_id missing"}
    end
  

  end

end

  ##
      # = Show Offer
      #
      # Show Offer ( by event id)
      #
      # Need a Offer ID. 
      # Need a Event ID For Getting the Offers. 
      # Need a user email and authentication token for getting the offers related to the event. 
      # Send user email and authentication token as url params. 
      #
      # GET /api/events/:id/offers/:id
      #
      # url_params
          #   user_email -- someone@example.com
          #   user_token -- xg282iD2oNTq8hgiyhhF
      #
      # = Examples
      #
      #   resp = conn.get("/api/events/:id/offers/:id")
      #
      #   resp.status
      #   => 200
      #
      #   resp.body
      #   => {"offer":{"id":30,"venue_group_id":1,"event_id":1,"user_id":1,"ticket_type_id":1,"price":200.0,"proxy":195.0,"multiple":1,"cancelothers":1,"status":null,"in_hand_date":"2014-10-26","expected_ship_date":"2014-10-26","is_negotiation":1,"expires_at":null,"confirm_transtaction":null,"piggybacked":null,"notes":null,"venue_group":{"id":1,"name":"4star","rating":4,"venue_id":1,"created_at":"2014-10-23T11:47:43.000Z","updated_at":"2014-10-23T11:47:43.000Z"},"ticket_type":{"id":1,"name":"pdf","created_at":"2014-10-27T07:27:03.000Z","updated_at":"2014-10-27T07:27:03.000Z"},"offer_tickets":[{"offer_ticket":{"id":84,"offer_id":30,"venue_row_id":1,"sale_id":1,"status":"available","seat_number":65,"barcode":null,"attendee_name":null,"created_at":"2014-10-28T12:25:25.000Z","updated_at":"2014-10-28T12:25:25.000Z"},"uploaded_ticket":{"id":40,"offer_ticket_id":84,"created_at":"2014-10-28T12:25:26.000Z","updated_at":"2014-10-28T12:25:26.000Z","ticket_file_name":"pdf-sample_1.pdf","ticket_content_type":"application/pdf","ticket_file_size":9503,"ticket_updated_at":"2014-10-28T12:25:26.000Z"}}]}}
      #
      #   resp = conn.put("/api/events/:id/offers/:id")
      #
      #   resp.status
      #   => 404
      #
      #   resp.body
      #   => {"message":"Offer id is invalid"}
      #

  def show
    @offer = Event.find(params[:event_id]).offers.find(params[:id])
    render status: 200
  end

  ##
      # = Update Offer
      #
      # Update the existing offer. Finding it by ID
      #
      # Need a Offer ID. 
      # Need a Event ID For Updating Offer. 
      # Need a user email and authentication token for updating the offer related to the event. 
      # Send user email and authentication token as url params. 
      #
      # PUT /api/events/:id/offers/:id
      #
      # url_params
          #   user_email -- someone@example.com
          #   user_token -- xg282iD2oNTq8hgiyhhF
      #
      # params
      #   venue_group_id  -- id
      #   ticket_type_id      -- id
      #   price   -- 100
      #   proxy   -- 95
      #   multiple   -- 1
      #   cancelothers    Required    -- 1
      #   in_hand_date    Required    -- 2014-02-03
      #   expected_ship_date    Required    -- 2014-02-03
      #   is_negotiation    Required    -- 1
      #   user_id    Required    -- User ID
      #   seat_numbers    Required    -- Seat Numbers
      #   venue_row_id    -- Venue Row Id for Tickets
      #   tickets    Required   -- PDF file for tickets
      #
      # = Examples
      #
      #   resp = conn.put("/api/events/:id/offers/:id")
      #
      #   resp.status
      #   => 204
      #
      #   resp.body
      #     => {}
      #
      #   resp = conn.put("/api/events/:id/offers/:id")
      #
      #   resp.status
      #   => 404
      #
      #   resp.body
      #   => {"message": "No Record Found"}

  def update
    @offer  = Event.find(params[:event_id]).offers.find(params[:id])
    if @offer.update(get_params_update)
      render status: 204, json: {}
    else
      render status: 422, json:{error: @offer.errors}
    end
  end

  ##
      # = Delete Offer
      #
      # Destroy a User Offer. Finding it by ID
      #
      # Need a Offer ID. 
      # Need a Event ID For Deleting Offer. 
      # Need a user email and authentication token for deleting the offers related to the event. 
      # Send user email and authentication token as url params. 
      #
      # DELETE /api/events/:id/offers/:id
      #
      # url_params
          #   user_email -- someone@example.com
          #   user_token -- xg282iD2oNTq8hgiyhhF
      #
      # = Examples
      #
      #   resp = conn.DELETE("/api/events/:id/offers/:id")
      #
      #   resp.status
      #   => 204
      #
      #   resp.body
      #   => {}
      #
      #   resp = conn.DELETE("/api/events/:id/offers/:id")
      #
      #   resp.status
      #   => 404
      #
      #   resp.body
      #   => {"message": "No Record Found"}

  def destroy
    @offer  = Event.find(params[:event_id]).offers.find(params[:id])
    if @offer.destroy()
      render status: 204, json: {}
    else
      render status: 422, json:{error: @offer.errors}
    end
  end

  ## => All User offerings

  ##
      # = All Offers of Current User
      #
      # Return a List Of All Offers of Current User
      #
      # Need a Event ID For Getting Offers 
      # Need a user email and authentication token for get the list of offers. 
      # Send user email and authentication token as url params. 
      #
      # url_params
          #   user_email -- someone@example.com
          #   user_token -- xg282iD2oNTq8hgiyhhF
          #
      # Get /api/events/:id/offers/all_user_offers
      #
      # = Examples
      #
      #   resp = conn.get("/api/events/:id/offers/all_user_offers")
          #
      # Get /api/events/:id/offers
      #
      # = Examples
      #
      #   resp = conn.get("/api/events/:id/offers/all_user_offers")
      #
      #   resp.status
      #   => 200
      #
      #   resp.body
      #   => {"offers":[{"offer":{"id":5,"name":"ABC","datetime":"2013-02-03T00:00:00.000Z","description":"this is certainly not description","primary":true,"primary_pdf":null,"setgeek_id":"5","venue_id":1,"created_at":"2014-10-20T12:24:46.000Z","updated_at":"2014-10-20T12:31:55.000Z","event_type":null},{"id":6,"name":"ABC","datetime":"2013-02-03T00:00:00.000Z","description":"This is description","primary":true,"primary_pdf":null,"setgeek_id":"2","venue_id":1,"created_at":"2014-10-20T12:57:09.000Z","updated_at":"2014-10-20T12:57:09.000Z","event_type":null}}]}


  def all_user_offers
    @offers         = Event.find(params[:event_id]).offers.where(:user_id => @user.id)
    render status: 200
  end

  ## => List tickets by Quantity

##
      # = List Offers
      #
      # Return a List Of Offers With Given quantity of Tickets
      #
      # Need a Event ID For Getting Offers 
      # Need a user email and authentication token for get the list of offers. 
      # Send user email and authentication token as url params. 
      #
      # url_params
          #   user_email -- someone@example.com
          #   user_token -- xg282iD2oNTq8hgiyhhF
          #   quantity -- Quantity of tickets
      # Get /api/events/:id/offers/list_offers
      #
      # = Examples
      #
      #   resp = conn.get("/api/events/:id/offers/list_offers")
          #   quantity -- 10
          #
      # Get /api/events/:id/offers/list_offers
      #
      # = Examples
      #
      #   resp = conn.get("/api/events/:id/offers/list_offers")
      #
      #   resp.status
      #   => 200
      #
      #   resp.body
      #   => {"offers":[{"offer":{"id":5,"name":"ABC","datetime":"2013-02-03T00:00:00.000Z","description":"this is certainly not description","primary":true,"primary_pdf":null,"setgeek_id":"5","venue_id":1,"created_at":"2014-10-20T12:24:46.000Z","updated_at":"2014-10-20T12:31:55.000Z","event_type":null},{"id":6,"name":"ABC","datetime":"2013-02-03T00:00:00.000Z","description":"This is description","primary":true,"primary_pdf":null,"setgeek_id":"2","venue_id":1,"created_at":"2014-10-20T12:57:09.000Z","updated_at":"2014-10-20T12:57:09.000Z","event_type":null}}]}

  def list_offers
    @offers   =[]
    Offer.all.each do |offer|
      @offers.append(offer) if offer.offer_tickets.count == params[:quantity].to_i
    end
    render status: 200
  end

# => Broker Batch Upload

# Ticket broker batch upload require to download file from dropbox first and then read it. DropBox API will used in the front end of the application. In this part we have completed the functionality with a static file only.

  def broker_batch_upload

    url     = "Sample_Tickets_File_1.csv"

    data    = SmarterCSV.process(url)
    data.each do |item|

      if Event.exists?(:name => item[:event], :date => Date.parse(item[:eventdate]))

        @event          = Event.where(:name => item[:event], :date => Date.parse(item[:eventdate])).first

        @event_section  = EventSection.find item[:section]
#        @event_section  = EventSection.first
        user           = User.first

        @row            = @event_section.venue_rows.find_by_row_number item[:row]
        @venue_group    = @row.venue_group

        @offer = Offer.new(:venue_group => @venue_group, :event => @event, :user=> user, :ticket_type => TicketType.second, :price => item[:cost], :cancelothers => true, :in_hand_date => item[:inhanddate])
        @offer.save

        (0..item[:quantity]).each do |ticket_count|
          @offer.offer_tickets.create(:offer => @offer, :venue_row => @row, :seat_number => item[:seatfrom] + ticket_count, :barcode => item[:ticketid])
        end

      end
    end

    render status: 204, json: {}
  end

  ## => Get Bids for an offer

##
      # = Get Bids for an offer
      #
      # Get Bids that are related to Given Offer
      #
      # Need a Event nad Offer ID For Getting Offer 
      # Need a user email and authentication token for get the offer. 
      # Send user email and authentication token as url params. 
      #
      # url_params
          #   user_email -- someone@example.com
          #   user_token -- xg282iD2oNTq8hgiyhhF
      #
      # Get /api/events/:id/offers/:id/get_notifications
      #
      # = Examples
      #
      #   resp = conn.get("/api/events/:id/offers/:id/get_notifications")
      #
      #   resp.status
      #   => 200
      #
      #   resp.body
      #   => {"bids":[]}
      #
      # Get /api/events/:id/offers/:id/get_notifications
      #
      # = Examples
      #
      #   resp = conn.get("/api/events/:id/offers/:id/get_notifications")
      #
      #   resp.status
      #   => 200
      #
      #   resp.body
      #   => {"bids":[{"id":11,"event_id":107,"user_id":1,"groupable_id":4,"groupable_type":"VenueGroup","price":100,"quantity":10,"proxy":null,"expire_at":"2014-11-17T06:42:14.000Z","cancelothers":0,"status":null,"in_hand_date":null,"is_negotiation":true,"expacted_ship_date":null,"created_at":"2014-11-17T06:42:14.000Z","updated_at":"2014-11-17T06:42:14.000Z","notification_seen":false}]}


  def get_notifications

    @offer  = Event.find(params[:event_id]).offers.find(params[:offer_id])    
    @bids   = get_bids @offer
    render status: 200

  end

  ## => Sell now

##
      # = Sell now offer to a bid
      #
      # Send email to Bidder to sell offer now
      #
      # Need a Event ID For Getting Offers 
      # Need a user email and authentication token for get the list of offers. 
      # Send user email and authentication token as url params. 
      #
      # url_params
          #   user_email -- someone@example.com
          #   user_token -- xg282iD2oNTq8hgiyhhF
          #   bid_id  --  Bid id 
      #
      # Post /api/events/:id/offers/:id/sell_now
      #
      # = Examples
      #
      #   resp = conn.post("/api/events/:id/offers/:id/sell_now")
      # => params[:bid_id]=1
      #
      #   resp.status
      #   => 200
      #
      #   resp.body
      #   => {"message":"email sent"}
      #
      # Post /api/events/:id/offers/:id/sell_now
      #
      # = Examples
      #
      #   resp = conn.post("/api/events/:id/offers/:id/sell_now")
      # => params[:bid_id]=2
      #
      #   resp.status
      #   => 404
      #
      #   resp.body
      #   => {"message": "No Record Found"}


  def sell_now
    @offer = Offer.find(params[:offer_id])
    @bids= get_bids @offer
    @bid=[]
    if not @bids.blank?
      @bids.each do |bid|
        @bid << bid if bid[0].id == params[:bid_id].to_i
      end
    end
    @bid= @bid.uniq
    if not @bid.blank?
      user = @bid[0][0].user
      @url_offer = "http://#{request.host_with_port}/api/events/#{params[:event_id]}/offers/#{params[:offer_id]}"
      @url_confirm = "http://#{request.host_with_port}/api/events/#{params[:event_id]}/bids/#{params[:bid_id]}/confirm_deal?offer_id=#{@offer.id}"
      if UserMailer.sell_now(user, @url_offer, @url_confirm).deliver
        render status: 200, json: {message: "email sent"}
      else
        render status: 422, json: {}
      end
    else
      render status: 422, json: {error: "Bid not found"}
    end
  end

  ## => Sell now

##
      # = Confirm deal
      #
      # Confirm deal between a bid and offer
      #
      # Need a Event ID For Getting Offers 
      # Need a user email and authentication token for get the list of offers. 
      # Send user email and authentication token as url params. 
      #
      # url_params
          #   user_email -- someone@example.com
          #   user_token -- xg282iD2oNTq8hgiyhhF
          #   bid_id  --  Bid id 
      #
      # Get /api/events/:id/offers/:id/confirm_deal
      #
      # = Examples
      #
      #   resp = conn.get("/api/events/:id/offers/:id/confirm_deal")
      # => params[:bid_id]=1
      #
      #   resp.status
      #   => 201
      #
      #   resp.body
      #   => {"sale":{"id":4,"bid_id":1,"offer_id":9,"ticket_status_id":1,"coupon_id":null,"price":636.0,"quantity":7,"created_at":"2014-11-18T07:34:55.447Z","updated_at":"2014-11-18T07:34:55.447Z"}}

  def confirm_deal
    bid = Bid.find(params[:bid_id])
    @offer = Offer.find(params[:offer_id])
    ticket_status = TicketStatus.first
    @sale = Sale.new(:offer => @offer, :bid => bid,:ticket_status => ticket_status,:price => @offer.price,:quantity => bid.quantity)
    if @sale.save
      render status: 201
    else
      render status: 422, json: {errors: @sale.errors}
    end
  end



  private

  def get_bids(offer)
    @offer    =offer
    @bids     =[]
    @offer.offer_tickets.all.each do |offer_ticket|
      row     =offer_ticket.venue_row
      if row
        @bids   << row.venue_group.bids.where(:event_id => params[:event_id])
      end
    end
    @bids.flatten.uniq.sort_by{|h| h[:created_at]}.reverse
    if not @bids.blank?
      send_emails(@offer, @bids)
    end
    @bids
  end

  def send_notifications(offer)

    @offer  = offer
    @bids   = get_bids @offer
    if not @bids.blank?
      @bids.each do |bid|
        if not bid[0].blank?
          noti = Notification.create(:sendable => @offer.user, :notifiable => @bid)
          notiRec = noti.recipients.build(:recievable => bid[0].user)
          notiRec.content = "Offer is Created by this #{@offer.user.first_name} #{@offer.user.last_name} user in VenueGroup #{@offer.venue_group.name} where you place a bid"
          noti.save
        end
      end
    end
  end

  def get_params_create
    params.permit(:venue_group_id, :event_id,
                  :ticket_type_id, :price, :proxy, :multiple, 
                  :cancelothers, :status, :in_hand_date, 
                  :expected_ship_date, :is_negotiation, 
                  :expires_at, :confirm_transtaction, 
                  :piggybacked, :notes).merge(:user_id => @user.id)
  end

  def get_params_update
    params.permit(:venue_group_id, :event_id,
                  :ticket_type_id, :price, :proxy, :multiple, 
                  :cancelothers, :status, :in_hand_date, 
                  :expected_ship_date, :is_negotiation, 
                  :expires_at, :confirm_transtaction, 
                  :piggybacked, :notes)
  end

  def get_ticket_params_create
    params.permit(:venue_row_id, :sale_id)
  end

  def post_to_facebook(offer)
    auth = @user.authorizations.where(:provider => "facebook").last
    if auth
      token = auth.token
      fb_user = FbGraph2::User.me(token).fetch
      event_url = url_for controller: 'events', action: 'show', id: offer.event_id
      offer_url = url_for controller: 'offers', action: 'show', event_id: offer.event_id, id: offer.id 
      message = "placed a offer on www.slicketticket.com/new for #{event_url}"
      fb_user.feed!(
        :message => message,
        :picture => "http://slicketticket.com/new/images/home.jpg",
        :link => offer_url,
        :name => 'Slicket Ticket',
        :description => 'Slicket Ticket: People powered tickets'
      )


    end
  end

  def post_to_twitter(offer)

    auth = @user.authorizations.where(:provider => 'twitter').last  
    if auth
      twitter = Twitter::REST::Client.new do |config|
        config.consumer_key = SlicketTicket::Application.config.twitter_token
        config.consumer_secret = SlicketTicket::Application.config.twitter_secret
        config.access_token = auth.token
        config.access_token_secret = auth.secret
      end
      
      message = "placed an offer on http://slicketticket.com/new "
      twitter.update(message)
    end
  end

  def set_twilio
    @client = Twilio::REST::Client.new
  end

  def send_emails(offer, bids)
    if not bids.blank?
      bids.each do |bid|
        if not bid[0].blank?
          user = bid[0].user
          if user.email_alert_configs.exists?(:option => "on bid match")
            UserMailer.on_bids_match(request, offer, bid).deliver
          end

          if user.sms_alert_configs.exists?(:option => "on bid match")
            send_sms(user.phone, "+15612575425", "A bid found for your bid. Please visit www.google.com")
          end
        end

      end
    end

  end

  def send_sms(to, from, body)
#    @client.messages.create( :from => from, :to => to, :body => body)
  end

end
