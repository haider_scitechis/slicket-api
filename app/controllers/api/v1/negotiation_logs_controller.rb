class Api::V1::NegotiationLogsController < ApiController

  rescue_from ActiveRecord::RecordNotFound, :with => :record_not_found

  before_filter :set_offer, :only => [:log]
  before_filter :set_bid, :only => [:log]

  ##
  # = List of All Negotiation Logs
  #
  # Return a List of All Negotiation Logs
  #
  # Need a user email and authentication token for getting the list of venues. 
  # Send user email and authentication token as url params. 
  #
  # Get /api/offers/:offer_id/bids/:bid_id/negotiation_logs
  #
  # url_params
  #   user_email -- someone@example.com
  #   user_token -- xg282iD2oNTq8hgiyhhF
  #   offer_id -- 1
  #   bid_id -- 7
  #
  # = Examples
  #
  #   resp = conn.get("/api/offers/7/bids/1/negotiation_logs")
  #
  #   resp.status
  #   => 200
  #
  #   resp.body
  #   => {"negotiation_logs": [{"id": 1,"negotiable_id": 10,"negotiable_type": "Bid","bid_id": 10,"offer_id": 10,"quantity": 5,"price": 200,"created_at": "2014-11-20T12:48:56.000Z","updated_at": "2014-11-20T12:48:56.000Z"}]}
  #
  #   resp = conn.get("/api/offers/7/bids/1/negotiation_logs")
  #
  #   resp.status
  #   => 401
  #
  #   resp.body
  #   => "negotiation_logs": []
  #

  def index
    @negotiation_logs = NegotiationLog.where(:offer_id => params[:offer_id], :bid_id => params[:bid_id])
    render  status: 200, json:{negotiation_logs: @negotiation_logs}
  end

  ##
    # = Show Negotiation Log
    #
    # Return a Single Negotiation Log Present in the System
    #
    # Need a Negotiation Log ID. 
    # Need a user email and authentication token for getting the single venue. 
    # Send user email and authentication token as url params. 
    #
    # Get /api/offers/10/bids/10/negotiation_logs/1
    #
    # url_params
    #   user_email -- someone@example.com
    #   user_token -- xg282iD2oNTq8hgiyhhF
    #   offer_id --10
    #   bid_id -- 10
    #   id -- 1
    #
    # = Examples
    #
    #   resp = conn.get("/api/offers/10/bids/10/negotiation_logs/1")
    #
    #   resp.status
    #   => 200
    #
    #   resp.body
    #   => {"negotiation_log": {"id": 1,"negotiable_id": 10,"negotiable_type": "Bid","bid_id": 10,"offer_id": 10,"quantity": 5,"price": 200,"created_at": "2014-11-20T12:48:56.000Z","updated_at": "2014-11-20T12:48:56.000Z"}}
    #
    #   resp = conn.get("/api/offers/10/bids/10/negotiation_logs/1")
    #
    #   resp.status
    #   => 404
    #
    #   resp.body
    #   => {"message": "No Record Found"}
    #

  def show
    @negotiation_log = NegotiationLog.find(params[:id])
    render status: 200, json:{negotiation_log: @negotiation_log}
  end

  ##
    # = Create Negotiation Log
    #
    # Creating a New Negotiation Log
    #
    # Need a user email and authentication token for creating the new venue. 
    # Send user email and authentication token as url params. 
    #
    # POST /api/offers/10/bids/10/negotiation_logs/?flag=bid
    #
    # url_params
    #   user_email -- someone@example.com
    #   user_token -- xg282iD2oNTq8hgiyhhF
    #   offer_id -- 1
    #   bid_id -- 7
    #   flasg -- bid
    #
    # params:
    #   price -- 200
    #   quantity -- 5
    #
    # = Examples
    #
    #   resp = conn.post("/api/offers/10/bids/10/negotiation_logs/?flag=bid")
    #
    #   resp.status
    #   => 201
    #
    #   resp.body
    #   => {"message": "NegotiationLog has been created successfully"}
    #
    #   resp = conn.post("/api/offers/10/bids/10/negotiation_logs/?flag=bid")
    #
    #   resp.status
    #   => 401
    #
    #   resp.body
    #   => {"message": "invalid", "error": {"price": [ "can't be blank" ]}}
    #

  def create

    @negotiation_log = NegotiationLog.new(negotiation_params_new)
    bid = Bid.find(params[:bid_id])
    offer = Offer.find(params[:offer_id])
    @negotiation_log.bid_id = params[:bid_id]
    @negotiation_log.offer_id = params[:offer_id]
    if params[:flag].downcase == "bid"

      @negotiation_log.negotiable = bid

      noti = Notification.create(:sendable => bid.user, :notifiable => bid)
          notiRec = noti.recipients.build(:recievable => offer.user)
          notiRec.content = "This #{bid.user.first_name} #{bid.user.last_name} user is update his bid"
          noti.save
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          
    elsif params[:flag].downcase == "offer"

      @negotiation_log.negotiable = offer

      noti = Notification.create(:sendable => offer.user, :notifiable => offer)
          notiRec = noti.recipients.build(:recievable => bid.user)
          notiRec.content = "This #{offer.user.first_name} #{offer.user.last_name} user is update his offer "
          noti.save

    end
      
    if @negotiation_log.save
      render status: 201, json: {message: "NegotiationLog has been created successfully"}
    else
      render status: 422, json:{errors: @negotiation_log.errors}
    end

  end

  ##
    # = Delete Negotiation Log
    #
    # Delete a Negotiation Log Present in the System
    #
    # Need a Negotiation Log ID. 
    # Need a user email and authentication token for deleting the existing venue. 
    # Send user email and authentication token as url params. 
    #
    # Delete /api/offers/10/bids/10/negotiation_logs/2
    #
    # url_params
    #   user_email -- someone@example.com
    #   user_token -- xg282iD2oNTq8hgiyhhF
    #   offer_id -- 1
    #   bid_id -- 7
    #   id -- 1
    #
    # = Examples
    #
    #   resp = conn.delete("/api/offers/10/bids/10/negotiation_logs/2")
    #
    #   resp.status
    #   => 204
    #
    #   resp.body
    #   => {}
    #
    #   resp = conn.delete("/api/offers/10/bids/10/negotiation_logs/2")
    #
    #   resp.status
    #   => 404
    #
    #   resp.body
    #   => {"message": "Record Not Found"}
    #

  def destroy

    @negotiation_log = NegotiationLog.find(params[:id])
    if @negotiation_log.destroy()
      render status: 204, json: {}
    else
      render status: 422, json:{errors: @negotiation_log.errors}
    end
  end

  private

  def negotiation_params_new
    params.require(:negotiation_log).permit(:price, :quantity)
  end

  def negotiation_params_update
    params.require(:negotiation_log).permit(:price, :quantity)
  end

  def set_offer
    @offer = Offer.find params[:offer_id]
  end

  def set_bid
    @bid = offer.find params[:bid_id]
  end

end