class Api::V1::CustomDevise::RegistrationsController < Devise::RegistrationsController

  after_filter :skip_set_cookies_header
  prepend_before_filter :require_no_authentication, :only => [ :create ]
  skip_before_filter :verify_authenticity_token
  before_filter :invite_only_flag, :only => [:create]

  respond_to :json

  ## => User memberships 


  ## => User Signup

    # Creates a User and returns an Authenication Token For User
    #
    # POST /api/v1/users/
    #
    # params:
    #   params[:email]      = user.email
    #   params[:password]   = "hellohello"
    #   params[:first_name] = "Roger"
    #   params[:last_name]  = "Smith"
    #   params[:user_name]  = "lomonop"
    #   params[:title]      = "Hello World"
    #
    # = Examples
    #
    #   resp = conn.post("/api/v1/users")
    #
    #   resp.status
    #   => 200
    #
    #   resp.body
    #   => {"auth_token":"bYLjepDqLFp2Beyv8pTp","first_name":"Muhammad","last_name":"Haris"}
    #
    # = Examples 
    #   resp = conn.post("/api/v1/users")
    #
    #   resp.status
    #   => 422
    #
    #   resp.body
    #   => {"errors":["Email has already been taken"]}

  def create
    #binding.pry
    build_resource(sign_up_params)
    resource.reset_authentication_token
    if resource.save
      if resource.active_for_authentication?
        sign_up(resource_name, resource)
        render json:
        {
        auth_token: resource.authentication_token,
        first_name: resource.first_name,
        last_name: resource.last_name
        }, status: :created
      else
        render json: {errors: [resource.inactive_message]}, status: :created
      end
    else
      clean_up_passwords resource
      render json: {errors: resource.errors.full_messages}, status: :unprocessable_entity
    end
  end

  ## => Profile

  ## => Update Profile

    # Updates a User's Profile
    #
    # PATCH /api/v1/users/
    #
    # params:
    #   params[:email]                = user.email
    #   params[:current_password]     = "hellohello"
    #   params[auth_token]            = "bYLjepDqLFp2Beyv8pTp"
    #   params[:first_name]           = "Roger"
    #   params[:last_name]            = "Smith"
    #   params[:user_name]            = "lomonop"
    #   params[:title]                = "Hello World"
    #   params[:password]             = "123456789"
    #   params[:password_confirmation]= "123456789"
    #
    # = Examples
    #
    #   resp = conn.post("/api/v1/users")
    #
    #   resp.status
    #   => 202
    #
    #   resp.body
    #   => {}
    #
    # = Examples 
    #   resp = conn.post("/api/v1/users")
    #
    #   resp.status
    #   => 401
    #
    #   resp.body
    #   => {"errors":["Invalid"],"debug_error_from":"DEBUG_INFO: CustomAuthFailure"}


  def update
    self.resource = resource_class.to_adapter.get!(send(:"current_#{resource_name}").to_key)
    prev_unconfirmed_email = resource.unconfirmed_email if resource.respond_to?(:unconfirmed_email)
    #update_resource_parmas=params.except(:action, :controller)
    resource_updated = update_resource(resource, update_resource_parmas)
    yield resource if block_given?
    if resource_updated
      """
      if is_flashing_format?
      flash_key = update_needs_confirmation?(resource, prev_unconfirmed_email) ?
      :update_needs_confirmation : :updated
      set_flash_message :notice, flash_key
      end
      """

      sign_in resource_name, resource, bypass: true
      #            respond_with resource, location: after_update_path_for(resource)
      render json: {}, status: 204

    else
      clean_up_passwords resource
      render json: {errors: resource.errors.full_messages}, status: :unprocessable_entity
    end
  end
  private

  def sign_up_params
    params.permit([:password, :password_confirmation, :email, :first_name, :last_name, :username, :title, :phone_cell, :phone_evening, :phone_daytime, :subscribe])
  end
  def update_resource(resource, params)
    #binding.remote_pry
    resource.update_with_password(params)
  end
  def update_resource_parmas
    params.permit([:current_password, :password, :password_confirmation, :email, :first_name, :last_name, :username, :title, :phone_cell, :phone_evening, :phone_daytime, :subscribe])
  end

  def skip_set_cookies_header
    request.session_options = {}
  end

  def invite_only_flag
   #binding.pry
    if User::ALLOW_SIGN_UP
      render json: {errors: "sign up is only invite only"}, status: :unprocessable_entity
      return
    end

  end

end
