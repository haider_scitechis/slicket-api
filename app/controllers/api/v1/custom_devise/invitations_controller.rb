class Api::V1::CustomDevise::InvitationsController < Devise::InvitationsController

#    prepend_before_filter :mera_method, :only => [:create]
#    after_filter :skip_set_cookies_header
#    skip_before_filter :verify_authenticity_token
#    skip_before_filter :authenticate_inviter!
    prepend_before_filter :resource_from_invitation_token, :only => [:update]

#    before_filter :authenticate_user_from_token!
    respond_to :json



  ##
        # = Invite Only Sign up
        #
        # => Required Parameters
        # 
        # Need email, auth_token, invitation email
        #
        # POST /api/users/invitation
        #
        # = Examples
        #
        #  params          = {} 
        #  params[:user][:email]    = {saad@slicket.com}
        #  params[:user][:auth_token] = xhER5Qasqff5qLbLi2
        #  params[:invite][:email] = "invitate@gmail.com"
        #   resp = conn.post("api/events/1/bids")
        #
        #   resp.status
        #   => 200
        #
        #   resp.body
        #   => {"message": "translation missing: en.send_instructions"}
        #



  # POST /resource/invitation
  def create
    
    self.resource = invite_resource

    if resource.errors.empty?
      yield resource if block_given?
      if self.resource.invitation_sent_at

        render json: {
          message: t(:send_instructions, :email => self.resource.email)
        }, status: :ok
        return
      end

      render json: {
        message: 'email could not be send'
      }, status: 503

    else

      render json: {
        errors: resource.errors
      }, status: 422 

    end
  end


  # PUT /resource/invitation
  def update
#    self.resource = accept_resource
    
    if resource.errors.empty?
      yield resource if block_given?
#      flash_message = resource.active_for_authentication? ? :updated : :updated_not_active
#      set_flash_message :notice, flash_message if is_flashing_format?
        sign_in(resource_name, resource)  
        render json: resource_class.after_sign_in_info(resource), status: :ok
    else
        render json: {
          errors: resource.errors
        }, status: 422


    end
  end


  protected



  def invite_resource(&block)
    resource_class.invite!(invite_params, current_inviter, &block)
  end

  def accept_resource
#    resource_class.accept_invitation!(update_resource_params)
      params.require(:user).permit(:password, :password_confirmation) 
  end

  def current_inviter
    authenticate_inviter!
  end

  def has_invitations_left?
    unless current_inviter.nil? || current_inviter.has_invitations_left?
      self.resource = resource_class.new
      set_flash_message :alert, :no_invitations_remaining if is_flashing_format?
      respond_with_navigational(resource) { render :new }
    end
  end

  def resource_from_invitation_token

    unless params[:invitation_token] && self.resource = resource_class.find_by_invitation_token(params[:invitation_token])
        render json: {
          message: "invalid token or token is missing"
          }, status: 401     
    end
  end

  def invite_params
#    devise_parameter_sanitizer.sanitize(:invite)

    params.require(:invite).permit(:email)      
  end

  def update_resource_params
    devise_parameter_sanitizer.sanitize(:accept_invitation)
  end


  private

    def skip_set_cookies_header
      request.session_options = {}
    end

    def mera_method
      sign_in resource_name, User.find_by_email(params[:user_email])
    end

  def authenticate_user_from_token!
    user_email = params[:user_email].presence
    user       = user_email && User.find_by_email(user_email)
    # Notice how we use Devise.secure_compare to compare the token
    # in the database with the token given in the params, mitigating
    # timing attacks.
    if user && Devise.secure_compare(user.authentication_token, params[:user_token])
      sign_in user, store: false
    else
      render json: {:errors => ["Invalid login credentials"]}, :status => 401
    end
  end


end