class Api::V1::CustomDevise::OmniauthCallbacksController < Devise::OmniauthCallbacksController

  skip_before_filter :authenticate_user!
  skip_before_filter :authenticate_user_from_token!

  def all

    user = User.from_omniauth(env['omniauth.auth'], current_user)
    if user.persisted?

        sign_in user
        user.reset_authentication_token!
        user.save!

        render json: {
          first_name: user.first_name,
          last_name: user.last_name,
          email: user.email,
          auth_token: user.authentication_token,
        }, status: :ok

    else

        render json: {
            result: "unable to register, please try again"
        }, status: :ok

    end
  end

  User::SOCIALS.each do |k, _|
    alias_method k, :all
  end

end