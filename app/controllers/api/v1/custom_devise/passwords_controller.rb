class Api::V1::CustomDevise::PasswordsController < Devise::PasswordsController

    after_filter :skip_set_cookies_header
    prepend_before_filter :require_no_authentication
    # Render the #edit only if coming from a reset password email link
    append_before_filter :assert_reset_token_passed, only: :edit
    skip_before_filter :verify_authenticity_token
    respond_to :json
    # POST /resource/password
    
    ##
        # = Reset Password Token
        #
        # Send a Email to User Email Address to get Reset Password Token.
        #
        # POST /api/users/password
        #
        # params:
        #   email -- User Email Address
        #
        # = Examples
        #
        #   resp = conn.post("/api/users/password", "user_email" => "someone@example.com")
        #
        #   resp.status
        #   => 200
        #
        #   resp.body
        #   => {"message": "Email sent."}
        # 
        #   Invalid Email Address
        #
        #   resp = conn.post("/api/users/password", "user_email" => "someone@example.com")
        #
        #   resp.status
        #   => 503
        #
        #   resp.body
        #   => {"message": "Error while sending email."}
        #

    def create
        self.resource = resource_class.send_reset_password_instructions({:email=>params[:email]})
        yield resource if block_given?

        if successfully_sent?(resource)
            render json: {:message=>"Email sent."}, status: 200
        else
            render json: {:message =>"Error while sending email."}, status: 503
        end
    end

    # GET /resource/password/edit?reset_password_token=abcdef
    def edit
        self.resource = resource_class.new
        resource.reset_password_token = params[:reset_password_token]
        render json: params
    end

    # PUT /resource/password
    ##
        # = Update Password 
        #
        # Updates User password .
        #
        # POST /api/users/password
        #
        # params:
        #   reset_password_token -- reset password token sent to user by email
        #   password -- new password
        #   password_confirmation -- confirm new password
        #
        # = Examples
        #
        #   resp = conn.put("/api/users/password", "reset_password_token" => "62RsVxucU8sPcm8Kxzzq")
        #
        #   resp.status
        #   => 202
        #
        #   resp.body
        #   => {"message":"Password reset successfully."}
        # 
        #   Invalid Reset Password Token
        #
        #   resp = conn.put("/api/users/password", "reset_password_token" => "62RsVxucU8sPcm8Kxzzq")
        #
        #   resp.status
        #   => 422
        #
        #   resp.body
        #   => {"message":["Reset password token is invalid"]}

    def update


        self.resource = resource_class.reset_password_by_token(
                            {:email=>params[:email],
                            :reset_password_token=>params[:reset_password_token],
                            :password=>params[:password],
                            :password_confirmation=>params[:password_confirmation]}
                            )
        yield resource if block_given?
        if resource.errors.empty?
            resource.unlock_access! if unlockable?(resource)
            flash_message = resource.active_for_authentication? ? :updated : :updated_not_active
            sign_in(resource_name, resource)
            render json: {:message => "Password reset successfully."}, status: 202
        else
            render json: {:message => resource.errors.full_messages}, status: 422
        end
    end

    protected

    # Check if a reset_password_token is provided in the request
    def assert_reset_token_passed
        if params[:reset_password_token].blank?
            render json: {:errors => "token failed"}
        end
    end

    # Check if proper Lockable module methods are present & unlock strategy
    # allows to unlock resource on password reset
    def unlockable?(resource)
        resource.respond_to?(:unlock_access!) &&
        resource.respond_to?(:unlock_strategy_enabled?) &&
        resource.unlock_strategy_enabled?(:email)
    end

    private
    def skip_set_cookies_header
      request.session_options = {}
    end
end
