class Api::V1::CustomDevise::SessionsController < Devise::SessionsController

  after_filter :skip_set_cookies_header

  prepend_before_filter :require_no_authentication, :only => [:create ]
  prepend_before_filter :allow_params_authentication!, only: :create
  skip_before_filter :verify_authenticity_token

  include Devise::Controllers::Helpers


	##
        # = Authenication Token For User Login
        #

  respond_to :json

  ## => User Login

        # Returns a Authenication Token For User Login
        #
        # POST /api/v1/users/sign_in
        #
        # params:
        #   user[email] -- User Email Address
        #   user[password] -- User password
        #
        # = Examples
        #
        #   resp = conn.post("/api/v1/users")
        #
        #   resp.status
        #   => 200
        #
        #   resp.body
        #   => [{"project": "first_name": "Saad", "last_name": "Masood", "email": "saad@example.com", "auth_token": "AXArxyZNcdjzRyc7YVP6"}]
        #   resp = conn.get("/api/v1/users", "user_token" => "dcbb7b36acd4438d07abafb8e28605a4")
        #
        #   resp.status
        #   => 200
        #
        #   resp.body
        #   => {"errors": "Invalid login credentials"}
        #

  def create
    self.resource = warden.authenticate!(auth_options)
    sign_in(resource_name, resource)
    resource.reset_authentication_token!
    resource.save!
    render json: {
        first_name: resource.first_name,
        last_name: resource.last_name,
        email: resource.email,
        auth_token: resource.authentication_token,
    }, status: :ok
  end

  ## => User log out

        # Destroy Session For Login User
        # Send User Email Addess and Password to Destroy User Session or Sign-Out the User from our system
        #
        # DELETE /api/v1/users/sign_out
        #
        # params:
        #   user[email] -- User Email Address
        #   user[password] -- User password
        #
        # = Examples
        #
        #   resp = conn.delete("/api/v1/users")
        #
        #   resp.status
        #   => 204
        #
  def destroy
    sign_out(resource_name)
    render json: {}, status: 204
  end


  private
    def skip_set_cookies_header
      request.session_options = {}
    end

end
