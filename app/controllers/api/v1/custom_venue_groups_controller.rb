class Api::V1::CustomVenueGroupsController < ApiController

    before_filter :set_event, :only => [:index, :destroy]
    before_filter :set_custom_group, :only => [:destroy]
    skip_before_filter :authenticate_user_from_token!, :only => [:show, :index]
    rescue_from ActiveRecord::RecordNotFound, :with => :record_not_found

    def index        
        @custom_venue_groups = @user.custom_venue_groups.where(:event_id => @event.id)
        render status: 200
    end

    def create

        @custom_venue_group = CustomVenueGroup.create custom_venue_group_params
        @venue_rows     = params[:venue_rows][:id].split(",")
        @venue_rows.each do |venue_row_id|
            CustomVenueRow.create(:venue_row_id => venue_row_id, :custom_venue_group_id => @custom_venue_group.id)
        end

        if @custom_venue_group.save
            render status: 201
        else
            render status: 422, json: {
                :errors   => @custom_venue_group.errors
            }
        end

    end

    def destroy
        @custom_venue_group.delete
        render json: {}, status: 204
    end

    private

        def custom_venue_group_params
            params.require(:custom_venue_group).permit(:name, :event_id).merge(:user_id => @user.id)
        end

        def set_custom_group
            @custom_venue_groups    = @user.custom_venue_groups.where :event_id => @event.id
            @custom_venue_group     = @custom_venue_groups.find params[:id]
        end

        def set_event
            @event = Event.find params[:event_id]
        end

end