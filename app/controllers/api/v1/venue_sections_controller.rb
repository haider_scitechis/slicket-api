class Api::V1::VenueSectionsController < ApiController
  rescue_from ActiveRecord::RecordNotFound, :with => :record_not_found
  skip_before_filter :authenticate_user_from_token!, :only => [:show, :index]
## => Venue CRUD


  ##
        # = List Of All Venus Section
        #
        # Return a List Of All Venus Section In Related to One Venue
        #
        # Need a Venue ID For Getting Venue Section. 
        # Need a user email and authentication token for get the list of venues section. 
        # Send user email and authentication token as url params. 
        #
        # Get /api/venues/id/venue_sections
        #
        # url_params
        #   user_email -- someone@example.com
        #   user_token -- xg282iD2oNTq8hgiyhhF
        #
        # = Examples
        #
        #   resp = conn.post("/api/venues/id/venue_sections")
        #
        #   resp.status
        #   => 200
        #
        #   resp.body
        #   => {"venue_sections": [{"venue_section": {"id": 1,"venue_id": 6,"raphael_data": null,"color": "blue","section_text": null,"section_number": "2","row_start": "1","row_end": "10","photo": null,"created_at": "2014-10-20T10:08:49.000Z","updated_at": "2014-10-20T10:08:49.000Z"},"venue_rows": []}]
        #
        #   resp = conn.get("/api/venues/id/venue_sections")
        #
        #   resp.status
        #   => 201
        #
        #   resp.body
        #   => {}

  def index
    @venue_sections = Venue.find(params[:venue_id]).venue_sections
    render status: 200
  end


  ##
        # = Create a Venue Section
        #
        # Creating a Venue Section
        #
        # Need a Venue Section ID. 
        # Need a Venue ID For Creating Venue Section. 
        # Need a user email and authentication token for creating a new venues section and related it to venue. 
        # Send user email and authentication token as url params. 
        #
        # POST /api/venues/id/venue_sections
        #
        # url_params
        #   user_email -- someone@example.com
        #   user_token -- xg282iD2oNTq8hgiyhhF
        #
        # params:
        #
        #  params               = {} 
        #  params[:venue_id]    = 1 
        #  params[:disclosures_attributes]  = [{:name=>"diclousure 1", :desc=>"Please keep stadium clean"}, {:name=>"disclosure 2", :desc=>"Please keep an eye on your things"}]
        #
        # = Examples
        #
        #   resp = conn.post("/api/venues/id/venue_sections")
        #
        #   resp.status
        #   => 201
        #
        #   resp.body
        #   => {"message": "venue_section has been created successfully"}
        #



  def create

    @venue_section = Venue.find(params[:venue_id]).venue_sections.build(get_params_create)
    if @venue_section.save
      render status: 201, json: {message: "venue_section has been created successfully"}
    else
      render status: 422, json:{error: @venue_section.errors}
    end
  end


  ##
        # = Show Venue Section
        #
        # Return a Single Venue Section
        #
        # Need a Venue Section ID. 
        # Need a Venue ID For getting Venue Section. 
        # Need a user email and authentication token for get a venues section related to the venue. 
        # Send user email and authentication token as url params. 
        #
        # GET /api/venues
        #
        # url_params
        #   user_email -- someone@example.com
        #   user_token -- xg282iD2oNTq8hgiyhhF
        #
        # = Examples
        #
        #   resp = conn.get("/api/venues/id/venue_sections/id")
        #
        #   resp.status
        #   => 200
        #
        #   resp.body
        #   => { "venue_section": {"id":1,"category":null,"subcategory":null,"venue_id":6,"street_address":null,"team":null,"city":null,"state":null,"zip":null,"phone":null,"description":null,"school":null,"team_name":null,"mascot":null,"venue_rows":[]}}
        #
        #   resp = conn.get("/api/venues/id/venue_sections/id")
        #
        #   resp.status
        #   => 404
        #
        #   resp.body
        #   => { "message": "No Record Found"}
        #

  def show
    @venue_section = Venue.find(params[:venue_id]).venue_sections.find(params[:id])
    render  status:200
  end

  ##
        # = Update a Venue Section
        #
        # Updating a Venue Section by Venue ID
        #
        # Need a Venue Section ID. 
        # Need a Venue ID For Updating Venue Section. 
        # Need a user email and authentication token for updating the existing venues section related to the venue. 
        # Send user email and authentication token as url params. 
        #
        # Patch /api/venues/1/venue_sections/1
        #
        # url_params
        #   user_email -- someone@example.com
        #   user_token -- xg282iD2oNTq8hgiyhhF
        #
        # params:
        #   color -- Color Name
        #   section_text -- economy
        #   section -- Section Name A.B,C...
        #   row_start -- Starting Number
        #   row_end -- Ending Number
        #
        # = Examples
        #
        #   resp = conn.patch("/api/venues/id/venue_sections/id")
        #
        #   resp.status
        #   => 204
        #
        #   resp.body
        #   => {}
        #
        #   resp = conn.patch("/api/venues/id/venue_sections/id")
        #
        #   resp.status
        #   => 404
        #
        #   resp.body
        #   => {"message": "No Record Found"}
        #

  def update
    @venue_section = Venue.find(params[:venue_id]).venue_sections.find(params[:id])
    if @venue_section.update(get_params_update)
      render status: 204, json: {}
    else
      render status: 422, json:{errors: @venue_section.errors}
    end
  end

  ##
        # Destroy a Venue Section
        #
        # Deleting a Venue Section by Venue ID
        #
        # Need a Venue Section ID. 
        # Need a Venue ID For Deleting Venue Section. 
        # Need a user email and authentication token for deleting the venue section related to the venue. 
        # Send user email and authentication token as url params. 
        #
        # Delete /api/venues/id/venue_sections/id
        #
        # url_params
        #   user_email -- someone@example.com
        #   user_token -- xg282iD2oNTq8hgiyhhF
        #
        # = Examples
        #
        #   resp = conn.delete("/api/venues/id/venue_sections/id")
        #
        #   resp.status
        #   => 204
        #
        #   resp.body
        #   => {}
        #
        #   resp = conn.delete("/api/venues/id/venue_sections/id")
        #
        #   resp.status
        #   => 404
        #
        #   resp.body
        #   => {"message": "No Record Found"}
        #

  def destroy
    @venue_section = Venue.find(params[:venue_id]).venue_sections.find(params[:id])
    if @venue_section.destroy()
      render status: 204, json: {}
    else
      render status: 422, json:{errors: @venue_section.errors}
    end
  end

  ## => List sections by venue rows

  ##
        # = List Of Rows
        #
        # Return a List Of Venus Group In Related to One Venue Based on Given Row Number
        #
        # Need a Venue Section ID. 
        # Need a Venue ID For Getting Venue Section. 
        # Need a user email and authentication token for deleting the venue section related to the venue. 
        # Send user email and authentication token as url params. 
        #
        # Get /api/venues/id/venue_sections/list_by_row
        #
        # url_params
        #   user_email -- someone@example.com
        #   user_token -- xg282iD2oNTq8hgiyhhF
        #
        # params:
        #   row_num -- Row Number
        #
        # = Examples
        #
        #   resp = conn.post("/api/venues/id/venue_sections/list_by_row")
        #
        #   resp.status
        #   => 200
        #
        #   resp.body
        #   => {"venue_sections": [{"venue_section": {"id": 1,"venue_id": 6,"raphael_data": null,"color": "blue","section_text": null,"section_number": "2","row_start": "1","row_end": "10","photo": null,"created_at": "2014-10-20T10:08:49.000Z","updated_at": "2014-10-20T10:08:49.000Z"},"venue_rows": []}]
        #
        #   resp = conn.get("/api/venues/id/venue_sections/list_by_row")
        #
        #   resp.status
        #   => 201
        #
        #   resp.body
        #   => {}

  def list_by_row
    @venue_sections = Venue.find(params[:venue_id]).venue_sections.where("row_start <= ? AND row_end >= ?",params[:row_num].to_i,params[:row_num].to_i)
    render status: 200
  end


  private
  def get_params_create
    params.permit(:venue_id, :raphael_data,
            :color, :section_text, :section_number,
            :row_start, :row_end, :photo,:row_num,
            :disclosures_attributes => [:name, :desc])
  end
  def get_params_update
    params.permit(:venue_id, :raphael_data,
            :color, :section_text, :section_number,
            :row_start, :row_end, :photo,:row_num,
            :disclosures_attributes => [:name, :desc])
  end
end
