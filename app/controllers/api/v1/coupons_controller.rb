class Api::V1::CouponsController < ApiController
  rescue_from ActiveRecord::RecordNotFound, :with => :record_not_found
  #before_filter :check_if_admin, :only => [:create, :destroy]
  skip_before_filter :authenticate_user_from_token!, :only => [:show, :index]
## => Coupons


##
        # = List of all Coupons
        #
        # Return a List of All Coupons
        #
        # Need a user email and authentication token for getting the list of Coupons. 
        # Send user email and authentication token as url params. 
        #
        # Get /api/coupons
        #
        # url_params
        #   user_email -- someone@example.com
        #   user_token -- xg282iD2oNTq8hgiyhhF
        #
        # = Examples
        #
        #   resp = conn.get("/api/coupons")
        #
        #   resp.status
        #   => 200
        #
        #   resp.body
        #   => {"coupons":[{"coupon":{"id":1,"coupon_number":"skjdbfb","amount":null,"is_percentage":null,"created_at":"2014-11-06T11:58:40.000Z","updated_at":"2014-11-06T11:58:40.000Z"}}]}
        #

  def index
    @coupons = Coupon.all
    render status: 200
  end

  ##
        # = Create Coupon
        #
        # Creating a New Coupon
        #
        # Need a user email and authentication token for creating the new coupon. 
        # Send user email and authentication token as url params. 
        #
        # POST /api/coupons
        #
        # url_params
        #   user_email -- someone@example.com
        #   user_token -- xg282iD2oNTq8hgiyhhF
        #
        # params:
        #   coupon_number -- Unique Coupon Number
        #   amount -- Amount of Discount
        #   is_percentage -- Is Coupon Percetage Based
        #
        # = Examples
        #
        #   resp = conn.post("/api/coupons")
        #
        #   resp.status
        #   => 201
        #
        #   resp.body
        #   => {"coupon":{"id":1,"coupon_number":"skjdbfb","amount":null,"is_percentage":null,"created_at":"2014-11-06T11:58:40.000Z","updated_at":"2014-11-06T11:58:40.000Z"}}
        #
        #   resp = conn.post("/api/coupons")
        #
        #   resp.status
        #   => 422
        #
        #   resp.body
        #   => {"message": {"coupon_number": [ "can't be blank" ]}}
        #

  def create
    @coupon = Coupon.new(get_create_params)
    if @coupon.save
      render status: 201
    else
      render status: 422, json:{messages: @coupon.errors}
    end
  end

  ##
        # = Shows a Coupon
        #
        # Returns a Coupon
        #
        # Need a user email and authentication token for getting the Coupon. 
        # Send user email and authentication token as url params. 
        #
        # Get /api/coupons
        #
        # url_params
        #   user_email -- someone@example.com
        #   user_token -- xg282iD2oNTq8hgiyhhF
        #
        # = Examples
        #
        #   resp = conn.get("/api/coupons/:id")
        #
        #   resp.status
        #   => 200
        #
        #   resp.body
        #   => {"coupons":[{"coupon":{"id":1,"coupon_number":"skjdbfb","amount":null,"is_percentage":null,"created_at":"2014-11-06T11:58:40.000Z","updated_at":"2014-11-06T11:58:40.000Z"}}]}
        #
        #   resp = conn.put("/api/coupons/:id")
        #
        #   resp.status
        #   => 404
        #
        #   resp.body
        #   => {"message":"No Record Found"}

  def show
    @coupon = Coupon.find(params[:id])
    render status: 200
  end

    ##
        # = Update a Coupon
        #
        # Updating a Coupon
        #
        # Need a user email and authentication token for updating the coupon. 
        # Send user email and authentication token as url params. 
        #
        # POST /api/coupons
        #
        # url_params
        #   user_email -- someone@example.com
        #   user_token -- xg282iD2oNTq8hgiyhhF
        #
        # params:
        #   coupon_number -- Unique Coupon Number
        #   amount -- Amount of Discount
        #   is_percentage -- Is Coupon Percetage Based
        #
        # = Examples
        #
        #   resp = conn.patch("/api/coupons/:id")
        #
        #   resp.status
        #   => 204
        #
        #   resp.body
        #   => {}
        #
        #   resp = conn.patch("/api/coupons/:id")
        #
        #   resp.status
        #   => 422
        #
        #   resp.body
        #   => {"message": {"coupon_number": [ "can't be blank" ]}}
        #

  def update
    @coupon = Coupon.find(params[:id])
    if @coupon.update(get_update_params)
      render status: 204, json: {}
    else
      render status: 422, json:{messages: @coupon.errors}
    end
  end

  ##
        # = Delete a Coupon
        #
        # Deleting a Coupon
        #
        # Need a user email and authentication token for deleting the new coupon. 
        # Send user email and authentication token as url params. 
        #
        # POST /api/coupons
        #
        # url_params
        #   user_email -- someone@example.com
        #   user_token -- xg282iD2oNTq8hgiyhhF
        #
        #
        # = Examples
        #
        #   resp = conn.delete("/api/coupons/:id")
        #
        #   resp.status
        #   => 204
        #
        #   resp.body
        #   => {}
        #
        #   resp = conn.delete("/api/coupons/:id")
        #
        #   resp.status
        #   => 404
        #
        #   resp.body
        #   => {"message":"No Record Found"}
        #

  def destroy
    @coupon = Coupon.find(params[:id])
    if @coupon.destroy
      render status: 204, json: {}
    else
      render status: 422, json:{messages: @coupon.errors}
    end
  end

  private
  def get_create_params
    params.permit(:coupon_number, :amount, :is_percentage)
  end

  def get_update_params
    params.permit(:coupon_number, :amount, :is_percentage)
  end

end
