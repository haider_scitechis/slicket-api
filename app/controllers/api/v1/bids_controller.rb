class Api::V1::BidsController < ApiController

# => Bid CRUD

  before_filter :set_event, :only => [:index, :create,:show, :update, :destroy]
  before_filter :set_event_bid, :only => [:update, :destroy]
  before_filter :set_twilio, :only => [:create]
  skip_before_filter :authenticate_user_from_token!, :only => [:show, :index]
  
  rescue_from ActiveRecord::RecordNotFound, :with => :record_not_found

  ##
        # => List all bids for event
        #
        # Return a List of All bids for given event_id
        #
        # => Required Parameters
        # Only event_id is required as url parameter
        #
        # => Example 1
        #
        # Get /api/events/:event_id/bids
        #
        #   resp = conn.get("/api/events/57/bids")
        #
        #   resp.status
        #   => 200
        #
        #   resp.body
        #   => {"bid":{"id":1,"event_id":57,"user_id":5,"venue_group_id":9,"price":100,"quantity":2,"proxy":null,"expire_at":"2014-11-13T00:00:00.000Z","cancelothers":1,"status":null,"in_hand_date":null,"is_negotiation":true,"expacted_ship_date":null,"created_at":"2014-11-06T08:13:46.000Z","updated_at":"2014-11-06T08:13:46.000Z"},"event":{"id":57,"name":"Gerard Spencer III","datetime":"2014-10-31T00:00:00.000Z","description":"Accusamus dicta consequatur accusantium quia itaque voluptatum. Sit amet asperiores delectus et inventore odit earum. In ipsa quia necessitatibus excepturi fugit. Quo ipsa minima earum et.","primary":true,"primary_pdf":"http://robohash.org/utreprehenderitrem.png?size=300x300","setgeek_id":"2","venue_id":8,"created_at":"2014-11-06T08:13:37.000Z","updated_at":"2014-11-06T08:13:37.000Z","event_type":"IB","lat":null,"lang":null},"user":{"id":5,"email":"john0.7128589735707505@example.com!","created_at":"2014-11-06T08:13:37.000Z","updated_at":"2014-11-06T08:13:37.000Z","sale_commission":null,"negotiation_commission":null,"title":"someTitle","subscribe":null,"username":"John4","first_name":"John4","middle_name":null,"last_name":"Doe","phone":null,"phone_daytime":null,"phone_evening":null,"phone_cell":null},"venue_group":{"id":9,"name":"Wilhelmine Monahan","rating":5,"event_id":58,"created_at":"2014-11-06T08:13:37.000Z","updated_at":"2014-11-06T08:13:37.000Z"}}
        #
        # => Example 2
        #
        # Get /api/events/:event_id/bids
        #
        #   resp = conn.get("/api/events/5777/bids")
        #
        #   resp.status
        #   => 404
        #
        #   resp.body
        #   => {"message":"No Record Found"}

  def index

    @bids = @event.bids.all
#    render status: 200, json:{result: @bids}
    render status: 200

  end


  ##
        # = Create Bid for Event
        #
        # Creating a New Bid of event
        #
        # => Required Parameters
        # 
        # Need bids parameters :user_id, event_id, venue_group_id, price, quantity, expire_at, cancelothers, is_negotiation
        #
        # POST /api/events/:event_id/bids
        #
        # url_params
        #
        #   event_id -- 10
        #   user_email -- slicket.ticket@gmail.com
        #   user_token -- sfJlsdf2323n
        #
        # = Examples
        #
        #  params          = {} 
        #  params[:bid]    = {}
        #  params[:bid][:venue_group_id] = venue_group.id
        #  params[:bid][:event_id]       = event.id
        #  params[:bid][:price]          = 100
        #  params[:bid][:quantity]       = 2
        #  params[:bid][:cancelothers]   = true
        #  params[:bid][:is_negotiation] = true
        #  params[:bid][:expire_at]      = Mon, 24 Nov 2014
        #   resp = conn.post("api/events/1/bids")
        #
        #   resp.status
        #   => 201
        #
        #   resp.body
        #   => {"result":{"id":2,"event_id":1,"user_id":1,"venue_group_id":1,"price":500,"quantity":2,"proxy":null,"expire_at":"2014-10-30T13:59:10.000Z","cancelothers":0,"status":null,"in_hand_date":null,"is_negotiation":true,"expacted_ship_date":null,"created_at":"2014-11-06T10:32:18.316Z","updated_at":"2014-11-06T10:32:18.316Z"}}
        #
        #   resp = conn.post("api/events/1/bids")
        #
        #  params          = {} 
        #  params[:bid]    = {}
        #  params[:bid][:venue_group_id] = venue_group.id
        #  params[:bid][:event_id]       = event.id
        #  params[:bid][:price]          = 100
        #  params[:bid][:cancelothers]   = true
        #  params[:bid][:is_negotiation] = true
        #  params[:bid][:expire_at]      = Mon, 24 Nov 2014
        #   resp.status
        #   => 422
        #
        #   resp.body
        #   => {"errors":{"quantity":["can't be blank"]}}
        #


  def create
    @bid          = @event.bids.create bid_params
    if @user.commission_shipping_configs.where(:name => "commission", :action_type => "buyer").first.nil? then amount = 20 else amount = @user.commission_shipping_configs.where(:name => "commission", :action_type => "buyer").first.amount end
    commission    = amount/@bid.quantity
    @bid.price    = @bid.price - commission
    if @bid.proxy 
      #binding.pry
      @bid.proxy    = @bid.proxy - commission
    end
    if  @bid.errors.blank?

      post_to_twitter @bid
      send_notifications @bid
      post_to_facebook @bid
      render status: 201, json:{result: @bid}

    else
      render status: 422, json:{errors: @bid.errors}
    end

  end

    ##
        # = Update Bid
        #
        # Update a Bid for Event
        #
        # Need bids parameters ( any parameters except user, event and venue_group)
        #
        # Patch /api/events/:event_id/bids/:id
        #
        # url_params
        #   event_id  -- 57
        #   id        -- 1
        #   user_email -- slicket.ticket@gmail.com
        #   user_token -- sfJlsdf2323n
        #
        # = Examples
        #
        #   resp = conn.patch("/api/events/57/bids/1")
        #
        #  params          = {} 
        #  params[:bid]    = {}
        #  params[:bid][:price]          = 100
        #  params[:bid][:cancelothers]   = false
        #  params[:bid][:is_negotiation] = true
        #
        #   resp.status
        #   => 204
        #
        #   resp.body
        #   => {}
        #


  def update

    if @bid.update(update_bid_params)
      if @user.commission_shipping_configs.where(:name => "commission", :action_type => "buyer").first.nil? then amount = 20 else amount = @user.commission_shipping_configs.where(:name => "commission", :action_type => "buyer").first.amount end
      commission    = amount/@bid.quantity
      @bid.price    = @bid.price - commission
      if @bid.proxy 
        @bid.proxy    = @bid.proxy - commission
      end
      @bid.save
      render  status: 204, json: {}
    else
      render json: { :message => @bid.errors }, status: 401
    end

  end

  ##
        # = Show bid 
        #
        # Return a Single Bid of Event
        #
        # Need a Event Id
        # Need a BID Id
        #
        # Get /api/events/:event_id/bids/:id
        #
        # url_params
        #   event_id -- 57
        #   id -- 1
        #   event_id -- 10
        #   user_email -- slicket.ticket@gmail.com
        #   user_token -- sfJlsdf2323n

        #
        # = Examples
        #
        #   resp = conn.get("/api/events/57/bids/1")
        #
        #   resp.status
        #   => 200
        #
        #   resp.body
        #   => {"bid":{"id":1,"event_id":57,"user_id":5,"venue_group_id":9,"price":100,"quantity":2,"proxy":null,"expire_at":"2014-11-13T00:00:00.000Z","cancelothers":1,"status":null,"in_hand_date":null,"is_negotiation":true,"expacted_ship_date":null,"created_at":"2014-11-06T08:13:46.000Z","updated_at":"2014-11-06T08:13:46.000Z"},"user":{"id":5,"email":"john0.7128589735707505@example.com!","created_at":"2014-11-06T08:13:37.000Z","updated_at":"2014-11-06T08:13:37.000Z","sale_commission":null,"negotiation_commission":null,"title":"someTitle","subscribe":null,"username":"John4","first_name":"John4","middle_name":null,"last_name":"Doe","phone":null,"phone_daytime":null,"phone_evening":null,"phone_cell":null},"event":{"id":57,"name":"Gerard Spencer III","datetime":"2014-10-31T00:00:00.000Z","description":"Accusamus dicta consequatur accusantium quia itaque voluptatum. Sit amet asperiores delectus et inventore odit earum. In ipsa quia necessitatibus excepturi fugit. Quo ipsa minima earum et.","primary":true,"primary_pdf":"http://robohash.org/utreprehenderitrem.png?size=300x300","setgeek_id":"2","venue_id":8,"created_at":"2014-11-06T08:13:37.000Z","updated_at":"2014-11-06T08:13:37.000Z","event_type":"IB","lat":null,"lang":null},"venue_group":{"id":9,"name":"Wilhelmine Monahan","rating":5,"event_id":58,"created_at":"2014-11-06T08:13:37.000Z","updated_at":"2014-11-06T08:13:37.000Z"}}
        #
        #   resp = conn.get("//api/events/1/bids/1")
        #
        #   resp.status
        #   => 404
        #
        #   resp.body
        #   => {"message":"No Record Found"}
        #

  def show
     @bid  = @event.bids.find(params[:id]) 
     render status: 200
  end


    ##
        # = Delete Bid
        #
        # Delete a Bid of Event
        #
        # Need a Event ID. 
        # Need a Bid ID. 
        #
        # Delete /api/events/:event_id/bids/:id
        #
        # url_params
        #   event_id -- 1
        #   id -- 2
        #
        # = Examples
        #
        #   resp = conn.delete("/api/events/1/bids/2")
        #
        #   resp.status
        #   => 204
        #
        #   resp.body
        #   => {}
        #
        #   resp = conn.delete("/api/events/1/bids/2")
        #
        #   resp.status
        #   => 404
        #
        #   resp.body
        #   => {"message": "Record Not Found"}
        #

  def destroy

    if @bid.destroy()
      render status: 204, json: {}
    else
      render status: 422, json:{errors: @bid.errors}
    end

  end

  ##
        # => List all bids of a user for event
        #
        # Return a List of All bids of a user for given event_id
        #
        # => Required Parameters
        # Event_id and user_email is required as url parameter
        #
        # => Example 1
        #
        # Get /api/events/:event_id/bids
        # url_params
          #   user_email -- someone@example.com
          #   user_token -- xg282iD2oNTq8hgiyhhF
        #
        #   resp = conn.get("/api/events/57/bids")
        #
        #   resp.status
        #   => 200
        #
        #   resp.body
        #   => {"bid":{"id":1,"event_id":57,"user_id":5,"venue_group_id":9,"price":100,"quantity":2,"proxy":null,"expire_at":"2014-11-13T00:00:00.000Z","cancelothers":1,"status":null,"in_hand_date":null,"is_negotiation":true,"expacted_ship_date":null,"created_at":"2014-11-06T08:13:46.000Z","updated_at":"2014-11-06T08:13:46.000Z"},"event":{"id":57,"name":"Gerard Spencer III","datetime":"2014-10-31T00:00:00.000Z","description":"Accusamus dicta consequatur accusantium quia itaque voluptatum. Sit amet asperiores delectus et inventore odit earum. In ipsa quia necessitatibus excepturi fugit. Quo ipsa minima earum et.","primary":true,"primary_pdf":"http://robohash.org/utreprehenderitrem.png?size=300x300","setgeek_id":"2","venue_id":8,"created_at":"2014-11-06T08:13:37.000Z","updated_at":"2014-11-06T08:13:37.000Z","event_type":"IB","lat":null,"lang":null},"user":{"id":5,"email":"john0.7128589735707505@example.com!","created_at":"2014-11-06T08:13:37.000Z","updated_at":"2014-11-06T08:13:37.000Z","sale_commission":null,"negotiation_commission":null,"title":"someTitle","subscribe":null,"username":"John4","first_name":"John4","middle_name":null,"last_name":"Doe","phone":null,"phone_daytime":null,"phone_evening":null,"phone_cell":null},"venue_group":{"id":9,"name":"Wilhelmine Monahan","rating":5,"event_id":58,"created_at":"2014-11-06T08:13:37.000Z","updated_at":"2014-11-06T08:13:37.000Z"}}
        #
        # => Example 2
        #
        # Get /api/events/:event_id/bids
        #
        #   resp = conn.get("/api/events/5777/bids")
        #
        #   resp.status
        #   => 404
        #
        #   resp.body
        #   => {"message":"No Record Found"}

  def all_user_bids
    @bids = Event.find(params[:event_id]).bids.where(:user_id => @user.id)
    render status: 200
  end

    ## => Get Offers for an bid

##
      # = Get Offers for an bid
      #
      # Get Offers that are related to Given bid
      #
      # Need a Event ID For Getting Offers 
      # Need a user email and authentication token for get the list of bids. 
      # Send user email and authentication token as url params. 
      #
      # url_params
          #   user_email -- someone@example.com
          #   user_token -- xg282iD2oNTq8hgiyhhF
      #
      # Get /api/events/:id/offers/get_notifications
      #
      # = Examples
      #
      #   resp = conn.get("/api/events/:id/bids/:id/get_notifications")
      #
      #   resp.status
      #   => 200
      #
      #   resp.body
      #   => {"bids":[]}
      #
      # Get /api/events/:id/bids/:id/get_notifications
      #
      # = Examples
      #
      #   resp = conn.get("/api/events/:id/bids/:id/get_notifications")
      #
      #   resp.status
      #   => 200
      #
      #   resp.body
      #   => {"offers":[{"id":11,"venue_group_id":4,"event_id":107,"user_id":1,"ticket_type_id":1,"price":100.0,"proxy":null,"multiple":null,"cancelothers":1,"status":null,"in_hand_date":null,"expected_ship_date":null,"is_negotiation":null,"expires_at":null,"confirm_transtaction":null,"piggybacked":null,"notes":null,"created_at":"2014-11-17T08:02:12.000Z","updated_at":"2014-11-17T08:02:12.000Z","tickets_file_name":null,"tickets_content_type":null,"tickets_file_size":null,"tickets_updated_at":null}]}


  def get_notifications
    @bid      = Event.find(params[:event_id]).bids.find(params[:bid_id])
    @offers   = get_offers @bid
    @offers   = @offers.sort_by{|h| h[:created_at]}.reverse
    render status: 200

  end

  ## => Sell now

##
      # = Sell now offer to a bid
      #
      # Send email to Bidder to sell offer now
      #
      # Need a Event ID For Getting Offers 
      # Need a user email and authentication token for get the list of offers. 
      # Send user email and authentication token as url params. 
      #
      # url_params
          #   user_email -- someone@example.com
          #   user_token -- xg282iD2oNTq8hgiyhhF
          #   bid_id  --  Bid id 
      #
      # Post /api/events/:id/offers/:id/sell_now
      #
      # = Examples
      #
      #   resp = conn.post("/api/events/:id/offers/:id/sell_now")
      # => params[:bid_id]=1
      #
      #   resp.status
      #   => 200
      #
      #   resp.body
      #   => {"message":"email sent"}
      #
      # Post /api/events/:id/offers/:id/sell_now
      #
      # = Examples
      #
      #   resp = conn.post("/api/events/:id/offers/:id/sell_now")
      # => params[:bid_id]=2
      #
      #   resp.status
      #   => 404
      #
      #   resp.body
      #   => {"message": "No Record Found"}


  def buy_now
    @bid = Bid.find(params[:bid_id])
    @offers = get_offers(@bid)
    @offer=[]
    @offers.each do |offer|
      @offer << offer if offer.id == params[:offer_id].to_i
    end
    @offer=@offer.uniq
    if not @offer.blank?
      @user= @offer[0].user
      @url_bid = "http://#{request.host_with_port}/api/events/#{params[:event_id]}/bids/#{params[:bid_id]}"
      @url_confirm = "http://#{request.host_with_port}/api/events/#{params[:event_id]}/offers/#{params[:offer_id]}/confirm_deal?bid_id=#{@bid.id}"

      render status: 422, json: {}
      if UserMailer.buy_now(@user, @url_bid, @url_confirm).deliver
        render status: 200, json: {message: "email sent"}
      else
        render status: 422, json: {}
      end

    else
      render status: 422, json: {}
    end
  end

  ## => Sell now

##
      # = Confirm deal
      #
      # Confirm deal between a bid and offer
      #
      # Need a Event ID For Getting Offers 
      # Need a user email and authentication token for get the list of offers. 
      # Send user email and authentication token as url params. 
      #
      # url_params
          #   user_email -- someone@example.com
          #   user_token -- xg282iD2oNTq8hgiyhhF
          #   bid_id  --  Bid id 
      #
      # Get /api/events/:id/offers/:id/confirm_deal
      #
      # = Examples
      #
      #   resp = conn.get("/api/events/:id/offers/:id/confirm_deal")
      # => params[:bid_id]=1
      #
      #   resp.status
      #   => 201
      #
      #   resp.body
      #   => {"sale":{"id":4,"bid_id":1,"offer_id":9,"ticket_status_id":1,"coupon_id":null,"price":636.0,"quantity":7,"created_at":"2014-11-18T07:34:55.447Z","updated_at":"2014-11-18T07:34:55.447Z"}}

  def confirm_deal
    bid = Bid.find(params[:bid_id])
    @offer = Offer.find(params[:offer_id])
    ticket_status = TicketStatus.first
    @sale = Sale.new(:offer => @offer, :bid => bid,:ticket_status => ticket_status,:price => bid.price,:quantity => bid.quantity)
    if @sale.save
      render status: 201
    else
      render status: 422, json: {errors: @sale.errors}
    end
  end


  private

  def get_offers(bid)
    @bid      = bid
    @offers   =[]

    if(@bid.groupable_type == "VenueGroup")
      @offers <<@bid.groupable.offers.where(:event_id => params[:event_id])

    elsif(@bid.groupable_type == "CustomVenueGroup")
      @customgroup  = @bid.groupable
      @customgroup.custom_venue_rows.all.each do |customrow|
        customrow.venue_row.offer_tickets.each do |offer_ticket|
          @offers << offer_ticket.offer
        end
      end
    end

    @offers   = @offers.flatten.uniq

    send_emails(@bid, @offers)

    if not @offers.nil?

      if @user.email_alert_configs.exists?(:option => "on bid match") then UserMailer.on_bid_match(request, @user, @bid, @offers).deliver end
      if @user.sms_alert_configs.exists?(:option => "on bid match") then send_sms(@user.phone, "+15612575425", "Offers found for your bid. Please visit www.google.com") end

    end

    @offers
  end

  def send_emails(bid, offers)

    offers.each do |offer|
      user = offer.user

      if user.email_alert_configs.exists?(:option => "on offer match")
        binding.pry
        UserMailer.on_offers_match(request, bid, offer).deliver

      end

      if user.sms_alert_configs.exists?(:option => "on offer match")
        send_sms(user.phone, "+15612575425", "A bid found for your offer. Please visit www.google.com")
      end

    end

  end

  def send_sms(to, from, body)
#    @client.messages.create( :from => from, :to => to, :body => body)
  end

  def send_notifications(bid)
    @bid      = bid
    @offers   = get_offers bid
    @offers.each do |offer|
      noti    = Notification.create(:sendable => @bid.user, :notifiable => @bid)
      notiRec = noti.recipients.build(:recievable => offer.user)
      notiRec.content = "Bid is Created by this #{@bid.user.first_name} #{@bid.user.last_name} user in VenueGroup #{offer.venue_group.name} where you placed an offer"
      noti.save
    end
  end 

  def update_bid_params
    params.require(:bid).permit(:price, :proxy, :quantity, :expire_at, :cancelothers, :is_negotiation)
  end

  def bid_params
#    params.require(:bid).permit(:user_id, :event_id, :groupable_id, :groupable_type, :price, :quantity, :expire_at, :cancelothers, :is_negotiation)
    params.require(:bid).permit(:event_id, :groupable_id, :groupable_type, :price, :proxy, :quantity, :expire_at, :cancelothers, :is_negotiation).merge(:user_id => @user.id)
  end

  def set_bid
    @bid  = Bid.find params[:id]
  end

  def set_event
    @event = Event.find params[:event_id]
  end

  def set_event_bid
    @bid = @event.bids.find params[:id]
  end

  def set_twilio
    @client = Twilio::REST::Client.new
  end
  def post_to_facebook(bid)
    user = User.find_by_email params[:user_email]
    auth = user.authorizations.where(:provider => "facebook").last
    
    if auth
      token = auth.token
      fb_user = FbGraph2::User.me(token).fetch
      event_url = url_for controller: 'events', action: 'show', id: bid.event_id
      bid_url = url_for controller: 'bids', action: 'show', event_id: bid.event_id, id: bid.id 
      message = "placed a bid on www.slicketticket.com/new for #{event_url}"
      fb_user.feed!(
        :message => message,
        :picture => "http://slicketticket.com/new/images/home.jpg",
        :link => bid_url,
        :name => 'Slicket Ticket',
        :description => 'Slicket Ticket: People powered tickets'
      )
    end
  end

  def post_to_twitter(bid)

    user=bid.user
    auth = user.authorizations.where(:provider => 'twitter').last  
    
    if auth
    
      twitter = Twitter::REST::Client.new do |config|
        config.consumer_key = SlicketTicket::Application.config.twitter_token
        config.consumer_secret = SlicketTicket::Application.config.twitter_secret
        config.access_token = auth.token
        config.access_token_secret = auth.secret
      end
    
      message = "placed a bid on http://slicketticket.com/new "
      twitter.update(message)
    
    end
  end

end