class Api::V1::VenueGroupsController < ApiController
	rescue_from ActiveRecord::RecordNotFound, :with => :record_not_found
	skip_before_filter :authenticate_user_from_token!, :only => [:show, :index]
	##
        # = List of Venue Group
        #
        # Return a List of All Venue Groups Present in the System
        #
        # Need a Venue ID For Getting Venue Group. 
        # Need a user email and authentication token for getting the list of venue rows. 
        # Send user email and authentication token as url params. 
        #
        # Get /api/events/id/venue_groups
        #
        # url_params
        #   user_email -- someone@example.com
        #   user_token -- xg282iD2oNTq8hgiyhhF
        #
        # params:
        # 	name -- Group Name
        #   rating -- Number 
        #
        # = Examples
        #
        #   resp = conn.get("api/events/id/venue_groups")
        #
        #   resp.status
        #   => 200
        #
        #   resp.body
        #   => {"venue_groups": [ { "venue_group": { "id": 1,"name": "5star","rating": 5,"event_id": 1,"created_at": "2014-10-16T11:59:42.000Z","updated_at": "2014-10-16T11:59:42.000Z"}, "venue_rows": [] }, { "venue_group": { "id": 1,"name": "5star","rating": 5,"event_id": 1,"created_at": "2014-10-16T11:59:42.000Z","updated_at": "2014-10-16T11:59:42.000Z"}, "venue_rows": [] } ] }
        #
        #

	def index
		@venue_groups=Event.find(params[:event_id]).venue_groups
		render status: 200
	end

	##
        # = Create a Venue Group
        #
        # Creating a New Venue Group
        #
        # Need a Venue ID For Creating Venue Group. 
        # Need a user email and authentication token for creating the venue group and related it to single Event. 
        # Send user email and authentication token as url params. 
        #
        # Post /api/events/id/venue_groups
        #
        # url_params
        #   user_email -- someone@example.com
        #   user_token -- xg282iD2oNTq8hgiyhhF
        #
        # params:
        # 	name -- Group Name
        #   rating -- Number 
        #
        # = Examples
        #
        #   resp = conn.post("api/events/id/venue_groups")
        #
        #   resp.status
        #   => 200
        #
        #   resp.body
        #   => {"message": "venue_group has been created successfully"}
        #
        #

	def create
		@venue_group=Event.find(params[:event_id]).venue_groups.build(get_params_create)
		if @venue_group.save
			render status: 201, json: {message: "venue_group has been created successfully"}
		else
			render status: 422, json:{error: @venue_group.errors}
		end
	end
	
	##
        # = Show Venue Group
        #
        # Return a List of All Venue Groups Present in the System
        #
        # Need a Venue Group ID. 
        # Need a Venue ID For Getting Venue Group. 
        # Need a user email and authentication token for getting the list of venue groups related to single Event. 
        # Send user email and authentication token as url params. 
        #
        # Get /api/events/id/venue_groups/id
        #
        # url_params
        #   user_email -- someone@example.com
        #   user_token -- xg282iD2oNTq8hgiyhhF
        #
        # params:
        # 	name -- Group Name
        #   rating -- Number 
        #
        # = Examples
        #
        #   resp = conn.get("/api/events/id/venue_groups/id")
        #
        #   resp.status
        #   => 200
        #
        #   resp.body
        #   => { "venue_group": { "id": 1,"name": "5star","rating": 5,"event_id": 1,"created_at": "2014-10-16T11:59:42.000Z","updated_at": "2014-10-16T11:59:42.000Z"}, "venue_rows": [] }, { "venue_group": { "id": 1,"name": "5star","rating": 5,"event_id": 1,"created_at": "2014-10-16T11:59:42.000Z","updated_at": "2014-10-16T11:59:42.000Z"}, "venue_rows": [] }
        #
        #   resp = conn.get("api/events/id/venue_groups/id")
        #
        #   resp.status
        #   => 404
        #
        #   resp.body
        #   => {"message": "No Record Found"}
        #

	def show
		@venue_group = Event.find(params[:event_id]).venue_groups.find(params[:id])
		render status: 200
	end
	
	##
        # = Update Venue Group
        #
        # Updating the Exisiting Venue Group
        #
        # Need a Venue Group ID. 
        # Need a Venue ID For Updating Venue Group. 
        # Need a user email and authentication token for updating venue group related to single Event. 
        # Send user email and authentication token as url params. 
        #
        # Get /api/events/id/venue_groups/id
        #
        # url_params
        #   user_email -- someone@example.com
        #   user_token -- xg282iD2oNTq8hgiyhhF
        #
        # params:
        # 	name -- Group Name
        #   rating -- Number 
        #
        # = Examples
        #
        #   resp = conn.get("api/events/id/venue_groups/id")
        #
        #   resp.status
        #   => 204
        #
        #   resp.body
        #   => {}
        #
        #   resp = conn.get("api/events/id/venue_groups/id")
        #
        #   resp.status
        #   => 404
        #
        #   resp.body
        #   => {"message": "No Record Found"}
        #

	def update
		@venue_group=Event.find(params[:event_id]).venue_groups.find(params[:id])
		if @venue_group.update(get_params_update)
			render status: 204, json: {}
		else
			render status: 422, json:{errors: @venue_group.errors}
		end
  end
	
	##
        # = Delete Venue Group
        #
        # Delete a exisiting Venue Group
        #
        # Need a Venue Group ID. 
        # Need a Venue ID For Deleting Venue Group. 
        # Need a user email and authentication token for deleting the venue group related to single Event. 
        # Send user email and authentication token as url params. 
        #
        # Delete /api/events/id/venue_groups/id
        #
        # url_params
        #   user_email -- someone@example.com
        #   user_token -- xg282iD2oNTq8hgiyhhF
        #
        # = Examples
        #
        #   resp = conn.delete("/api/events/id/venue_groups/id")
        #
        #   resp.status
        #   => 204
        #
        #   resp.body
        #   => {}
        #
        #   resp = conn.delete("/api/events/id/venue_groups/id")
        #
        #   resp.status
        #   => 404
        #
        #   resp.body
        #   => {"message": "No Record Found"}
        #

	
	def destroy
		@venue_group=Event.find(params[:event_id]).venue_groups.find(params[:id])
		if @venue_group.destroy()
			render status: 204, json: {}
		else
			render status: 422, json:{errors: @venue_group.errors}
    end
  end 

  ## => List tickets by star

  ##
        # = List of Tickets of a Venue Group
        #
        # Return a List of All Tickets That Belong to the Given Venue Group Present in the System
        #
        # Need a Event ID For Getting Event.
        # Need a Venue Group ID For Getting Venue Group. 
        # Need a user email and authentication token for getting the list of venue rows. 
        # Send user email and authentication token as url params. 
        #
        # Get /api/events/id/venue_groups
        #
        # url_params
        #   user_email -- someone@example.com
        #   user_token -- xg282iD2oNTq8hgiyhhF
        #
        #
        # params:
        #   name -- Group Name
        #   rating -- Number 
        #
        # = Examples
        #
        #   resp = conn.get("api/events/id/venue_groups/id/list_tickets")
        #
        #   resp.status
        #   => 200
        #
        #   resp.body
        #   => {"offer_tickets":[{"offer_ticket":{"id":19,"offer_id":2,"venue_row_id":8,"sale_id":null,"status":null,"seat_number":81,"barcode":null,"attendee_name":null,"created_at":"2014-11-05T08:20:27.000Z","updated_at":"2014-11-05T08:20:27.000Z"},"uploaded_ticket":null}]}
        #
        #
  
  def list_tickets
    temp = []
    i=0
    Event.find(params[:event_id]).venue_groups.find(params[:id]).venue_rows.each do |row|
      temp[i]=row.offer_tickets
      i=i+1
    end
    @tickets=temp.flatten
    render status: 200

  end

	private
	def get_params_create
		params.permit( :id, :name, :rating, :event_id)
	end
  def get_params_update
    params.permit( :id, :name, :rating, :event_id)
  end
end
