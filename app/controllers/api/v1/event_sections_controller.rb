class Api::V1::EventSectionsController < ApiController
  rescue_from ActiveRecord::RecordNotFound, :with => :record_not_found

  before_filter :set_event, :only => [:show,:list_tickets]
  before_filter :set_event_section, :only => [:show,:list_tickets]
  skip_before_filter :authenticate_user_from_token!, :only => [:show, :index]

  ##
      # = Get maximum ticket price and minimum ticket price in a given event section
      #
      # Return min max for min ticket price and max ticket price
      #
      # Need a event id ,event section id 
      #
      # Get /api/events/:event_id/event_sections/:id
      #
      # = Examples
      #
      #   resp = conn.get("/api/events/1/event_sections/2")
      #
      #   resp.status
      #   => 200
      #
      #   resp.body
      #   => {"min":633,"max":483}
      #
      #   resp = conn.get("/api/events/1/event_sections/245")
      #
      #   resp.status
      #   => 422
      #
      #   resp.body
      #   => {"errors":"invalid event section id"}

  def show

    price_array = []

    @event_section.venue_rows.map do |venue_row|
        venue_row.venue_group.offers.map do |offer|
            price_array << offer.price.to_i
        end
    end

    if not price_array.blank?
        render status: 200, json:{min: price_array[0], max:price_array[price_array.length-1]}
    else
        render status: 422, json:{min: "-", max:"-"}
    end

  end

  ##
        # = List of Tickets of a Event Section
        #
        # Return a List of All Tickets That Belong to the Given Event Section Present in the System
        #
        # Need a Event ID For Getting Event Section. 
        # Need a user email and authentication token for getting the list of venue rows. 
        # Send user email and authentication token as url params. 
        #
        # Get /api/events/id/event_sections
        #
        # url_params
        #   user_email -- someone@example.com
        #   user_token -- xg282iD2oNTq8hgiyhhF
        #
        #
        # params:
        #   name -- Group Name
        #   rating -- Number 
        #
        # = Examples
        #
        #   resp = conn.get("api/events/id/event_sections/id/list_tickets")
        #
        #   resp.status
        #   => 200
        #
        #   resp.body
        #   => {"offer_tickets":[{"offer_ticket":{"id":19,"offer_id":2,"venue_row_id":8,"sale_id":null,"status":null,"seat_number":81,"barcode":null,"attendee_name":null,"created_at":"2014-11-05T08:20:27.000Z","updated_at":"2014-11-05T08:20:27.000Z"},"uploaded_ticket":null}]}
        #
        #

  def list_tickets
    temp = []
    i=0
    @event_section.venue_rows.each do |row|
      temp[i]=row.offer_tickets
      i=i+1
    end
    @tickets=temp.flatten
    render status: 200

  end

  private

    def set_event_section

      if not @event.event_sections.exists?(:id => params[:id])
        render status: 422, json:{errors: "invalid event section id"}
        return
      end

      @event_section = @event.event_sections.find(params[:id])
    end

    def set_event
      if not Event.exists?(:id => params[:event_id])
        render status: 422, json:{errors: "invalid event id"}
        return
      end

      @event = Event.find params[:event_id]
    end

end