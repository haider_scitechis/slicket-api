class Api::V1::VenuesController < ApiController
    rescue_from ActiveRecord::RecordNotFound, :with => :record_not_found
    skip_before_filter :authenticate_user_from_token!, :only => [:show, :index]
  ##
    # = List of all Venues
    #
    # Return a List of All Venues
    #
    # Need a user email and authentication token for getting the list of venues. 
    # Send user email and authentication token as url params. 
    #
    # Get /api/venues
    #
    # url_params
    #   user_email -- someone@example.com
    #   user_token -- xg282iD2oNTq8hgiyhhF
    #
    # = Examples
    #
    #   resp = conn.get("/api/venues")
    #
    #   resp.status
    #   => 200
    #
    #   resp.body
    #   => {"venues": [ { "venue": {"id": 1, "category": "A", "subcategory": "A1", "venue": "Lahore",  "street_address": "Gulberg 3", "team": "Pakistan", "city": "Lahore", "state": "Punjab", "zip": "54000",  "phone": null, "description": "Qadaffi Stadium", "school": "FAST", "team_name": null, "mascot": null, "seatgeek_id": null, "created_at": "2014-10-16T11:35:06.000Z", "updated_at": "2014-10-16T11:35:06.000Z" }, "venue_sections": [], "venue_groups": []  } ] }
    #
    #   resp = conn.get("/api/venues")
    #
    #   resp.status
    #   => 401
    #
    #   resp.body
    #   => {"message": "invalid", "error": {"category": [ "can't be blank" ]}}
    #

  def index

    @venues = Venue.all
    render  status: 200
  end

  ##
    # = Create Venue
    #
    # Creating a New Venue
    #
    # Need a user email and authentication token for creating the new venue. 
    # Send user email and authentication token as url params. 
    #
    # POST /api/venues
    #
    # url_params
    #   user_email -- someone@example.com
    #   user_token -- xg282iD2oNTq8hgiyhhF
    #
    # params:
    #   category -- Category Name
    #   subcategory -- Sub Category Name
    #   venue -- Venue Name
    #   street_address -- Address Details
    #   team -- Country Name for team
    #   school -- School Name
    #   city -- City Name
    #   zip --  Zip Code
    #   state -- Region/State/District
    #   description -- Details About Venue
    #
    # = Examples
    #
    #   resp = conn.post("/api/venues")
    #
    #   resp.status
    #   => 201
    #
    #   resp.body
    #   => {"message": "Venue has been created successfully"}
    #
    #   resp = conn.post("/api/venues")
    #
    #   resp.status
    #   => 401
    #
    #   resp.body
    #   => {"message": "invalid", "error": {"category": [ "can't be blank" ]}}
    #

  def create
    @venue = Venue.new(get_params_create)
    if @venue.save
        render status: 201, json: {message: "Venue has been created successfully"}
    else
        render status: 422, json:{errors: @venue.errors}
    end
  end

  ##
    # = Show Venue
    #
    # Return a Single Venues Present in the System
    #
    # Need a Venue ID. 
    # Need a user email and authentication token for getting the single venue. 
    # Send user email and authentication token as url params. 
    #
    # Get /api/venues/id
    #
    # url_params
    #   user_email -- someone@example.com
    #   user_token -- xg282iD2oNTq8hgiyhhF
    #
    # = Examples
    #
    #   resp = conn.get("/api/venues/id")
    #
    #   resp.status
    #   => 200
    #
    #   resp.body
    #   => { "venue": {"id": 1, "category": "A", "subcategory": "A1", "venue": "Lahore",  "street_address": "Gulberg 3", "team": "Pakistan", "city": "Lahore", "state": "Punjab", "zip": "54000",  "phone": null, "description": "Qadaffi Stadium", "school": "FAST", "team_name": null, "mascot": null, "seatgeek_id": null, "created_at": "2014-10-16T11:35:06.000Z", "updated_at": "2014-10-16T11:35:06.000Z" , "venue_sections": [], "venue_groups": [] }}
    #
    #   resp = conn.get("/api/venues/id")
    #
    #   resp.status
    #   => 404
    #
    #   resp.body
    #   => { "venue": {"id": null, "category": "null", "subcategory": "null", "venue": "null",  "street_address": "null", "team": "null", "city": "null", "state": "null", "zip": "null",  "phone": null, "description": "null", "school": "null", "team_name": null, "mascot": null, "seatgeek_id": null, "created_at": "null", "updated_at": "null" , "venue_sections": [], "venue_groups": [] }}
    #

  def show
    @venue = Venue.find(params[:id])
    render status: 200
  end

  ##
    # = Update Venue
    #
    # Update a Venues Present in the System
    #
    # Need a Venue ID. 
    # Need a user email and authentication token for updating the existing venue. 
    # Send user email and authentication token as url params. 
    #
    # Patch /api/venues/id
    #
    # url_params
    #   user_email -- someone@example.com
    #   user_token -- xg282iD2oNTq8hgiyhhF
    #
    # = Examples
    #
    #   resp = conn.patch("/api/venues/id")
    #
    #   resp.status
    #   => 204
    #
    #   resp.body
    #   => {}
    #
    #   resp = conn.patch("/api/venues/id")
    #
    #   resp.status
    #   => 404
    #
    #   resp.body
    #   => {"message": "Record Not Found"}
    #


  def update
    @venue = Venue.find(params[:id])
    if @venue.update(get_params_update)
        render status: 204, json: {}
    else
        render status: 422, json:{errors: @venue.errors}
    end
  end

  ##
    # = Delete Venue
    #
    # Delete a Venues Present in the System
    #
    # Need a Venue ID. 
    # Need a user email and authentication token for deleting the existing venue. 
    # Send user email and authentication token as url params. 
    #
    # Delete /api/venues/id
    #
    # url_params
    #   user_email -- someone@example.com
    #   user_token -- xg282iD2oNTq8hgiyhhF
    #
    # = Examples
    #
    #   resp = conn.delete("/api/venues/id")
    #
    #   resp.status
    #   => 204
    #
    #   resp.body
    #   => {}
    #
    #   resp = conn.delete("/api/venues/id")
    #
    #   resp.status
    #   => 404
    #
    #   resp.body
    #   => {"message": "Record Not Found"}
    #

  def destroy
    @venue = Venue.find(params[:id])
    if @venue.destroy()
        render status: 204, json: {}
    else
        render status: 422, json:{errors: @venue.errors}
    end
  end
	rescue_from ActiveRecord::RecordNotFound, :with => :record_not_found

	##
        # = List of all Venues
        #
        # Return a List of All Venues
        #
        # Need a user email and authentication token for getting the list of venues. 
        # Send user email and authentication token as url params. 
        #
        # Get /api/venues
        #
        # url_params
        #   user_email -- someone@example.com
        #   user_token -- xg282iD2oNTq8hgiyhhF
        #
        # = Examples
        #
        #   resp = conn.get("/api/venues")
        #
        #   resp.status
        #   => 200
        #
        #   resp.body
        #   => {"venues": [ { "venue": {"id": 1, "category": "A", "subcategory": "A1", "venue": "Lahore",  "street_address": "Gulberg 3", "team": "Pakistan", "city": "Lahore", "state": "Punjab", "zip": "54000",  "phone": null, "description": "Qadaffi Stadium", "school": "FAST", "team_name": null, "mascot": null, "seatgeek_id": null, "created_at": "2014-10-16T11:35:06.000Z", "updated_at": "2014-10-16T11:35:06.000Z" }, "venue_sections": [], "venue_groups": []  } ] }
        #
        #   resp = conn.get("/api/venues")
        #
        #   resp.status
        #   => 401
        #
        #   resp.body
        #   => {"message": "invalid", "error": {"category": [ "can't be blank" ]}}
        #

	def index

		@venues = Venue.all
		render  status: 200
	end

	##
        # = Create Venue
        #
        # Creating a New Venue
        #
        # Need a user email and authentication token for creating the new venue. 
        # Send user email and authentication token as url params. 
        #
        # POST /api/venues
        #
        # url_params
        #   user_email -- someone@example.com
        #   user_token -- xg282iD2oNTq8hgiyhhF
        #
        # params:
        #   category -- Category Name
        #   subcategory -- Sub Category Name
        #   venue -- Venue Name
        #   street_address -- Address Details
        #   team -- Country Name for team
        #   school -- School Name
      	#   city -- City Name
      	#   zip --  Zip Code
      	#   state -- Region/State/District
      	#   description -- Details About Venue
        #
        # = Examples
        #
        #   resp = conn.post("/api/venues")
        #
        #   resp.status
        #   => 201
        #
        #   resp.body
        #   => {"message": "Venue has been created successfully"}
        #
        #   resp = conn.post("/api/venues")
        #
        #   resp.status
        #   => 401
        #
        #   resp.body
        #   => {"message": "invalid", "error": {"category": [ "can't be blank" ]}}
        #

	def create
		@venue = Venue.new(get_params_create)
        if @venue.save
			render status: 201, json: {message: "Venue has been created successfully"}
		else
			render status: 422, json:{errors: @venue.errors}
		end
	end

	##
        # = Show Venue
        #
        # Return a Single Venues Present in the System
        #
        # Need a Venue ID. 
        # Need a user email and authentication token for getting the single venue. 
        # Send user email and authentication token as url params. 
        #
        # Get /api/venues/id
        #
        # url_params
        #   user_email -- someone@example.com
        #   user_token -- xg282iD2oNTq8hgiyhhF
        #
        # = Examples
        #
        #   resp = conn.get("/api/venues/id")
        #
        #   resp.status
        #   => 200
        #
        #   resp.body
        #   => { "venue": {"id": 1, "category": "A", "subcategory": "A1", "venue": "Lahore",  "street_address": "Gulberg 3", "team": "Pakistan", "city": "Lahore", "state": "Punjab", "zip": "54000",  "phone": null, "description": "Qadaffi Stadium", "school": "FAST", "team_name": null, "mascot": null, "seatgeek_id": null, "created_at": "2014-10-16T11:35:06.000Z", "updated_at": "2014-10-16T11:35:06.000Z" , "venue_sections": [], "venue_groups": [] }}
        #
        #   resp = conn.get("/api/venues/id")
        #
        #   resp.status
        #   => 404
        #
        #   resp.body
        #   => { "venue": {"id": null, "category": "null", "subcategory": "null", "venue": "null",  "street_address": "null", "team": "null", "city": "null", "state": "null", "zip": "null",  "phone": null, "description": "null", "school": "null", "team_name": null, "mascot": null, "seatgeek_id": null, "created_at": "null", "updated_at": "null" , "venue_sections": [], "venue_groups": [] }}
        #

	def show
		@venue = Venue.find(params[:id])
		render status: 200
	end

    ##
        # = Update Venue
        #
        # Update a Venues Present in the System
        #
        # Need a Venue ID. 
        # Need a user email and authentication token for updating the existing venue. 
        # Send user email and authentication token as url params. 
        #
        # Patch /api/venues/id
        #
        # url_params
        #   user_email -- someone@example.com
        #   user_token -- xg282iD2oNTq8hgiyhhF
        #
        # = Examples
        #
        #   resp = conn.patch("/api/venues/id")
        #
        #   resp.status
        #   => 204
        #
        #   resp.body
        #   => {}
        #
        #   resp = conn.patch("/api/venues/id")
        #
        #   resp.status
        #   => 404
        #
        #   resp.body
        #   => {"message": "Record Not Found"}
        #


	def update
		@venue = Venue.find(params[:id])
		if @venue.update(get_params_update)
			render status: 204, json: {}
		else
			render status: 422, json:{errors: @venue.errors}
		end
	end

    ##
        # = Delete Venue
        #
        # Delete a Venues Present in the System
        #
        # Need a Venue ID. 
        # Need a user email and authentication token for deleting the existing venue. 
        # Send user email and authentication token as url params. 
        #
        # Delete /api/venues/id
        #
        # url_params
        #   user_email -- someone@example.com
        #   user_token -- xg282iD2oNTq8hgiyhhF
        #
        # = Examples
        #
        #   resp = conn.delete("/api/venues/id")
        #
        #   resp.status
        #   => 204
        #
        #   resp.body
        #   => {}
        #
        #   resp = conn.delete("/api/venues/id")
        #
        #   resp.status
        #   => 404
        #
        #   resp.body
        #   => {"message": "Record Not Found"}
        #

	def destroy
		@venue = Venue.find(params[:id])
		if @venue.destroy()
			render status: 204, json: {}
		else
			render status: 422, json:{errors: @venue.errors}
		end
	end

  ## => Venue search


  def search
    @venues= Venue.where(" venue LIKE '%#{params[:query]}%' OR street_address LIKE '%#{params[:query]}%' OR team LIKE '%#{params[:query]}%' OR city LIKE '%#{params[:query]}%' OR state LIKE '%#{params[:query]}%' OR zip LIKE '%#{params[:query]}%' OR school LIKE '%#{params[:query]}%' OR team_name LIKE '%#{params[:query]}%' ")
    render status: 200
  end

    private

    def get_params_create
        params.permit(:venue,
                        :street_address, :team, :city,
                        :state, :zip, :phone, :description,
                        :school, :team_name, :mascot,
                        :seatgeek_id)
    end
  def get_params_update
    params.permit(:venue,
            :street_address, :team, :city,
            :state, :zip, :phone, :description,
            :school, :team_name, :mascot,
            :seatgeek_id)
  end
end
