class ApiController < ApplicationController

  before_filter :authenticate_user_from_token!, :execpt => [:show]
  #before_filter :authenticate_user!
  #before_filter :set_user

  # skip_before_filter :verify_authenticity_token
  respond_to :json

  private

  def record_not_found
    logger.info("venue_group doesn't exist")
    render status:404, json: {message: "No Record Found"}
    return
  end

  def authenticate_user_from_token!

    user_email = params[:user_email].presence
    user       = user_email && User.find_by_email(user_email)
    # Notice how we use Devise.secure_compare to compare the token
    # in the database with the token given in the params, mitigating
    # timing attacks.
    if user && Devise.secure_compare(user.authentication_token, params[:user_token])
      #sign_in user, store: false
      @user  = User.find_by_email(params[:user_email])
    else
      render json: {:errors => ["Invalid login credentials"], debug_error_from: "DEBUG_INFO: authenticate_user_from_token!"}, :status => 401
    end

  end

  def check_if_admin
    render status: 422, json: {error: 'Only admins allowed!'} unless @user.user_type == "admin"
  end

end
