json.bids @bids.each do |bid|

	json.bid bid
	json.event bid.event
	json.user bid.user
	json.groupable bid.groupable

end