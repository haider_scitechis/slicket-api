json.bids @bids do |bid|

    json.bid bid
    json.event bid.event
    json.user bid.user
    if bid.groupable_type == "VenueGroup"
    	json.venue_group bid.groupable
    else
    	json.dynamic_group bid.groupable
    end

end