json.offers @offers do |offer|
	json.offer offer
	json.venue_group offer.venue_group
	json.ticket_type offer.ticket_type
	json.offer_tickets offer.offer_tickets do |offer_ticket|
		json.offer_ticket offer_ticket
		json.uploaded_ticket offer_ticket.uploaded_ticket
	end
	json.disclosure offer.disclosures
end