json.offer do
	json.id @offer.try(:id)
	json.venue_group_id @offer.try(:venue_group_id)
	json.event_id @offer.try(:event_id)
	json.user_id @offer.try(:user_id)
	json.ticket_type_id @offer.try(:ticket_type_id)
	json.price @offer.try(:price)
	json.proxy @offer.try(:proxy)
	json.multiple @offer.try(:multiple)
	json.cancelothers @offer.try(:cancelothers)
	json.status @offer.try(:status)
	json.in_hand_date @offer.try(:in_hand_date)
	json.expected_ship_date @offer.try(:expected_ship_date)
	json.is_negotiation @offer.try(:is_negotiation)
	json.expires_at @offer.try(:expires_at)
	json.confirm_transtaction @offer.try(:confirm_transtaction)
	json.piggybacked @offer.try(:piggybacked)
	json.notes @offer.try(:notes)
	json.venue_group @offer.try(:venue_group)
	json.ticket_type @offer.try(:ticket_type)
	json.offer_tickets @offer.offer_tickets do |offer_ticket|
		json.offer_ticket offer_ticket
		json.uploaded_ticket offer_ticket.uploaded_ticket
	end
	json.disclosure @offer.disclosures
end
