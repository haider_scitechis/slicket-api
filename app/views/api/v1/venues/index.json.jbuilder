json.venues @venues do |venue|
  json.venue venue
  json.venue_sections venue.venue_sections do |section|
    json.venue_section section
  end
end