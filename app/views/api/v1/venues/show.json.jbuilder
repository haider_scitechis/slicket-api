json.venue do
	json.id @venue.try(:id)
	json.venue @venue.try(:venue)
	json.street_address @venue.try(:street_address)
	json.team @venue.try(:team)
	json.city @venue.try(:city)
	json.state @venue.try(:state)
	json.zip @venue.try(:zip)
	json.phone @venue.try(:phone)
	json.description @venue.try(:description)
	json.school @venue.try(:school)
	json.team_name @venue.try(:team_name)
	json.mascot @venue.try(:mascot)
	json.seatgeek_id @venue.try(:seatgeek_id)
	json.venue_sections @venue.try(:venue_sections) do |section|
		json.venue_section section
	end
end