json.custom_venue_groups @custom_venue_groups do |custom_venue_group|

  json.custom_venue_group custom_venue_group
  json.user custom_venue_group.user
  json.event custom_venue_group.event

end