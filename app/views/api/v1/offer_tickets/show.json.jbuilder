json.offer_ticket do
json.id @offer_ticket.id
  json.offer_id @offer_ticket.try(:offer_id)
  json.venue_row_id @offer_ticket.try(:venue_row_id)
  json.sale_id @offer_ticket.try(:sale_id)
  json.status @offer_ticket.try(:status)
  json.seat_number @offer_ticket.try(:seat_number)
  json.barcode @offer_ticket.try(:barcode)
  json.attendee_name @offer_ticket.try(:attendee_name)
  if @offer_ticket.uploaded_ticket
		json.uploaded_ticket do
	    json.filename @offer_ticket.uploaded_ticket.ticket_file_name
	    json.content_type @offer_ticket.uploaded_ticket.ticket_content_type
	    json.file_size number_to_human_size(@offer_ticket.uploaded_ticket.ticket.size)
	    json.path @offer_ticket.uploaded_ticket.ticket.path
		end
  end
end