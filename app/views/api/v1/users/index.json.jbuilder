json.users @users do |user|
	json.name user.username
	json.email user.email 
	json.title user.title
	json.name_first user.first_name
	json.name_mi user.middle_name
	json.name_last user.last_name
	json.phone user.phone
	json.phone_daytime user.phone_daytime
	json.phone_evening user.phone_evening
	json.phone_cell user.phone_cell
	json.subscribe user.subscribe
	json.sale_commission user.sale_commission
	json.negotiation_commission user.negotiation_commission

	json.security_questions user.security_answers do |answer|
		json.question answer.security_question
		json.answer answer.answer
	end
end