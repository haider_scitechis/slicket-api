json.venue_groups @venue_groups do |venue_group|
	json.venue_group venue_group
	json.venue_rows venue_group.venue_rows do |venue_row|
		json.venue_row venue_row
	end
end