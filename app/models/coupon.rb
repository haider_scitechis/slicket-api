class Coupon < ActiveRecord::Base
  has_one :sale
  validates :coupon_number, presence: :true
end
