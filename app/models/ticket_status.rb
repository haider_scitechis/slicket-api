class TicketStatus < ActiveRecord::Base
  enum name: { not_delivered: 0, delivered: 1 }
end
