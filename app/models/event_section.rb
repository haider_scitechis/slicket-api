class EventSection < ActiveRecord::Base

  belongs_to :event
  belongs_to :venue_section


  has_many :venue_rows
  has_many :venue_groups, through: :venue_rows
end