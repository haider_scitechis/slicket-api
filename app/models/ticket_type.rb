class TicketType < ActiveRecord::Base
  has_many :offers

  validates :name, length: {maximum: 45}
end