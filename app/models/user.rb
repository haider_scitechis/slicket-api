class User < ActiveRecord::Base
  ALLOW_SIGN_UP = false

  attr_accessor :unhashed_password
  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable

  has_many :sentNotifications, as: :sendable, class_name: 'Notification', :dependent => :destroy
  has_many :recievedNotifications, class_name: 'NotificationRecipient', source_type: 'User', as: :recievable, :dependent => :destroy
  has_many :bids

  devise :database_authenticatable, :registerable, :recoverable, :rememberable, :trackable,
   :validatable, :token_authenticatable, :invitable, :invite_for => 2.weeks

  has_many :security_answers
  has_many :security_questions, through: :security_answers
  has_many :addresses
  has_many :authorizations, :dependent => :destroy
  has_many :custom_venue_groups
  has_many :commission_shipping_configs

  has_many :email_alert_config_options
  has_many :email_alert_configs, :through => :email_alert_config_options

  has_many :sms_alert_config_options
  has_many :sms_alert_configs, :through => :sms_alert_config_options


  SOCIALS = {
    facebook: 'Facebook',
    google_oauth2: 'Google',
    linkedin: 'Linkedin'    
  }

  validates :email, presence: true, length: { maximum: 256 }
  validates :username, presence: true, length: { maximum: 128 }
  validates :title, presence: true, length: { maximum: 32 }
  validates :first_name, presence: true, length: { maximum: 128 }
  validates :middle_name,length: { maximum: 32 }
  validates :last_name, presence: true, length: { maximum: 128 }
  validates :phone, length: { maximum: 45 }
  validates :phone_daytime, length: { maximum: 64 }
  validates :phone_evening, length: { maximum: 64 }
  validates :phone_cell, length: { maximum: 64 }

  accepts_nested_attributes_for :security_questions, :security_answers, :addresses


  def self.after_sign_in_info user
    {
      first_name: user.first_name,
      last_name: user.last_name,
      email: user.email,
      auth_token: user.authentication_token,
    }

  end

  def self.from_omniauth(auth, current_user)
    authorization = Authorization.where(:provider => auth.provider, :uid => auth.uid.to_s, 
                                        :token => auth.credentials.token, 
                                        :secret => auth.credentials.secret).first_or_initialize


    authorization.profile_page = auth.info.urls.first.last unless authorization.persisted?
    if authorization.user.blank?

      if auth.provider == "twitter" then email = "#{auth.info.nickname}@example.com" else email = auth.info.email end
      user = current_user.nil? ? User.where('email = ?', email).first : current_user
      if user.blank?

        user          = User.new
        password      = Devise.friendly_token[0, 20]
        user.password = password
        user.password_confirmation  = password
#        user.skip_confirmation!

        user.fetch_details(auth)
        user.save
      end
      authorization.user = user
      authorization.save
    end
    authorization.user
  end

  def fetch_details(auth)


    if auth.provider == "twitter"
      self.email      = "#{auth.info.nickname}@example.com"

      if  auth.info.name.split(" ").count == 2

        self.first_name = auth.info.name.split(" ")[0]
        self.last_name  = auth.info.name.split(" ")[1]

      else
        self.first_name = auth.info.name
        self.last_name  = auth.info.name


      end

    else
      self.email      = auth.info.email
      self.first_name = auth.info.first_name
      self.last_name  = auth.info.last_name
    end
    self.username       = self.email.split("@")[0]
    self.title      = "-"

  end

  def just_for_mail
    UserMailer.email_name.deliver
  end

  # def as_json(options)
  #   super(only: [:id, :email, :phone_number, :first_name, :last_name, :authentication_token])
  # end
end