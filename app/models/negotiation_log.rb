class NegotiationLog < ActiveRecord::Base

  belongs_to :negotiable, polymorphic: true

  belongs_to :bid
  belongs_to :offer

  validates :price, presence: true
  validates :quantity, presence: true

end