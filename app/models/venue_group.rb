class VenueGroup < ActiveRecord::Base
	belongs_to :event

	has_many :venue_rows
	has_many :event_section, through: :venue_rows
	has_many :offers
	has_many :bids, as: :groupable
	
	validates :name, length: {maximum: 45}
end
