class UploadedTicket < ActiveRecord::Base
  belongs_to :offer_ticket
  has_attached_file :ticket
  validates_attachment :ticket, content_type: { content_type: "application/pdf" }
end
