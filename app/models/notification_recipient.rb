class NotificationRecipient < ActiveRecord::Base

	belongs_to :recievable, polymorphic: true
	belongs_to :notification

end
