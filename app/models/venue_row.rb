class VenueRow < ActiveRecord::Base

  belongs_to :event_section
  belongs_to :venue_group


  has_many :offer_tickets
  validates :name, length: {maximum: 45}
  validates :row_number, :presence => true
  has_many :custom_venue_rows
end