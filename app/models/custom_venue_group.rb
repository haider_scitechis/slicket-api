class CustomVenueGroup < ActiveRecord::Base
	has_many :custom_venue_rows
	has_many :bids, as: :groupable

	belongs_to 	:event
	belongs_to 	:user

end