
class Offer < ActiveRecord::Base


  belongs_to :venue_group
  belongs_to :event
  belongs_to :user
  belongs_to :ticket_type
  has_many :offer_tickets
  has_many :negotiation_logs

  has_many :disclosures, as: :disclosureable, dependent: :destroy
  accepts_nested_attributes_for :disclosures

  has_many :offerNotifications, as: :notifiable, class_name: 'Notification', :dependent => :destroy


  has_attached_file :tickets
  validates_attachment :tickets, content_type: { content_type: "application/pdf" }
  has_one :sale

  validates :venue_group_id, presence: :true
  validates :event_id, presence: :true
  validates :user_id, presence: :true
  validates :ticket_type_id, presence: :true
  validates :price, presence: :true
#  validates :multiple, presence: :true
  validates :cancelothers, presence: :true
  validates :status, length: {maximum: 45}

end