class CustomVenueRow < ActiveRecord::Base
	belongs_to :custom_venue_group
	belongs_to :venue_row
end