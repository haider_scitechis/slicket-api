class OfferTicket < ActiveRecord::Base
  belongs_to :offer
  belongs_to :venue_row
  belongs_to :sale
  has_one :uploaded_ticket

  validates :offer_id, presence: :true
  validates :status, length: {maximum: 45}
  validates :venue_row_id, presence: :true
end
