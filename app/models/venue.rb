class Venue < ActiveRecord::Base

	has_many :events
	has_many :venue_sections, :dependent => :destroy

	validates :venue, presence: true,
				length: { maximum: 256 }
	validates :street_address, presence: true,
				length: { maximum: 256 }
	validates :team, presence: true,
				length: { maximum: 256 }
	validates :city,
				length: { maximum: 80 }
	validates :state,
				length: { maximum: 40 }
	validates :zip,
				length: { maximum: 15 }
	validates :phone,
				length: { maximum: 20 }
end