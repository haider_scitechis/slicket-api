class Disclosure < ActiveRecord::Base
  belongs_to :disclosureable, polymorphic: true

  validates :name, :presence => true
  validates :desc, :presence => true  
  
end