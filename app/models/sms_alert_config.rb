class SmsAlertConfig < AlertConfig

  has_many :sms_alert_config_options
  has_many :users, :through => :sms_alert_config_options

end