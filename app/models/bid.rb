class Bid < ActiveRecord::Base

  belongs_to :user
  belongs_to :event

  has_many :negotiation_logs
  belongs_to :groupable, polymorphic: true
  has_one :sale

  has_many :bidNotifications, as: :notifiable, class_name: 'Notification', :dependent => :destroy

  validates :price, presence: true
  validates :quantity, presence: true
  validates :expire_at, presence: true
  validates :cancelothers, presence: true
  validates :is_negotiation, presence: true
  validates :user, presence: true
  validates :event, presence: true

end