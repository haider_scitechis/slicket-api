class Notification < ActiveRecord::Base

	belongs_to :notifiable, polymorphic: true
	belongs_to :sendable, polymorphic: true

	has_many :recipients, class_name: 'NotificationRecipient', dependent: :restrict_with_error


end
