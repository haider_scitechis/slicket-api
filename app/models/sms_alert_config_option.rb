class SmsAlertConfigOption < ActiveRecord::Base
  belongs_to :user
  belongs_to :sms_alert_config

  validates :user_id, :presence => true
  validates :sms_alert_config_id, :presence => true

end