class OfferDisclosure < ActiveRecord::Base
  belongs_to :offer
  belongs_to :disclosure
end
