class Address < ActiveRecord::Base

	belongs_to :user
	validates :name, length: { maximum: 45 }
    validates :address1, length: { maximum: 100 }
    validates :address2, length: { maximum: 100 }
    validates :city, length: { maximum: 80 }
    validates :state, length: { maximum: 45 }
    validates :zip, length: { maximum: 45 }
    validates :phone, length: { maximum: 45 }
                    
end