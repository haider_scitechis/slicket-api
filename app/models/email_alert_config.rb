class EmailAlertConfig < AlertConfig

  has_many :email_alert_config_options
  has_many :users, :through => :email_alert_config_options

end