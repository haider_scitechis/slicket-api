class Event < ActiveRecord::Base

  RADIUS = 10

  has_many 	:offers
  has_many 	:venue_groups
  has_many  :bids

  has_many  :event_sections
  has_many  :venue_sections, through: :event_sections

  belongs_to :venue
  belongs_to :event_category

  validates :name, presence: true
  validates :date, presence: true


  def self.search(search_string, type, latitude, longitude)
    sql = "SELECT * FROM events WHERE name LIKE '%#{search_string}%' OR event_type='#{type}' OR (lat-(#{latitude}))*(lat-(#{latitude})) + (lang-(#{longitude}))*(lang-(#{longitude})) < 10"
    Event.find_by_sql(sql)
  end
end
