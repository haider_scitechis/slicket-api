class EmailAlertConfigOption < ActiveRecord::Base

  belongs_to :user
  belongs_to :email_alert_config

  validates :user_id, :presence => true
  validates :email_alert_config_id, :presence => true

end