class VenueSection < ActiveRecord::Base

  belongs_to :venue

  has_many :event_sections
  has_many :events, through: :event_sections
  has_many :disclosures, as: :disclosureable, dependent: :destroy

  accepts_nested_attributes_for :disclosures


  validates :raphael_data,
              length: { maximum: 45 }
  validates :color,
              length: { maximum: 45 }
  validates :section_text,
              length: { maximum: 45 }
  validates :section_number,
              length: { maximum: 45 }
  validates :row_start,
              length: { maximum: 45 }
  validates :row_end,
              length: { maximum: 45 }
  validates :photo,
              length: { maximum: 45 }
end
