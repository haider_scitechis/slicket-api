class Sale < ActiveRecord::Base
  belongs_to :bid
  belongs_to :offer
  belongs_to :ticket_status
  belongs_to :coupon

  validates :bid, presence: true
  validates :offer, presence: true
  validates :quantity, presence: true
  validates :price, presence: true

end
