class Payment < ActiveRecord::Base

  PROCESSING, FAILED, SUCCESS = 1, 2, 3

  validates :amount, :presence => true, :numericality => { :greater_than => 0 }

  def self.conf
    @@gateway_conf ||= YAML.load_file(Rails.root.join('config/gateway.yml').to_s)[Rails.env]
  end

  def success?
    self.status == SUCCESS
  end

  ## Authorize :: SIM
  def setup_transaction(options ={})
    options.merge!(:link_method => AuthorizeNet::SIM::HostedReceiptPage::LinkMethod::POST)
    t = AuthorizeNet::SIM::Transaction.new(
      auth['login'], auth['key'], amount,
      :hosted_payment_form => true,
      :test => auth['mode']
    )
    t.set_hosted_payment_receipt(AuthorizeNet::SIM::HostedReceiptPage.new(options))
    return t
  end

  def auth
    self.class.conf
  end

end