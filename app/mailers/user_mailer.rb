class UserMailer < ActionMailer::Base
  default from: "muhammad.haris.4063@gmail.com"
  def send_eticket (user, eticket)
    @user   = user
    attachments['e-ticket.pdf'] = File.read("public#{eticket.ticket.url.split('?')[0]}")
    mail :to => @user.email, :subject => "E-Ticket"
  end
  def sell_now (user,url_offer,url_confirm)

    @user         = user
    @url_offer    = url_offer
    @url_confirm  = url_confirm
    mail :to => @user.email, :subject => "Confirm your deal"
  end
  def buy_now (user,url_bid,url_confirm)

    @user         = user
    @url_bid      = url_bid
    @url_confirm  = url_confirm
    mail :to => @user.email, :subject => "Confirm your deal"
  end

  def on_offers_match(request, bid, offer)

    @user     = offer.user
    @bid      = bid
    @offer    = offer
    mail :to => @user.email, :subject => "Bid found for your offer"

  end

  def on_bids_match(request, offer, bid)


    @bid      = bid[0]
    @user     = @bid.user
    @offer    = offer
    @request  = request

    @url_bid = "http://#{request.host_with_port}/api/events/#{@bid.event.id}/bids/#{@bid.id}"

    mail :to => @user.email, :subject => "Offers found for your bid"

  end

  	#default from: "jgray@slicketticket.com"
  	
  	def send_eticket (user, eticket)
	    @user = user
	    attachments['e-ticket.pdf'] = File.read("public#{eticket.ticket.url.split('?')[0]}")
	    mail(:to => @user.email, :subject => "E-Ticket")
  	end

  	def email_name
      mail(:subject => "Mandrill rides the Rails!", :to => "tayyaz@stis.pk", :from => "jgray@slicketticket.com")
  	end

end
