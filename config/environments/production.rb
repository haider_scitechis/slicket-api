Rails.application.configure do
  # Settings specified here will take precedence over those in config/application.rb.

  # Code is not reloaded between requests.
  config.cache_classes = true

  # Eager load code on boot. This eager loads most of Rails and
  # your application in memory, allowing both threaded web servers
  # and those relying on copy on write to perform better.
  # Rake tasks automatically ignore this option for performance.
  config.eager_load = true

  # For Lograge Enable
  config.lograge.enabled = true
  
  config.lograge.ignore_actions = ['api/v1/venues#index', 'api/v1/venues#create', 'api/v1/venues#show', 'api/v1/venues#update', 'api/v1/venues#destroy', 'api/v1/venues#search', 'api/v1/venue_sections#index', 'api/v1/venue_sections#create', 'api/v1/venue_sections#show', 'api/v1/venue_sections#update', 'api/v1/venue_sections#destroy', 'api/v1/venue_sections#list_by_row', 'api/v1/venue_groups#index', 'api/v1/venue_groups#create', 'api/v1/venue_groups#show', 'api/v1/venue_groups#update', 'api/v1/venue_groups#destroy', 'api/v1/venue_groups#list_tickets', 'api/v1/venue_rows#index', 'api/v1/venue_rows#create', 'api/v1/venue_rows#show', 'api/v1/venue_rows#update', 'api/v1/venue_rows#destroy', 'api/v1/venue_rows#list_by_rating', 'api/v1/venue_rows#list_tickets', 'api/v1/events#index', 'api/v1/events#create', 'api/v1/events#show', 'api/v1/events#update', 'api/v1/events#destroy', 'api/v1/events#search', 'api/v1/events#list_tickets', 'api/v1/event_sections#show', 'api/v1/event_sections#list_tickets', 'api/v1/offers#index', 'api/v1/offers#all_user_offers', 'api/v1/offers#show', 'api/v1/offers#list_offers', 'api/v1/offers#broker_batch_upload', 'api/v1/offers#get_notifications', 'api/v1/offers#confirm_deal', 'api/v1/offer_tickets#index', 'api/v1/offer_tickets#create', 'api/v1/offer_tickets#show', 'api/v1/offer_tickets#update', 'api/v1/offer_tickets#destroy', 'api/v1/offer_tickets#check_ticket_status', 'api/v1/uploaded_tickets#show', 'api/v1/uploaded_tickets#update', 'api/v1/uploaded_tickets#print', 'api/v1/uploaded_tickets#email', 'api/v1/coupons#index', 'api/v1/coupons#create', 'api/v1/coupons#show', 'api/v1/coupons#update', 'api/v1/coupons#destroy', 'api/v1/bids#index', 'api/v1/bids#show', 'api/v1/bids#all_user_bids', 'api/v1/bids#get_notifications', 'api/v1/bids#confirm_deal', 'api/v1/accounts#new', 'api/v1/accounts#create', 'api/v1/accounts#instagram', 'api/v1/accounts#dropbox_authorize', 'api/v1/accounts#authorized_callback', 'api/v1/custom_venue_groups#index', 'api/v1/custom_venue_groups#create', 'api/v1/custom_venue_groups#destroy', 'api/v1/negotiation_logs#index', 'api/v1/negotiation_logs#create', 'api/v1/negotiation_logs#show', 'api/v1/negotiation_logs#destroy', 'api/v1/users#index', 'api/v1/users#show']
  
  config.lograge.custom_options = lambda do |event|
      # capture some specific timing values you are interested in
      if not event.payload[:params]["user_email"].nil?
        foo = {:date => event.time.localtime.strftime("%B %d, %Y"), :time => event.time.localtime, :ip => event.payload[:data].env["REMOTE_HOST"], :user_agent => event.payload[:data].env["HTTP_USER_AGENT"], :user => event.payload[:params]["user_email"]}
        File.open("#{Rails.public_path}/production_logging.json", 'a'){|f| f.puts foo.to_json }
      elsif not event.payload[:params]["user"].nil?
        if not event.payload[:params]["user"]["email"].nil?
          foo = {:date => event.time.localtime.strftime("%B %d, %Y"), :time => event.time.localtime, :ip => event.payload[:data].env["REMOTE_HOST"], :user_agent => event.payload[:data].env["HTTP_USER_AGENT"], :user => event.payload[:params]["user"]["email"]}   
          File.open("#{Rails.public_path}/production_logging.json", 'a'){|f| f.puts foo.to_json }
        else
          foo = {:date => event.time.localtime.strftime("%B %d, %Y"), :time => event.time.localtime, :ip => event.payload[:data].env["REMOTE_HOST"], :user_agent => event.payload[:data].env["HTTP_USER_AGENT"]}   
          File.open("#{Rails.public_path}/production_logging.json", 'a'){|f| f.puts foo.to_json }
        end
      else
        foo = {:date => event.time.localtime.strftime("%B %d, %Y"), :time => event.time.localtime, :ip => event.payload[:data].env["REMOTE_HOST"], :user_agent => event.payload[:data].env["HTTP_USER_AGENT"]}   
        File.open("#{Rails.public_path}/production_logging.json", 'a'){|f| f.puts foo.to_json }
      end
  end
  
  # Full error reports are disabled and caching is turned on.
  config.consider_all_requests_local       = true
  config.action_controller.perform_caching = true

  # Enable Rack::Cache to put a simple HTTP cache in front of your application
  # Add `rack-cache` to your Gemfile before enabling this.
  # For large-scale production use, consider using a caching reverse proxy like nginx, varnish or squid.
  # config.action_dispatch.rack_cache = true

  # Disable Rails's static asset server (Apache or nginx will already do this).
  config.serve_static_assets = false


  # Specifies the header that your server uses for sending files.
  # config.action_dispatch.x_sendfile_header = "X-Sendfile" # for apache
  # config.action_dispatch.x_sendfile_header = 'X-Accel-Redirect' # for nginx

  # Force all access to the app over SSL, use Strict-Transport-Security, and use secure cookies.
  # config.force_ssl = true

  # Set to :debug to see everything in the log.
  config.log_level = :info

  # Prepend all log lines with the following tags.
  # config.log_tags = [ :subdomain, :uuid ]

  # Use a different logger for distributed setups.
  # config.logger = ActiveSupport::TaggedLogging.new(SyslogLogger.new)

  # Use a different cache store in production.
  # config.cache_store = :mem_cache_store

  # Enable serving of images, stylesheets, and JavaScripts from an asset server.
  # config.action_controller.asset_host = "http://assets.example.com"


  # Ignore bad email addresses and do not raise email delivery errors.
  # Set this to true and configure the email server for immediate delivery to raise delivery errors.
  config.action_mailer.raise_delivery_errors = true
  config.action_mailer.default_url_options  = { :host => '#{Rails.application.secrets.app_host}' }
  config.action_mailer.delivery_method      = :smtp

  config.action_mailer.smtp_settings = {
    :address   => "smtp.mandrillapp.com",
    :port      => 587, # ports 587 and 2525 are also supported with STARTTLS
    :enable_starttls_auto => true, # detects and uses STARTTLS
    :user_name => ENV['MANDRILL_USERNAME'],
    :password  => ENV['MANDRILL_PASSWORD'], # SMTP password is any valid API key
    :authentication => 'login', # Mandrill supports 'plain' or 'login'
    :domain => 'https://mandrillapp.com/login', # your domain to identify your server when connecting
  }

  # Enable locale fallbacks for I18n (makes lookups for any locale fall back to
  # the I18n.default_locale when a translation cannot be found).
  config.i18n.fallbacks = true

  # Send deprecation notices to registered listeners.
  config.active_support.deprecation = :notify

  # Disable automatic flushing of the log to improve performance.
  # config.autoflush_log = false

  # Use default logging formatter so that PID and timestamp are not suppressed.
  config.log_formatter = ::Logger::Formatter.new

  # Do not dump schema after migrations.
  config.active_record.dump_schema_after_migration = false
end
