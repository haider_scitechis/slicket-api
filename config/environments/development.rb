Rails.application.configure do
  # Settings specified here will take precedence over those in config/application.rb.

  # In the development environment your application's code is reloaded on
  # every request. This slows down response time but is perfect for development
  # since you don't have to restart the web server when you make code changes.
  config.cache_classes = false

  # Do not eager load code on boot.
  config.eager_load = false

  # For Lograge Enable
  config.lograge.enabled = true
  
  config.lograge.ignore_actions = ['api/v1/venues#index', 'api/v1/venues#create', 'api/v1/venues#show', 'api/v1/venues#update', 'api/v1/venues#destroy', 'api/v1/venues#search', 'api/v1/venue_sections#index', 'api/v1/venue_sections#create', 'api/v1/venue_sections#show', 'api/v1/venue_sections#update', 'api/v1/venue_sections#destroy', 'api/v1/venue_sections#list_by_row', 'api/v1/venue_groups#index', 'api/v1/venue_groups#create', 'api/v1/venue_groups#show', 'api/v1/venue_groups#update', 'api/v1/venue_groups#destroy', 'api/v1/venue_groups#list_tickets', 'api/v1/venue_rows#index', 'api/v1/venue_rows#create', 'api/v1/venue_rows#show', 'api/v1/venue_rows#update', 'api/v1/venue_rows#destroy', 'api/v1/venue_rows#list_by_rating', 'api/v1/venue_rows#list_tickets', 'api/v1/events#index', 'api/v1/events#create', 'api/v1/events#show', 'api/v1/events#update', 'api/v1/events#destroy', 'api/v1/events#search', 'api/v1/events#list_tickets', 'api/v1/event_sections#show', 'api/v1/event_sections#list_tickets', 'api/v1/offers#index', 'api/v1/offers#all_user_offers', 'api/v1/offers#show', 'api/v1/offers#list_offers', 'api/v1/offers#broker_batch_upload', 'api/v1/offers#get_notifications', 'api/v1/offers#confirm_deal', 'api/v1/offer_tickets#index', 'api/v1/offer_tickets#create', 'api/v1/offer_tickets#show', 'api/v1/offer_tickets#update', 'api/v1/offer_tickets#destroy', 'api/v1/offer_tickets#check_ticket_status', 'api/v1/uploaded_tickets#show', 'api/v1/uploaded_tickets#update', 'api/v1/uploaded_tickets#print', 'api/v1/uploaded_tickets#email', 'api/v1/coupons#index', 'api/v1/coupons#create', 'api/v1/coupons#show', 'api/v1/coupons#update', 'api/v1/coupons#destroy', 'api/v1/bids#index', 'api/v1/bids#show', 'api/v1/bids#all_user_bids', 'api/v1/bids#get_notifications', 'api/v1/bids#confirm_deal', 'api/v1/accounts#new', 'api/v1/accounts#create', 'api/v1/accounts#instagram', 'api/v1/accounts#dropbox_authorize', 'api/v1/accounts#authorized_callback', 'api/v1/custom_venue_groups#index', 'api/v1/custom_venue_groups#create', 'api/v1/custom_venue_groups#destroy', 'api/v1/negotiation_logs#index', 'api/v1/negotiation_logs#create', 'api/v1/negotiation_logs#show', 'api/v1/negotiation_logs#destroy', 'api/v1/users#index', 'api/v1/users#show']
  
  config.lograge.custom_options = lambda do |event|
      # capture some specific timing values you are interested in
      if not event.payload[:params]["user_email"].nil?
        foo = {:date => event.time.localtime.strftime("%B %d, %Y"), :time => event.time.localtime, :ip => event.payload[:data].env["REMOTE_HOST"], :user_agent => event.payload[:data].env["HTTP_USER_AGENT"], :user => event.payload[:params]["user_email"]}
        File.open("#{Rails.public_path}/development_logging.json", 'a'){|f| f.puts foo.to_json }
      elsif not event.payload[:params]["user"].nil?
        if not event.payload[:params]["user"]["email"].nil?
          foo = {:date => event.time.localtime.strftime("%B %d, %Y"), :time => event.time.localtime, :ip => event.payload[:data].env["REMOTE_HOST"], :user_agent => event.payload[:data].env["HTTP_USER_AGENT"], :user => event.payload[:params]["user"]["email"]}   
          File.open("#{Rails.public_path}/development_logging.json", 'a'){|f| f.puts foo.to_json }
        else
          foo = {:date => event.time.localtime.strftime("%B %d, %Y"), :time => event.time.localtime, :ip => event.payload[:data].env["REMOTE_HOST"], :user_agent => event.payload[:data].env["HTTP_USER_AGENT"]}   
          File.open("#{Rails.public_path}/development_logging.json", 'a'){|f| f.puts foo.to_json }
        end
      else
#        foo = {:date => event.time.localtime.strftime("%B %d, %Y"), :time => event.time.localtime, :ip => event.payload[:data].env["REMOTE_HOST"], :user_agent => event.payload[:data].env["HTTP_USER_AGENT"]}   
#        File.open("#{Rails.public_path}/development_logging.json", 'a'){|f| f.puts foo.to_json }
      end
  end
  
  # Show full error reports and disable caching.
  config.consider_all_requests_local       = true
  config.action_controller.perform_caching = false

  # Don't care if the mailer can't send.
  config.action_mailer.raise_delivery_errors = true
  config.mandrill_mailer.default_url_options

  # Print deprecation notices to the Rails logger.
  config.active_support.deprecation = :log

  # Raise an error on page load if there are pending migrations.
  config.active_record.migration_error = :page_load

  config.action_mailer.default_url_options  = { :host => 'localhost:3000' }
  config.action_mailer.delivery_method      = :smtp

  config.action_mailer.smtp_settings = {
    :address   => "smtp.mandrillapp.com",
    :port      => 2525, # ports 587 and 2525 are also supported with STARTTLS
    :enable_starttls_auto => true, # detects and uses STARTTLS
    :user_name => 'jgray@slicketticket.com',
    :password  => 'GkqPkcH3lDeLzOZw6U7gCg', # SMTP password is any valid API key
    :authentication => 'login', # Mandrill supports 'plain' or 'login'
    :domain => 'https://mandrillapp.com/login', # your domain to identify your server when connecting
  }

  # Raises error for missing translations
  # config.action_view.raise_on_missing_translations = true
end