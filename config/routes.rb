require 'api_constraints'


Rails.application.routes.draw do


  default_url_options :host => "localhost:3000"

  scope module: :v1, constraints: ApiConstraints.new(version: 1, default: :true) do

    devise_for :users, path: '/api/users',controllers: {
      registrations: 'api/v1/custom_devise/registrations',
      passwords: 'api/v1/custom_devise/passwords',
      sessions: 'api/v1/custom_devise/sessions',
      invitations: 'api/v1/custom_devise/invitations'
    
    }
  end

  namespace :api, defaults: {format: 'json'} do
    scope module: :v1, constraints: ApiConstraints.new(version: 1, default: :true) do
      resources :users
      post 'broker_batch_upload', :to => 'offers#broker_batch_upload', :as => :broker_batch_upload


      resources :email_alert_config_options, :only =>[:index, :create, :destroy]
      resources :sms_alert_config_options, :only =>[:index, :create, :destroy]

      resources :offers, :except => [:new, :edit, :create, :update, :destroy, :show, :index]  do 
        resources :bids, :except => [:new, :edit, :create, :update, :destroy, :show, :index] do
          resources :negotiation_logs, :only => [:index, :create, :show, :destroy]
        end
      end

        resources :offer_tickets, :except => [:new, :edit, :create, :update, :destroy, :show, :index] do
            collection do
                get :check_ticket_status
            end
        end

      resources :events do
        get 'venue_groups/:id/list_tickets', :to => 'venue_groups#list_tickets', :as => :venue_groups_list_tickets
        resources :venue_groups
        get '/offers/all_user_offers', :to => 'offers#all_user_offers', :as => :all_user_offers
        resources :offers do 
          resources :offer_tickets
        end

        resource :custom_venue_groups
        get '/bids/all_user_bids', :to => 'bids#all_user_bids', :as => :all_user_bids
        resources  :custom_venue_groups, :only => [:index, :create, :destroy]
        resources :offers do 
          resources :offer_tickets
        end


        get 'offers/list_offers', :to => 'offers#list_offers', :as => :offers_list_tickets

        resources :offers do 
          get '/get_notifications' ,  :to => 'offers#get_notifications', :as => :get_notifications
          post '/sell_now' ,  :to => 'offers#sell_now', :as => :sell_now
          get '/confirm_deal' ,  :to => 'offers#confirm_deal', :as => :confirm_deal
          resources :offer_tickets do
            resources :uploaded_tickets, :only => [:show, :update]
            get 'uploaded_tickets/:id/print', :to => 'uploaded_tickets#print', :as => :print_uploaded_tickets
            get 'uploaded_tickets/:id/email', :to => 'uploaded_tickets#email', :as => :email_uploaded_tickets
          end
        end

        resources :bids do
          get '/get_notifications' ,  :to => 'bids#get_notifications', :as => :get_notifications
          post '/buy_now' ,  :to => 'bids#buy_now', :as => :buy_now
          get '/confirm_deal' ,  :to => 'bids#confirm_deal', :as => :confirm_deal
        end

        get 'event_sections/:id/list_tickets', :to => 'event_sections#list_tickets', :as => :sections_list_tickets

        resources :event_sections do
            get 'venue_rows/list_by_rating', :to => 'venue_rows#list_by_rating', :as => :list_rows_by_rating
            get 'venue_rows/:id/list_tickets', :to => 'venue_rows#list_tickets', :as => :venue_rows_list_tickets
            resources :venue_rows
        end
      end

      get '/venues/search', :to => 'venues#search', :as => :venue_search
      resources :venues do
          get 'venue_sections/list_by_row', :to => 'venue_sections#list_by_row', :as => :list_sections_by_row
          resources :venue_sections 
      end
      get '/events/:id/list_tickets', :to => 'events#list_tickets', :as => :event_list_tickets
      get '/events/search/:query', :to => 'events#search', :as => :event_search

      resources :coupons

    end
  end

  resources :charges
  #root :to => "home#index"

end