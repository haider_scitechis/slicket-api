Rails.application.config.middleware.use OmniAuth::Builder do
  provider :facebook, SlicketTicket::Application.config.facebook_token, SlicketTicket::Application.config.facebook_secret
  provider :twitter, SlicketTicket::Application.config.twitter_token, SlicketTicket::Application.config.twitter_secret

end