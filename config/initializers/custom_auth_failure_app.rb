class CustomAuthFailure < Devise::FailureApp
  def respond
    self.status = 401
    self.content_type = 'json'
    self.response_body = {"errors" => ["Invalid"], debug_error_from: "DEBUG_INFO: CustomAuthFailure"}.to_json
  end
end
